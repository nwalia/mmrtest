//
// CollectionType.swift
// Dev-SAML
//
// Created by SAP Cloud Platform SDK for iOS Assistant application on 12/03/18
//

import Foundation

enum CollectionType: String {

    case zdmgetmeterreadinfoSet = "ZDMGETMETERREADINFOSet"
    case engineerSet = "EngineerSet"
    case operationSet = "OperationSet"
    case newMeterSet = "ZDM_NEW_SERVICE_COLL"
    case imageSet = "FILESET"
    case sequenceSet = "RESEQSet"
    case changeStatusSet = "MassUpdateTasksSet"
    case none = ""

    static let all = [
        zdmgetmeterreadinfoSet,
        engineerSet,
        operationSet
    ]
}
