//
//  AppDelegate.swift
//  Enbridge Meter Reader
//
//  Created by Johann Wentzel on 2017-11-29.
//  Copyright © 2017 Enbridge. All rights reserved.
//

import UIKit
import CoreData
import SAPFiori
import SAPFioriFlows
import SAPFoundation
import SAPCommon
import UserNotifications
import SAPOData
import MapKit
import SAPOfflineOData
import proxyclasses


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UISplitViewControllerDelegate, OnboardingManagerDelegate, UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    static var counter = 0
    static var hasMockData = false
    
    var zdmnewservicesrvEntities: ZDM<OnlineODataProvider>!
    var zdmmeterpicsrvEntities: I1<OnlineODataProvider>!
    static var onboardingContext: OnboardingContext!
    
    private let logger = Logger.shared(named: "AppDelegateLogger")
    var MEc1: M1Ec1<OnlineODataProvider>!
    
    var center: UNUserNotificationCenter!
    
    // Check for reachability
    let reachability: Reachability? = Reachability.networkReachabilityForInternetConnection()
    
    private var ShareInstancePeriodic: LocalStorePeriodic? = nil
    private var ShareInstanceOffCycle: LocalStoreOffCycle? = nil
    private var ShareInstanceNewMeter: LocalStoreNewMeter? = nil
    private var ShareInstanceImage: LocalStoreImage? = nil
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        //Set the user defaults for application settings
        registerDefaultsFromSettingsBundle()
        
        //function that is called to set the environemnt in which to run the app when users changes it in the settings
        SettingsBundleHelper.setEnvironment()
        
        // enable uploading of logs
        do {
            try SAPcpmsLogUploader.attachToRootLogger()
        } catch {
            //print("ERROR: Logger could not be initialized.")
            logger.error("Failed to attach to root logger.", error: error)
        }
        
        //adding application settings
        SettingsBundleHelper.setVersionAndBuildNumber()
        
        
        
        Logger.root.logLevel = .debug
        
        // Register device for remote notifications
        registerForPushNotifications()
        
        // Custom style for SAP SAML Auth
        NUISettings.initWithStylesheet(name: "CustomTheme")
        
        UIApplication.shared.statusBarStyle = .lightContent
        
        // Enables IQKeyboardManager. Docs: https://github.com/hackiftekhar/IQKeyboardManager
        IQKeyboardManager.sharedManager().enable = true
        
        // Check for reachability
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityDidChange(_:)), name: NSNotification.Name(rawValue: ReachabilityDidChangeNotificationName), object: nil)
        
        _ = reachability?.startNotifier()
        
        
        /**Dana
         let urlSession = SAPURLSession(configuration: URLSessionConfiguration.default, delegate: nil, delegateQueue: nil)
         urlSession.register(SAPcpmsObserver(applicationID: Constants.appId, deviceID: UIDevice.current.identifierForVendor!.uuidString))
         
         // Set a FUIInfoViewController as the rootViewController, since there it is none set in the Main.storyboard
         self.window = UIWindow(frame: UIScreen.main.bounds)
         self.window!.rootViewController = FUIInfoViewController.createSplashScreenInstanceFromStoryboard()
         self.configureOData(urlSession, Constants.sapcpmsUrl)
         **/
        
        do {
            // Attaches a LogUploadFileHandler instance to the root of the logging system
            try SAPcpmsLogUploader.attachToRootLogger()
        } catch {
            self.logger.error("Failed to attach to root logger.", error: error)
        }
        
        UINavigationBar.applyFioriStyle()
        
        OnboardingManager.shared.delegate = self
        OnboardingManager.shared.assignEnvironmentInfo()
        OnboardingManager.shared.onboardOrRestore()
        
        // Update app badge icon to number of unread messages
        UIApplication.shared.applicationIconBadgeNumber = GlobalVariables.badgeCount
        
        //Model.clear()
        
        return true
        
    }
    
    // To only support portrait orientation during onboarding
    func application(_: UIApplication, supportedInterfaceOrientationsFor _: UIWindow?) -> UIInterfaceOrientationMask {
        switch OnboardingFlowController.presentationState {
        case .onboarding, .restoring:
            return .portrait
        default:
            return .portrait
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        
        
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        OnboardingManager.shared.applicationDidEnterBackground()
        
    }
    
    // Delegate to OnboardingManager.
    func applicationWillEnterForeground(_ application: UIApplication) {
        OnboardingManager.shared.applicationWillEnterForeground()
        
    }
    
    func onboarded(onboardingContext: OnboardingContext) {
        let sapCpmsSettingParameter = onboardingContext.info[OnboardingInfoKey.sapcpmsSettingsParameters] as! SAPcpmsSettingsParameters
        self.configureOData(onboardingContext.sapURLSession)
        self.setRootViewController()
        LogHelper.shared = LogHelper(urlSession: onboardingContext.sapURLSession, settingsParameters: sapCpmsSettingParameter)
        LogHelper.shared?.upload()
        self.registerForRemoteNotification(onboardingContext.sapURLSession, sapCpmsSettingParameter)
        AppDelegate.onboardingContext = onboardingContext
        
        self.logger.logLevel = .debug
        
        LogHelper.shared?.debug("SAP UUID = \(sapCpmsSettingParameter.deviceID ?? "NONE") -- iOS UUID = \(String(describing: UIDevice.current.identifierForVendor)) ", loggerObject: self.logger)
        
    }
    
    
    // Function takes user to select route screen after log in successful
    func setRootViewController() {
        DispatchQueue.main.async {
            //  Take user to select route screen
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "MainNav", bundle: nil)
            let vc = mainStoryboard.instantiateInitialViewController()
            self.window!.rootViewController = vc
        }
    }
    
    
    // MARK: - Split view
    func splitViewController(_: UISplitViewController, collapseSecondary _: UIViewController, onto _: UIViewController) -> Bool {
        // The first Collection will be selected automatically, so we never discard showing the secondary ViewController
        return false
    }
    
    // MARK: - Remote Notification handling
    private var deviceToken: Data?
    
    func application(_: UIApplication, willFinishLaunchingWithOptions _: [UIApplication.LaunchOptionsKey: Any]? = nil) -> Bool {
        UIApplication.shared.registerForRemoteNotifications()
        center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.alert, .badge, .sound]) { _, _ in
            // Enable or disable features based on authorization.
        }
        center.delegate = self
        return true
    }
    
    // Called to let your app know which action was selected by the user for a given notification.
    // Add functionality for when user clicks on notification
    func userNotificationCenter(_: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        LogHelper.shared?.info("App opened via user selecting notification: \(response.notification.request.content.body)", loggerObject: self.logger)
        // Here is where you want to take action to handle the notification, maybe navigate the user to a given screen.
        completionHandler()
        
    }
    
    // Called when a notification is delivered to a foreground app.
    func userNotificationCenter(_: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        LogHelper.shared?.info("Remote Notification arrived while app was in foreground: \(notification.request.content.body)", loggerObject: self.logger)
        // Currently we are presenting the notification alert as the application were in the background.
        // If you have handled the notification and do not want to display an alert, call the completionHandler with empty options: completionHandler([])
        completionHandler([.alert, .sound])
    }
    
    // Initialize this to allow push norigications
    func registerForRemoteNotification(_ urlSession: SAPURLSession, _ settingsParameters: SAPcpmsSettingsParameters) {
        guard let deviceToken = self.deviceToken else {
            // Device token has not been acquired
            return
        }
        
        let remoteNotificationClient = SAPcpmsRemoteNotificationClient(sapURLSession: urlSession, settingsParameters: settingsParameters)
        remoteNotificationClient.registerDeviceToken(deviceToken) { error in
            if let error = error {
                LogHelper.shared?.error("Register for push notifications failed.", loggerObject: self.logger, error: error)
                return
            }
            LogHelper.shared?.debug("Register for push notifications succeeded.", loggerObject: self.logger)
        }
    }
    
    
    func unregisterForRemoteNotification() {
        guard self.deviceToken != nil else {
            // Device token has not been acquired
            return
        }
        
        let remoteNotificationClient = SAPcpmsRemoteNotificationClient(sapURLSession: AppDelegate.onboardingContext.sapURLSession, settingsParameters: AppDelegate.onboardingContext.info[OnboardingInfoKey.sapcpmsSettingsParameters] as! SAPcpmsSettingsParameters)
        remoteNotificationClient.unregisterDeviceToken() { error in
            if let error = error {
                LogHelper.shared?.error("Register for push notifications failed.", loggerObject: self.logger, error: error)
                return
            }
            LogHelper.shared?.debug("Register for push notifications succeeded.", loggerObject: self.logger)
        }
    }
    
    // Handle data coming in from remote push notification (triggered every time notification is received) - holds
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        
        var meterStatus: String?
        var meterCallId: String?
        
        if let data = userInfo as? [String:Any] {
            
            if let status = data["STATUS"] as? String {
                // access individual value in dictionary
                meterStatus = status
                //print("status: \(status)")
            }
            if let callId = data["CALLID"] as? String {
                // access individual value in dictionary
                meterCallId = callId
                //print("callId: \(callId)")
            }
            if let number = data["NUMBER"] as? String {
                // access individual value in dictionary
                print("number: \(number)")
            }
            if let message = data["MESSAGE"] as? String {
                // access individual value in dictionary
                print("message: \(message)")
            }
            
        }
        //print(userInfo.description) // userInfo is json of APNS message
        LogHelper.shared?.info("Received notification - meterCallId = \(meterCallId ?? "none"), meterStatus = \(meterStatus ?? "none"), description: \(userInfo.description)", loggerObject: self.logger)
        
        
        
        // Currently only create notification for cancelled and new work
        // Only call function when callID and meter status are not empty or nil
        if (!((meterCallId ?? "").isEmpty) && !((meterStatus ?? "").isEmpty)) {
            localNotification(callId: meterCallId!, status: meterStatus!)
        }
        
        
        // Need to include completion handler
        completionHandler(UIBackgroundFetchResult(rawValue: UInt(1))!)
        
    }
    
    
    // Create local message & notification about job cancellation or new work
    func localNotification(callId: String, status: String) {
        
        
        // Currently only supporting notifications for cancelled or new work
        if status == "CANCEL" || status == "DISP" || status == "REDE" {
            
            // Create message elements for new message
            var message: Message!
            let from = "MET Back Office"
            var subject = "No Subject"
            var body = "Message is empty."
            
            // Format time correctly
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.amSymbol = "AM"
            formatter.pmSymbol = "PM"
            formatter.dateFormat = "h:mm a"
            let timeString = formatter.string(from: Date())
            
            // Format date correctly
            formatter.dateFormat = "MMMM dd, yyyy"
            let dateString = formatter.string(from: Date())
            
            if status == "CANCEL" {
                
                // Cancelled work order - offcycle only
                
                // Create message elements for new message
                subject = "Work Order Cancellation"
                body = "Your assigned work order #\(callId) has been cancelled. Please open your dashboard to view the updates."
                
                createNotification(contentTitle: subject, contentBody: body, contentSound: UNNotificationSound.default, triggerTime: 1, triggerRepeat: false, requestIdentifier: "CancelJob")
                
                // Find cancelled meter in offcycle route and cancel it
                if let meterRead = Model.sharedInstance.routes[GlobalConstants.OFFCYCLE_ROUTE_ID]?.meterReads {
                    for meter in meterRead {
                        if meter.jobID == callId {
                            meter.cancelled = true
                            meter.mmrStatus = "CANCEL"
                            meter.status = .completed
                            meter.rawReadDate = Date()
                            
                            //print("Cancelled meter job ID: \(meter.jobID)")
                        }
                    }
                }
                
                // Create message with info
                message = Message(from: from, subject: subject, date: dateString, time: timeString, body: body, opened: false)
                messages.append(message)
                
                // Increase badge count and icon for messaging tab
                GlobalVariables.badgeCount += 1
                updateMessagesBadge()
                
                // "reloadTables" notification called - will trigger observers in various classes to reload messages table, select route table, offcycle cells, and offcycle dasboard
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadTables"), object: nil)
                
            } else if status == "DISP" {
                
                // New work order
                
                // Create message elements for new message
                subject = "New Work"
                body = "You have been assigned new work. Please return to the Select Route screen and pull-down to refresh your available routes."
                
                createNotification(contentTitle: subject, contentBody: body, contentSound: UNNotificationSound.default, triggerTime: 1, triggerRepeat: false, requestIdentifier: "NewJob")
                
                // Create message with info
                message = Message(from: from, subject: subject, date: dateString, time: timeString, body: body, opened: false)
                messages.append(message)
                
                // Increase badge count and icon for messaging tab
                GlobalVariables.badgeCount += 1
                updateMessagesBadge()
                
                // "newWork" notification called - will trigger observers in various classes to reload messages table, select route table, offcycle cells, and offcycle dasboard
                //                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "newWork"), object: nil)
                
                
            } else if status == "REDE" {
                
                if Model.sharedInstance.routes[GlobalConstants.OFFCYCLE_ROUTE_ID] == nil {return}
                // Unscheduled work order - offcycle only (NO local notification or message)
                
                
                // Find unscheduled meter in offcycle route and remove it
                var index: Int?
                var number: Int?
                if let meterRead = Model.sharedInstance.routes[GlobalConstants.OFFCYCLE_ROUTE_ID]?.meterReads {
                    for meter in meterRead {
                        if meter.jobID == callId {
                            meter.unscheduled = true
                            meter.status = .completed
                            // Save index of unscheduled meter to be removed
                            index = Model.sharedInstance.routes[GlobalConstants.OFFCYCLE_ROUTE_ID]!.meterReads.firstIndex(of: meter)
                            number = meter.number
                            print("\(String(describing: number))")
                            //print("! Unscheduled meter job ID: \(meter.jobID)")
                        }
                    }
                }
                
                // Remove the unscheduled work from the device
                if index != nil {
                    Model.sharedInstance.routes[GlobalConstants.OFFCYCLE_ROUTE_ID]!.meterReads.remove(at: index!)
                }
                
                // Check if route is empty, call notification accordingly
                if let meters = Model.sharedInstance.routes[GlobalConstants.OFFCYCLE_ROUTE_ID]?.meterReads {
                    if meters.count == 0 {
                        
                        // Remove the route and call "emptyRoute" notification - trigger observers in various classes to show alert if user is currently on that route (dashboard or service request screen)
                        Model.sharedInstance.routes.removeValue(forKey: GlobalConstants.OFFCYCLE_ROUTE_ID)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "emptyRoute"), object: nil)
                        UserDefaults.standard.set("", forKey: "OffCycleRouteStatus")
                        OffCycleCell.hasBeenStarted = false
                    } else {
                        
                        // "reloadTables" notification called - will trigger observers in various classes to refresh messages table, select route table, offcycle cells, and offcycle dasboard
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadTables"), object: nil)
                    }
                }
            } // end REDE
            
        } // end if status == CANCEL or DISP or REDE
        
    }
    
    // Creates local notification (content and trigger)
    func createNotification(contentTitle: String, contentBody: String, contentSound: UNNotificationSound, triggerTime: Double, triggerRepeat: Bool, requestIdentifier: String) {
        // Create notification object with the notification details
        let content = UNMutableNotificationContent()
        content.title = NSString.localizedUserNotificationString(forKey: contentTitle, arguments: nil)
        content.body = NSString.localizedUserNotificationString(forKey: contentBody, arguments: nil)
        content.sound = contentSound
        
        //Set trigger for one time in 1 second
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: triggerTime, repeats: false)
        
        // Create the request object.
        let identifier = requestIdentifier
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        center.add(request, withCompletionHandler: { (error) in
            if let error = error {
                // Something went wrong
                print("! Error in adding notification to notification center. Error: \(error)")
            }
        })
    }
    
    // Update messages tab and app badge with number of unread messages
    func updateMessagesBadge() {
        
        // Call function from Main Nav (tab bar) VC to update messages tab badge
        let mainNav: MainNavViewController = self.window?.rootViewController as! MainNavViewController
        mainNav.setMessagesBadge(badgeCount: GlobalVariables.badgeCount)
        
        // Update app badge
        UIApplication.shared.applicationIconBadgeNumber = GlobalVariables.badgeCount
    }
    
    func application(_: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        self.deviceToken = deviceToken
    }
    
    func application(_: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        LogHelper.shared?.error("Failed to register for Remote Notification", loggerObject: self.logger, error: error)
    }
    
    // MARK: - Configure OData
    private func configureOData(_ urlSession: SAPURLSession) {
        
        let odataProvider = OnlineODataProvider(serviceName: "M1Ec1", serviceRoot: URL(string: "\(environmentURL)/\(clickDestination)")!, sapURLSession: urlSession)
        // Disables version validation of the backend OData service
        // TODO: Should only be used in demo and test applications
        odataProvider.serviceOptions.checkVersion = false
        self.MEc1 = M1Ec1(provider: odataProvider)
        // To update entity force to use X-HTTP-Method header
        self.MEc1.provider.networkOptions.tunneledMethods.append("MERGE")
        
        let odata_Provider = OnlineODataProvider(serviceName: "ZDMNEWSERVICESRVEntities", serviceRoot: URL(string: "\(environmentURL)/\(newServiceDestination)")!, sapURLSession: urlSession)
        // Disables version validation of the backend OData service
        // TODO: Should only be used in demo and test applications
        odata_Provider.serviceOptions.checkVersion = false
        self.zdmnewservicesrvEntities = ZDM(provider: odata_Provider)
        // To update entity force to use X-HTTP-Method header
        self.zdmnewservicesrvEntities.provider.networkOptions.tunneledMethods.append("MERGE")
        
        // make the session available in get call
        GetWorkOrderFromBackEnd_SAP_Click.urlSession = urlSession
        
        // Set up local store for image
        let _ = LocalStoreImage(urlSession)
        LocalStoreImage.offlineLS?.downloadFromDB()
        
        // Set up local store for new meter
        let _ = LocalStoreNewMeter(urlSession)
        LocalStoreNewMeter.offlineLS?.downloadFromDB()
        
        //Set up local store for resequence
        //let _ = LocalStoreReSequence(urlSession)
        //LocalStoreReSequence.offlineLS?.downloadFromDB()
        
        //Set up local store for changeStatusServ
        let _ = LocalStoreUpdateStatusServ(urlSession)
        LocalStoreUpdateStatusServ.shareInstance?.downloadFromDB()
        
        
        //Set up local manager
        let _ = LocalStoreManager(urlSession: urlSession)
        
        
        
    }
    
    // END SAP ASSISTANT
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        dereferenceLocalStore()
        self.saveContext()
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Enbridge_Meter_Reader")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func checkReachability() {
        guard let r = reachability else { return }
        if r.isReachable  {
            // do stuff when network is available.
            AppDelegate.counter = 0
            
            // If the status bar was previously greyed out, make it normal
            if let colorBarView = window?.rootViewController?.view.viewWithTag(100) {
                colorBarView.removeFromSuperview()
            }
            
            LocalStoreManager.ShareInstance?.offlineToOnlineSync()
            
        } else {
            if AppDelegate.counter == 0 {
                let alertController = UIAlertController(title: "Error", message: "Map is not available.", preferredStyle: .alert)
                
                let CloseAction = UIAlertAction(title: "Okay", style: .default) { action in
                }
                alertController.addAction(CloseAction)
                
                if window?.currentViewController() is PeriodicDashboardViewController || window?.currentViewController() is DashboardViewController {
                    self.window?.currentViewController()?.present(alertController, animated: true) {
                        // ...
                    }
                }
            }
            AppDelegate.counter = 1
            
            // Gray out the status bar.
            let colorBarFrame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 20)
            let colorBarView = UIView(frame: colorBarFrame)
            colorBarView.tag = 100
            colorBarView.backgroundColor = GlobalConstants.OFFLINE_STATUS_BAR
            window?.rootViewController?.view.addSubview(colorBarView)
            
        }
        
    }
    
    @objc func reachabilityDidChange(_ notification: Notification) {
        checkReachability()
    }
    
    func showNetworkToolbar(isOnline: Bool){
        
        let spaceLeft = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let spaceRight = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let label = UILabel()
        
        let viewController = UIApplication.shared.topMostViewController()!
        
        label.text = isOnline ? "Connected. Pending work has been sent." : "You are not connected to the network. Work will be sent when connection is restored."
        label.textColor = .white
        label.font = UIFont.boldSystemFont(ofSize: 16.0)
        
        let labelBarButtonItem = UIBarButtonItem(customView: label)
        let toolbarFrame = CGRect(x: 0, y: -48, width: UIScreen.main.bounds.width, height: 48)
        let toolbar = UIToolbar(frame: toolbarFrame)
        
        toolbar.isTranslucent = false
        toolbar.barTintColor = isOnline ? GlobalConstants.NETWORK_STATUS_GREEN : .red
        
        toolbar.setItems([spaceLeft, labelBarButtonItem, spaceRight], animated: false)
        toolbar.delegate = self
        
        // animates the bar onto the screen.
        UIView.animate(withDuration: 0.3, animations: {
            viewController.view.addSubview(toolbar)
            var newFrame = toolbar.frame
            newFrame.origin.y += toolbar.frame.height
            toolbar.frame = newFrame
        })
        
        // animates the bar out of sight after 5 seconds.
        UIView.animate(withDuration: 0.3, delay: 5.0, options: .curveLinear, animations: {
            var newFrame = toolbar.frame
            newFrame.origin.y -= toolbar.frame.height
            toolbar.frame = newFrame
            
        }, completion: nil)
        
    }
    
    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        //print("Perform Fetch with completion handler TEST")
    }
    
    func registerForPushNotifications() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, error) in
            //print("Permission granted: \(granted)")
        }
    }
    
    func dereferenceLocalStore () {
        LocalStoreManager.ShareInstance = nil
        LocalStoreImage.offlineSP = nil
        LocalStoreImage.offlineLS = nil
        LocalStoreNewMeter.offlineSP = nil
        LocalStoreNewMeter.offlineLS = nil
        LocalStoreUpdateStatusServ.offlineSP = nil
        LocalStoreUpdateStatusServ.shareInstance = nil
        LocalStoreReSequence.offlineSP = nil
        LocalStoreReSequence.offlineLS = nil
    }
    
}

extension AppDelegate: UIToolbarDelegate {
    func position(for bar: UIBarPositioning) -> UIBarPosition {
        return .top
    }
}

func registerDefaultsFromSettingsBundle()
{
    let settingsUrl = Bundle.main.url(forResource: "Settings", withExtension: "bundle")!.appendingPathComponent("Root.plist")
    let settingsPlist = NSDictionary(contentsOf:settingsUrl)!
    let preferences = settingsPlist["PreferenceSpecifiers"] as! [NSDictionary]
    
    var defaultsToRegister = Dictionary<String, Any>()
    
    for preference in preferences {
        guard let key = preference["Key"] as? String else {
            NSLog("Key not fount")
            continue
        }
        defaultsToRegister[key] = preference["DefaultValue"]
    }
    UserDefaults.standard.register(defaults: defaultsToRegister)
}
//
//extension AppDelegate: SAPUIWebViewDelegate {
//
//    func webView(_ webView: UIWebView, handleFailedLoadWithError error: Error) -> Error? {
//
//        let myError = error as NSError
//        let code = myError.code
//        let domain = myError.domain
//
//        // Filter for -999 "cancelled" error from SAML component. (domain: NSURLErrorDomain, code: -999)
//        if domain == NSURLErrorDomain {
//            if code == NSURLErrorCancelled {
//                return nil
//            }
//        }
//
//        return error
//    }
//
//}


