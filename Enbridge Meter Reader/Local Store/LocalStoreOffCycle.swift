//
//  LocalStoreOffCycle.swift
//  Enbridge Meter Reader
//
//  Created by Jerry on 2018-06-08.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import Foundation
import SAPFoundation
import SAPOfflineOData
import SAPCommon
import SAPOData
import proxyclasses

class LocalStoreOffCycle: OfflineODataDelegate,LocalStoreMeterInfoProtocol {
    
    private var offlineSP: M1Ec1<OfflineODataProvider>? = nil
    private let logger = Logger.shared(named: "Local Store OffCycle")
    private var isStoreOpened : Bool
    
    init(urlSession: SAPURLSession) {
        
        var offlineParameters = OfflineODataParameters()
        offlineParameters.enableRepeatableRequests = true
        isStoreOpened = false;
        
        offlineParameters.customHeaders = [
            "MMR_EngineerID" : Utilities.sharedInstance.userName ?? "",
            //"TIME_INTERVAL_START" : /*"2018-04-21T00:00:00"*/ Utilities.sharedInstance.fetchCurrentSystemDate().0,
            //"TIME_INTERVAL_END" :  /*"2018-04-30T00:00:00"*/Utilities.sharedInstance.fetchCurrentSystemDate().1,
            "MMR_OffCycle" : "true",
            "MMR_Periodic" : "false",
            "MMR_OutCard" : "false"
            
        ]

        
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let completePath = path.appending("/" + Utilities.sharedInstance.userName!)
        offlineParameters.storePath =  NSURL.fileURL(withPath: completePath)
        
        offlineParameters.storeName = "OffCycle"
        // create offline OData provider
        let offlineODataProvider = try! OfflineODataProvider(
            serviceRoot: URL(string: "\(environmentURL)/\(clickDestination)")!,
            parameters: offlineParameters,
            sapURLSession: urlSession,
            delegate: self
        )
        
        try! offlineODataProvider.add(
            definingQuery: OfflineODataDefiningQuery(
                name: CollectionType.operationSet.rawValue,
                query: "/\(CollectionType.operationSet.rawValue)",
                automaticallyRetrievesStreams: false
        ))
        
        self.offlineSP = M1Ec1(provider: offlineODataProvider) // For Read Meter
        //self.offlineSP?.provider.serviceOptions.supportsPatch = false
    }
    
    func destory () {
        offlineSP = nil
    }
    
    func returnSP() -> M1Ec1<OfflineODataProvider> {
        return self.offlineSP!
    }
    
    func download(completionHandler: @escaping (Error?) -> Void = {error in}){
        /* Read Meter Info Offline */
        self.offlineSP?.open { error in
            guard error == nil else {
                //print(error.debugDescription)
                LogHelper.shared?.error("Offline offcycle open failed: Reason: \(String(describing: error))", loggerObject: self.logger)
                completionHandler(error)
                return
            }
            
            self.isStoreOpened = true
            
            self.offlineSP?.download { error in
                guard error == nil else {
                    //Error occurred
                    LogHelper.shared?.error("Offline offcycle download failed: Reason: \(String(describing: error))", loggerObject: self.logger)
                    completionHandler(error)
                    return
                }
                LogHelper.shared?.info("Offline offcycle download ok", loggerObject: self.logger)
                completionHandler(nil)
            }
        }
    }
    
    func upload(completionHandler: @escaping (Error?) -> Void) {
        
        /* Read Meter Info Offline */
        self.offlineSP?.open { error in
            guard error == nil else {
                //print(error.debugDescription)
                LogHelper.shared?.error("Offline OffCyle open failed: Reason: \(String(describing: error))", loggerObject: self.logger)
                completionHandler(error)
                return
            }
            
            self.isStoreOpened = true
            
            self.reTouchRequestEntity()
            
            self.offlineSP?.upload { error in
                guard error == nil else {
                    //Error occurred
                    LogHelper.shared?.error("Offline OffCyle upload failed: Reason: \(String(describing: error))", loggerObject: self.logger)
                    //self.closeOfflineStore()
                    completionHandler(error)
                    return
                }
                LogHelper.shared?.info("Offline OffCyle upload ok", loggerObject: self.logger)
                self.clearErrorArchive()
                //unblock the offcycle cell if its been blocked
                Model.sharedInstance.routes[GlobalConstants.OFFCYCLE_ROUTE_ID]?.blockCell = false
                completionHandler(nil)
            }
            
        }
    }
    
    public func closeOfflineStore() {
        if isStoreOpened {
            do {
                try self.offlineSP?.close()
                isStoreOpened = false
            } catch {
                logger.error("Offline Store closing failed")
            }
        }
    }
    
    public func openOfflineStore(){
        if !isStoreOpened {
            self.offlineSP?.open{error in
                guard error == nil else {
                    //print(error.debugDescription)
                    self.logger.error("Offline failed to open")
                    return
                }}
            isStoreOpened = true
        }
    }
    
    public func clearErrorArchive() {
        do {
            let ea = self.offlineSP?.entitySet(withName: "ErrorArchive")
            let query = DataQuery().selectAll().from(ea!)
            let errors = try self.offlineSP?.executeQuery(query).entityList()
            if let errorArray =  errors?.toArray() {
                if errorArray.count > 0 {
                    print(errorArray.first ?? "nothing")
                    try self.offlineSP?.deleteEntity(errorArray.first!)
                }
            }
        }catch {
            print (error)
        }
        
    }
    
    public func reTouchRequestEntity(){
        do {
            if let sp = self.offlineSP {
                let errorArchiveSet = sp.entitySet(withName: "ErrorArchive")
                let errorArchiveType: EntityType = errorArchiveSet.entityType
                let requestEntityProp = errorArchiveType.property(withName: "RequestEntity")
                let errorArchiveQuery = DataQuery().from(errorArchiveSet)
                let errors = try sp.executeQuery(errorArchiveQuery).entityList()
                
                for error in errors {
                    try sp.loadProperty(requestEntityProp, into: error)
                    let entity = requestEntityProp.entityValue(from: error)
                    try sp.updateEntity(entity)
                }
                
            }
                
        }catch {
            print (error)
        }

        
    }
    
    public func reTouchEvent() {
        do {
            let query = DataQuery().filter(OfflineODataQueryFunction.inErrorState())
            let eventsInError = try self.offlineSP?.fetchOperationSet(matching: query)
            
            if let events = eventsInError {
                for event in events {
                    try self.offlineSP?.updateEntity(event)
                }
            }
        }catch {
            print(error)
        }
        
    }
    
    
    func offlineODataProvider(_ provider: OfflineODataProvider, didUpdateDownloadProgress progress: OfflineODataProgress) {
        // print("Donwload progress" + progress)
        LocalStoreManager.dataBytes =  LocalStoreManager.dataBytes + progress.bytesReceived
    }
    
    func offlineODataProvider(_ provider: OfflineODataProvider, didUpdateFileDownloadProgress progress: OfflineODataFileDownloadProgress) {
        ////print("in did update file download progress");
        //print(progress)
    }
    
    func offlineODataProvider(_ provider: OfflineODataProvider, didUpdateUploadProgress progress: OfflineODataProgress) {
        //        //print("in did update upload progress");
        LocalStoreManager.dataBytes =  LocalStoreManager.dataBytes + progress.bytesReceived
        GetWorkOrderFromBackEnd_SAP_Click.byteSent = GetWorkOrderFromBackEnd_SAP_Click.byteSent + progress.bytesReceived  
    }
    
    func offlineODataProvider(_ provider: OfflineODataProvider, requestDidFail request: OfflineODataFailedRequest) {
        //        //print("in did request fail");
    }
    
    func offlineODataProvider(_ provider: OfflineODataProvider, stateDidChange newState: OfflineODataStoreState) {
        //        //print("in state did change");
        // print(newState)
        // LocalStoreManager.status = "Offcycle " +
        
        if newState.contains(.opening) {
            LocalStoreManager.status = "Preparing the Ipad data"
        }else if newState.contains(.initializing){
            LocalStoreManager.status = "Initializing the Ipad data"
        }else if newState.contains(.initialCommunication){
            LocalStoreManager.status = "Initializing the communication"
        }else if newState.contains(.fileDownloading){
            LocalStoreManager.status = "Downloading files"
        }else if newState.contains(.downloading){
            LocalStoreManager.status = "Downloading data"
        }else if newState.contains(.uploading){
            LocalStoreManager.status = "Uploading data"
        }else if newState.contains(.open){
            LocalStoreManager.status = "Ipad database is opened"
        }else if newState.contains(.closed){
            LocalStoreManager.status = "Ipad database is closed"
        }
    }
    
}
