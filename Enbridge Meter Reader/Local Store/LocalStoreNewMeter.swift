//
//  LocalStoreNewMeter.swift
//  Enbridge Meter Reader
//
//  Created by Jerry on 2018-06-08.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import Foundation
import SAPFoundation
import SAPOfflineOData
import SAPCommon
import SAPOData
import proxyclasses

class LocalStoreNewMeter: OfflineODataDelegate {
    
    static var offlineSP: ZDM<OfflineODataProvider>?
    static var offlineLS: LocalStoreNewMeter? = nil
    private let logger = Logger.shared(named: "Local Store NewMeter")
    private var isStoreOpened : Bool
    
    init(_ urlSession: SAPURLSession) {
        
        var offlineParameters = OfflineODataParameters()
        offlineParameters.enableRepeatableRequests = true
        isStoreOpened = false;
        
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let completePath = path.appending("/" + Utilities.sharedInstance.userName!)
        offlineParameters.storePath =  NSURL.fileURL(withPath: completePath)
        
        offlineParameters.storeName = "OfflineNewMeter"
        // create offline OData provider
        let offlineODataProvider = try! OfflineODataProvider(
            serviceRoot: URL(string: "\(environmentURL)/\(newServiceDestination)")!,
            parameters: offlineParameters,
            sapURLSession: urlSession,
            delegate: self
        )
        
        try! offlineODataProvider.add(
            definingQuery: OfflineODataDefiningQuery(
                name: CollectionType.newMeterSet.rawValue,
                query: "/\(CollectionType.newMeterSet.rawValue)",
                automaticallyRetrievesStreams: false
            )
        )
        
        LocalStoreNewMeter.offlineSP = ZDM(provider: offlineODataProvider) // For Read Meter
        LocalStoreNewMeter.offlineLS = self
        
    }
    
    class func returnSP() -> ZDM<OfflineODataProvider> {
        return LocalStoreNewMeter.offlineSP!
    }
    
    func downloadFromDB(){
        
        /* Read Meter Info Offline */
        LocalStoreNewMeter.offlineSP?.open { error in
            guard error == nil else {
                //print(error.debugDescription)
                LogHelper.shared?.error("Offline newmeter open failed: Reason: \(String(describing: error))", loggerObject: self.logger)
                return
            }
            
            self.isStoreOpened = true
            
            LocalStoreNewMeter.offlineSP?.download { error in
                guard error == nil else {
                    //Error occurred
                    LogHelper.shared?.error("Offline newmeter download failed: Reason: \(String(describing: error))", loggerObject: self.logger)
                    return
                }
                LogHelper.shared?.info("Offline newmeter download ok", loggerObject: self.logger)
            }
        }
    }
    
    func uploadToDB() {
        LocalStoreNewMeter.offlineSP?.open { error in
            guard error == nil else {
                //print(error.debugDescription)
                LogHelper.shared?.error("Offline newmeter open failed: Reason: \(String(describing: error))", loggerObject: self.logger)
                return
            }
            
            self.isStoreOpened = true
            self.reTouchRequestEntity()
            LocalStoreNewMeter.offlineSP?.upload { error in
                guard error == nil else {
                    //Error occurred
                    LogHelper.shared?.error("Offline newmeter upload failed: Reason: \(String(describing: error))", loggerObject: self.logger)
                    return
                }
                LogHelper.shared?.info("Offline newmeter upload ok", loggerObject: self.logger)
                self.clearErrorArchive()
            }
        }
    }
    
    public func clearErrorArchive() {
        do {
            let ea = LocalStoreNewMeter.offlineSP?.entitySet(withName: "ErrorArchive")
            let query = DataQuery().selectAll().from(ea!)
            let errors = try LocalStoreNewMeter.offlineSP?.executeQuery(query).entityList()
            if let errorArray =  errors?.toArray() {
                if errorArray.count > 0 {
                    print(errorArray.first ?? "nothing")
                    try LocalStoreNewMeter.offlineSP?.deleteEntity(errorArray.first!)
                }
            }
        }catch {
            print (error)
        }
        
    }
    
    public func reTouchRequestEntity(){
        do {
            if let sp = LocalStoreNewMeter.offlineSP {
                let errorArchiveSet = sp.entitySet(withName: "ErrorArchive")
                let errorArchiveType: EntityType = errorArchiveSet.entityType
                let requestEntityProp = errorArchiveType.property(withName: "RequestEntity")
                let errorArchiveQuery = DataQuery().from(errorArchiveSet)
                let errors = try sp.executeQuery(errorArchiveQuery).entityList()
                
                for error in errors {
                    try sp.loadProperty(requestEntityProp, into: error)
                    let entity = requestEntityProp.entityValue(from: error)
                    try sp.updateEntity(entity)
                }
                
            }
            
        }catch {
            print (error)
        }
        
        
    }
    
    public func closeOfflineStore() {
        if isStoreOpened {
            do {
                try LocalStoreNewMeter.offlineSP?.close()
                isStoreOpened = false
            } catch {
                logger.error("Offline Store closing failed")
            }
        }
    }
    
    public func openOfflineStore(){
        if !isStoreOpened {
            LocalStoreNewMeter.offlineSP?.open{error in
                guard error == nil else {
                    //print(error.debugDescription)
                    self.logger.error("Offline failed to open")
                    return
                }}
            isStoreOpened = true
        }
    }
    
    func offlineODataProvider(_ provider: OfflineODataProvider, didUpdateDownloadProgress progress: OfflineODataProgress) {
        ////print("in did update donwload progress");
    }
    
    func offlineODataProvider(_ provider: OfflineODataProvider, didUpdateFileDownloadProgress progress: OfflineODataFileDownloadProgress) {
        ////print("in did update file download progress");
    }
    
    func offlineODataProvider(_ provider: OfflineODataProvider, didUpdateUploadProgress progress: OfflineODataProgress) {
        //        //print("in did update upload progress");
    }
    
    func offlineODataProvider(_ provider: OfflineODataProvider, requestDidFail request: OfflineODataFailedRequest) {
        //        //print("in did request fail");
    }
    
    func offlineODataProvider(_ provider: OfflineODataProvider, stateDidChange newState: OfflineODataStoreState) {
        //        //print("in state did change");
    }
    
}
