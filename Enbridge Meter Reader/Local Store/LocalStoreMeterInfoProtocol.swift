//
//  LocalStoreObject.swift
//  Enbridge Meter Reader
//
//  Created by Jerry on 2018-06-11.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import Foundation

protocol LocalStoreMeterInfoProtocol {
    func download(completionHandler: @escaping (Error?) -> Void)
    func upload(completionHandler: @escaping (Error?) -> Void)
    func closeOfflineStore()
}

