//
//  LocalStoreUpdateStatusServ.swift
//  Enbridge Meter Reader
//
//  Created by Jerry on 2018-06-08.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import Foundation
import SAPFoundation
import SAPOfflineOData
import SAPCommon
import SAPOData
import proxyclasses

class LocalStoreUpdateStatusServ: OfflineODataDelegate {
    
    static var offlineSP: StatusChange<OfflineODataProvider>? = nil
    static var shareInstance: LocalStoreUpdateStatusServ? = nil
    private let logger = Logger.shared(named: "Local Store Change Status")
    private var isStoreOpened : Bool
    
    init(_ urlSession: SAPURLSession) {
        
        var offlineParameters = OfflineODataParameters()
        offlineParameters.enableRepeatableRequests = true
        isStoreOpened = false;
        
        offlineParameters.customHeaders = [
            "MMR_EngineerID" : Utilities.sharedInstance.userName ?? "",
        ]
        
        
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let completePath = path.appending("/" + Utilities.sharedInstance.userName!)
        offlineParameters.storePath =  NSURL.fileURL(withPath: completePath)
        
        offlineParameters.storeName = "ChangeStatus"
        // create offline OData provider
        let offlineODataProvider = try! OfflineODataProvider(
            serviceRoot: URL(string: "\(environmentURL)/\(changeStatusDestination)")!,
            parameters: offlineParameters,
            sapURLSession: urlSession,
            delegate: self
        )
        
        try! offlineODataProvider.add(
            definingQuery: OfflineODataDefiningQuery(
                name: CollectionType.changeStatusSet.rawValue,
                query: "/\(CollectionType.changeStatusSet.rawValue)",
                automaticallyRetrievesStreams: false
            )
        )
        
        LocalStoreUpdateStatusServ.offlineSP = StatusChange(provider: offlineODataProvider) // For Read Meter
        LocalStoreUpdateStatusServ.shareInstance = self
        
    }
    
    class func returnSP() -> StatusChange<OfflineODataProvider> {
        return LocalStoreUpdateStatusServ.offlineSP!
    }
    
    //Private functions
    func createEntryInTable(entry:MassUpdateTasks) throws {
        
        let headers = HTTPHeaders()
        headers.setHeader(withName: "OfflineOData.NonMergeable", value: "yes")
        headers.setHeader(withName: "OfflineOData.RemoveAfterUpload", value:"true")
        let options = OfflineODataRequestOptions()
        options.sendEmptyUpdate = true
        let sp = LocalStoreUpdateStatusServ.offlineSP
        var optSet:[MassUpdateTasks]? = nil
        do {//query the UDB file first and check if this one exist
            let optEntity = sp!.entitySet(withName: "MassUpdateTasksSet")
            let query = DataQuery().selectAll().filter(MassUpdateTasks.mru.equal(entry.mru!)).from(optEntity)
            optSet = MassUpdateTasks.array(from: try sp!.executeQuery(query).entityList())
        }catch {
            return
        }
        if let set = optSet {
            if set.count > 0 {
                //print("This file exists")
               return
            }else{
                try LocalStoreUpdateStatusServ.offlineSP!.createEntity(entry,headers:headers, options:options)
            }
        }
        
    }
    
    
    func downloadFromDB(){
        
        /* Read Meter Info Offline */
        LocalStoreUpdateStatusServ.offlineSP?.open { error in
            guard error == nil else {
                //print(error.debugDescription)
                self.logger.error("Offline failed to open")
                return
            }
            
            self.isStoreOpened = true
            
            LocalStoreUpdateStatusServ.offlineSP?.download { error in
                guard error == nil else {
                    //Error occurred
                    LogHelper.shared?.error("Offline changeStatus download failed: Reason: \(String(describing: error))", loggerObject: self.logger)
                    return
                }
                LogHelper.shared?.info("Offline changeStatus download ok", loggerObject: self.logger)
            }
        }
    }
    
    func uploadToDB(completionHandler: @escaping(Error?)->Void = {error in }) {
        LocalStoreUpdateStatusServ.offlineSP?.open { error in
            guard error == nil else {
                //print(error.debugDescription)
                LogHelper.shared?.error("Offline changeStatus open failed: Reason: \(String(describing: error))", loggerObject: self.logger)
                completionHandler(error)
                return
            }
            
            self.isStoreOpened = true
            
            self.reTouchRequestEntity()
            
            LocalStoreUpdateStatusServ.offlineSP?.upload { error in
                guard error == nil else {
                    //Error occurred
                    LogHelper.shared?.error("Offline changeStatus upload failed: Reason: \(String(describing: error))", loggerObject: self.logger)
                    completionHandler(error)
                    return
                }
                LogHelper.shared?.info("Offline changeStatus upload ok", loggerObject: self.logger)
                self.clearErrorArchive()
                completionHandler(nil)
            }
        }
    }
    
    public func reTouchRequestEntity(){
        do {
            if let sp = LocalStoreUpdateStatusServ.offlineSP {
                let errorArchiveSet = sp.entitySet(withName: "ErrorArchive")
                let errorArchiveType: EntityType = errorArchiveSet.entityType
                let requestEntityProp = errorArchiveType.property(withName: "RequestEntity")
                let errorArchiveQuery = DataQuery().from(errorArchiveSet)
                let errors = try sp.executeQuery(errorArchiveQuery).entityList()
                
                for error in errors {
                    try sp.loadProperty(requestEntityProp, into: error)
                    let entity = requestEntityProp.entityValue(from: error)
                    try sp.updateEntity(entity)
                }
                
            }
            
        }catch {
            print (error)
        }
    }
    
    public func clearErrorArchive() {
        do {
            if let sp = LocalStoreUpdateStatusServ.offlineSP {
                let ea = sp.entitySet(withName: "ErrorArchive")
                let query = DataQuery().selectAll().from(ea)
                let errors = try sp.executeQuery(query).entityList()
                let errorArray = errors.toArray()
                if errorArray.count > 0 {
                    print(errorArray.first ?? "nothing")
                    try sp.deleteEntity(errorArray.first!)
                }
                
            }
        }catch {
            print (error)
        }
        
    }
    
    public func closeOfflineStore() {
        if isStoreOpened {
            do {
                try LocalStoreUpdateStatusServ.offlineSP?.close()
                isStoreOpened = false
            } catch {
                logger.error("Offline Store closing failed")
            }
        }
    }
    
    public func openOfflineStore(){
        if !isStoreOpened {
            LocalStoreUpdateStatusServ.offlineSP?.open{error in
                guard error == nil else {
                    //print(error.debugDescription)
                    self.logger.error("Offline failed to open")
                    return
                }}
            isStoreOpened = true
        }
    }
    
    
    func offlineODataProvider(_ provider: OfflineODataProvider, didUpdateDownloadProgress progress: OfflineODataProgress) {
        ////print("in did update donwload progress");
        LocalStoreManager.dataBytes =  LocalStoreManager.dataBytes + progress.bytesReceived
    }
    
    func offlineODataProvider(_ provider: OfflineODataProvider, didUpdateFileDownloadProgress progress: OfflineODataFileDownloadProgress) {
        ////print("in did update file download progress");
    }
    
    func offlineODataProvider(_ provider: OfflineODataProvider, didUpdateUploadProgress progress: OfflineODataProgress) {
        //        //print("in did update upload progress");
        LocalStoreManager.dataBytes =  LocalStoreManager.dataBytes + progress.bytesReceived
        GetWorkOrderFromBackEnd_SAP_Click.byteSent = GetWorkOrderFromBackEnd_SAP_Click.byteSent + progress.bytesReceived
    }
    
    func offlineODataProvider(_ provider: OfflineODataProvider, requestDidFail request: OfflineODataFailedRequest) {
        //        //print("in did request fail");
    }
    
    func offlineODataProvider(_ provider: OfflineODataProvider, stateDidChange newState: OfflineODataStoreState) {
        //        //print("in state did change");
    }
    
}

