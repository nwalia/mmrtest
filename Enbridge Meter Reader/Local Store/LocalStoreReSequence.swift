//
//  LocalStoreReSequence.swift
//  Enbridge Meter Reader
//
//  Created by Jerry on 2018-06-08.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import Foundation
import SAPFoundation
import SAPOfflineOData
import SAPCommon
import SAPOData
import proxyclasses

class LocalStoreReSequence: OfflineODataDelegate {
    
    static var offlineSP: Resequence<OfflineODataProvider>? = nil
    static var offlineLS: LocalStoreReSequence? = nil
    private let logger = Logger.shared(named: "Local Store ReSequence")
    private var isStoreOpened : Bool
    
    init(_ urlSession: SAPURLSession) {
        
        var offlineParameters = OfflineODataParameters()
        offlineParameters.enableRepeatableRequests = true
        isStoreOpened = false;
        
        
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let completePath = path.appending("/" + Utilities.sharedInstance.userName!)
        offlineParameters.storePath =  NSURL.fileURL(withPath: completePath)
        
        offlineParameters.storeName = "OfflineReSequence"
        offlineParameters.enableTransactionBuilder = true
        // create offline OData provider
        let offlineODataProvider = try! OfflineODataProvider(
            serviceRoot: URL(string: "\(environmentURL)/\(reSequenceDestination)")!,
            parameters: offlineParameters,
            sapURLSession: urlSession,
            delegate: self
        )
        
        try! offlineODataProvider.add(
            definingQuery: OfflineODataDefiningQuery(
                name: CollectionType.sequenceSet.rawValue,
                query: "/\(CollectionType.sequenceSet.rawValue)",
                automaticallyRetrievesStreams: false
            )
        )
        
        LocalStoreReSequence.offlineSP = Resequence(provider: offlineODataProvider) // For Read Meter
        LocalStoreReSequence.offlineLS = self
        
    }
    
    class func returnSP() -> Resequence<OfflineODataProvider> {
        return LocalStoreReSequence.offlineSP!
    }
    
    //Private functions
    func createSequenceInTable(seq:Reseq,transID:String) -> ChangeSet {
        
        let changeSet = ChangeSet()
        let headers = HTTPHeaders()
        headers.setHeader(withName: "OfflineOData.NonMergeable", value: "yes")
        headers.setHeader(withName: "OfflineOData.RemoveAfterUpload", value:"true" )
        let options = OfflineODataRequestOptions()
        options.sendEmptyUpdate = true
        changeSet.createEntity(seq,headers:headers, options:options)
        
        return changeSet
    }
    
    
    func downloadFromDB(){
        
        /* Read Meter Info Offline */
        LocalStoreReSequence.offlineSP?.open { error in
            guard error == nil else {
                //print(error.debugDescription)
                self.logger.error("Offline failed to open")
                return
            }
            
            self.isStoreOpened = true
            
            LocalStoreReSequence.offlineSP?.download { error in
                guard error == nil else {
                    //Error occurred
                    LogHelper.shared?.error("Offline resequence download failed: Reason: \(String(describing: error))", loggerObject: self.logger)
                    return
                }
                LogHelper.shared?.info("Offline resequence download ok", loggerObject: self.logger)
            }
        }
    }
    
    func uploadToDB() {
        LocalStoreReSequence.offlineSP?.open { error in
            guard error == nil else {
                //print(error.debugDescription)
                LogHelper.shared?.error("Offline resequence open failed: Reason: \(String(describing: error))", loggerObject: self.logger)
                return
            }
            
            self.isStoreOpened = true
            
            LocalStoreReSequence.offlineSP?.upload { error in
                guard error == nil else {
                    //Error occurred
                    LogHelper.shared?.error("Offline resequence upload failed: Reason: \(String(describing: error))", loggerObject: self.logger)
                    return
                }
                LogHelper.shared?.info("Offline resequence upload ok", loggerObject: self.logger)
            }
        }
    }
    
    public func closeOfflineStore() {
        if isStoreOpened {
            do {
                try LocalStoreReSequence.offlineSP?.close()
                isStoreOpened = false
            } catch {
                logger.error("Offline Store closing failed")
            }
        }
    }
    
    public func openOfflineStore(){
        if !isStoreOpened {
            LocalStoreReSequence.offlineSP?.open{error in
                guard error == nil else {
                    //print(error.debugDescription)
                    self.logger.error("Offline failed to open")
                    return
                }}
            isStoreOpened = true
        }
    }
    
    
    func offlineODataProvider(_ provider: OfflineODataProvider, didUpdateDownloadProgress progress: OfflineODataProgress) {
        ////print("in did update donwload progress");
    }
    
    func offlineODataProvider(_ provider: OfflineODataProvider, didUpdateFileDownloadProgress progress: OfflineODataFileDownloadProgress) {
        ////print("in did update file download progress");
    }
    
    func offlineODataProvider(_ provider: OfflineODataProvider, didUpdateUploadProgress progress: OfflineODataProgress) {
        //        //print("in did update upload progress");
    }
    
    func offlineODataProvider(_ provider: OfflineODataProvider, requestDidFail request: OfflineODataFailedRequest) {
        //        //print("in did request fail");
    }
    
    func offlineODataProvider(_ provider: OfflineODataProvider, stateDidChange newState: OfflineODataStoreState) {
        //        //print("in state did change");
    }
    
}
