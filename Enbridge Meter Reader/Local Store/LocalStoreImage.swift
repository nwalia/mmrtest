//
//  LocalStoreImage.swift
//  Enbridge Meter Reader
//
//  Created by Jerry on 2018-06-08.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import Foundation
import SAPFoundation
import SAPOfflineOData
import SAPCommon
import SAPOData
import proxyclasses

class LocalStoreImage: OfflineODataDelegate {
    
    static var offlineSP: I1<OfflineODataProvider>? = nil
    static var offlineLS: LocalStoreImage? = nil
    private let logger = Logger.shared(named: "Local Store StoreImage")
    private var isStoreOpened : Bool

    init(_ urlSession: SAPURLSession) {
        
        var offlineParameters = OfflineODataParameters()
        offlineParameters.enableRepeatableRequests = true
        isStoreOpened = false;
        
        
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let completePath = path.appending("/" + Utilities.sharedInstance.userName!)
        offlineParameters.storePath =  NSURL.fileURL(withPath: completePath)
        
        offlineParameters.storeName = "OfflineImage"
        // create offline OData provider
        let offlineODataProvider = try! OfflineODataProvider(
            serviceRoot: URL(string: "\(environmentURL)/\(imageDestination)")!,
            parameters: offlineParameters,
            sapURLSession: urlSession,
            delegate: self
        )
        
        try! offlineODataProvider.add(
            definingQuery: OfflineODataDefiningQuery(
                name: CollectionType.imageSet.rawValue,
                query: "/\(CollectionType.imageSet.rawValue)",
                automaticallyRetrievesStreams: false
            )
        )
        
        LocalStoreImage.offlineSP = I1(provider: offlineODataProvider) // For Read Meter
        LocalStoreImage.offlineLS = self
        
    }
    
   class func returnSP() -> I1<OfflineODataProvider> {
        return LocalStoreImage.offlineSP!
    }
    
    
    func downloadFromDB(){
        
        /* Read Meter Info Offline */
        LocalStoreImage.offlineSP?.open { error in
            guard error == nil else {
                //print(error.debugDescription)
                self.logger.error("Offline failed to open")
                return
            }
            
            self.isStoreOpened = true
            
            LocalStoreImage.offlineSP?.download { error in
                guard error == nil else {
                    //Error occurred
                    LogHelper.shared?.error("Offline image download failed: Reason: \(String(describing: error))", loggerObject: self.logger)
                    return
                }
                LogHelper.shared?.info("Offline image download ok", loggerObject: self.logger)
            }
        }
    }
    
    func uploadToDB() {
        LocalStoreImage.offlineSP?.open { error in
            guard error == nil else {
                //print(error.debugDescription)
                LogHelper.shared?.error("Offline image open failed: Reason: \(String(describing: error))", loggerObject: self.logger)
                return
            }
            
            self.isStoreOpened = true
            self.reTouchRequestEntity()
            
            LocalStoreImage.offlineSP?.upload { error in
                guard error == nil else {
                    //Error occurred
                    LogHelper.shared?.error("Offline image upload failed: Reason: \(String(describing: error))", loggerObject: self.logger)
                    return
                }
                LogHelper.shared?.info("Offline image download ok", loggerObject: self.logger)
                self.clearErrorArchive()
            }
        }
    }
    
    public func closeOfflineStore() {
        if isStoreOpened {
            do {
                try LocalStoreImage.offlineSP?.close()
                isStoreOpened = false
            } catch {
                logger.error("Offline Store closing failed")
            }
        }
    }
    
    public func openOfflineStore(){
        if !isStoreOpened {
            LocalStoreImage.offlineSP?.open{error in
                guard error == nil else {
                    //print(error.debugDescription)
                    self.logger.error("Offline failed to open")
                    return
                }}
            isStoreOpened = true
        }
    }
    
    public func clearErrorArchive() {
        do {
            let ea = LocalStoreNewMeter.offlineSP?.entitySet(withName: "ErrorArchive")
            let query = DataQuery().selectAll().from(ea!)
            let errors = try LocalStoreNewMeter.offlineSP?.executeQuery(query).entityList()
            if let errorArray =  errors?.toArray() {
                if errorArray.count > 0 {
                    print(errorArray.first ?? "nothing")
                    try LocalStoreNewMeter.offlineSP?.deleteEntity(errorArray.first!)
                }
            }
        }catch {
            print (error)
        }
        
    }
    
    public func reTouchRequestEntity(){
        do {
            if let sp = LocalStoreNewMeter.offlineSP {
                let errorArchiveSet = sp.entitySet(withName: "ErrorArchive")
                let errorArchiveType: EntityType = errorArchiveSet.entityType
                let requestEntityProp = errorArchiveType.property(withName: "RequestEntity")
                let errorArchiveQuery = DataQuery().from(errorArchiveSet)
                let errors = try sp.executeQuery(errorArchiveQuery).entityList()
                
                for error in errors {
                    try sp.loadProperty(requestEntityProp, into: error)
                    let entity = requestEntityProp.entityValue(from: error)
                    try sp.updateEntity(entity)
                }
                
            }
            
        }catch {
            print (error)
        }
        
        
    }

    
    func offlineODataProvider(_ provider: OfflineODataProvider, didUpdateDownloadProgress progress: OfflineODataProgress) {
        ////print("in did update donwload progress");
    }
    
    func offlineODataProvider(_ provider: OfflineODataProvider, didUpdateFileDownloadProgress progress: OfflineODataFileDownloadProgress) {
        ////print("in did update file download progress");
    }
    
    func offlineODataProvider(_ provider: OfflineODataProvider, didUpdateUploadProgress progress: OfflineODataProgress) {
        //        //print("in did update upload progress");
    }
    
    func offlineODataProvider(_ provider: OfflineODataProvider, requestDidFail request: OfflineODataFailedRequest) {
        //        //print("in did request fail");
    }
    
    func offlineODataProvider(_ provider: OfflineODataProvider, stateDidChange newState: OfflineODataStoreState) {
        //        //print("in state did change");
    }
    
}
