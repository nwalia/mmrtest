
//
// OnboardingManager.swift
// Dev-SAML
//
// Created by SAP Cloud Platform SDK for iOS Assistant application on 12/03/18
// --- Remove Comment --- //

import SAPFoundation
import SAPFioriFlows
import SAPFiori
import SAPCommon
import SAPOData
import WebKit

protocol OnboardingManagerDelegate {
    /// Called either when Onboarding or Restoring is successfully executed.
    func onboarded(onboardingContext: OnboardingContext)
}

// Environment variables (from info.plist and build settings)
var environmentURL = "NONE"
var newServiceURL = "NONE"
var sapAppId = "NONE"

// Destination variables (from info.plist)
var clickDestination = "NONE"
var imageDestination = "NONE"
var newServiceDestination = "NONE"
var reSequenceDestination = "NONE"
var changeStatusDestination = "NONE"

// Oauth variables (from info.plist)
var oauthAuthorizationEndpoint = "NONE"
var oauthClientID = "NONE"
var oauthGrantType = "NONE"
var oauthRedirectURL = "NONE"
var oauthEndUserUI = "NONE"
var oauthTokenEndpoint = "NONE"

class OnboardingManager {
    // MARK: - Singleton
    static let shared = OnboardingManager()
    private init() {}
    
    // MARK: - Properties
    private let logger = Logger.shared(named: "OnbardingManager")
    private let appDelegate = UIApplication.shared.delegate as! AppDelegate
    private let presentationDelegate = ModalUIViewControllerPresenter()
    private var credentialStore: CompositeCodableStoring!
    private var state: State = .onboarding
    
    //onBoardingContext
    private var onBoardingcontext:OnboardingContext? = nil
    
    var delegate: OnboardingManagerDelegate?
    var userRoles: SAPcpmsUserRoles!
    
    /// Steps executed during Onboarding.
    private var onboardingSteps: [OnboardingStep] {
        return [
            SAPcpmsSessionConfigurationStep(),
            //customSAMLAuthStep(),
            self.configuredOAuth2AuthenticationStep(),
            SAPcpmsSettingsDownloadStep(),
        ]
    }
    
    /// Steps executed during Restoring.
    private var restoringSteps: [OnboardingStep] {
        return [
            SAPcpmsSessionConfigurationStep(),
            //customSAMLAuthStep(),
            self.configuredOAuth2AuthenticationStep(),
            SAPcpmsSettingsDownloadStep(),
        ]
    }
    
    // MARK: - Steps
    
    // OAuth2AuthenticationStep
    private func configuredOAuth2AuthenticationStep() -> OAuth2AuthenticationStep {
        let presenter = FioriWKWebViewPresenter(webViewDelegate: self)
        self.removeCancelButton(presenter)
        let oAuth2AuthenticationStep = OAuth2AuthenticationStep(presenter: presenter)
        return oAuth2AuthenticationStep
    }
    
    private func removeCancelButton(_ presenter:FioriWKWebViewPresenter) {
        let samlBundle = Bundle(for: OAuth2AuthenticationStep.self)
        let localizedCancelButtonText = NSLocalizedString("CancelButtonText", tableName: "Example Table name", bundle: samlBundle, value: "", comment: "")
        presenter.webViewControllerConfigurationHandler = { webView in
            webView.cancelButton?.title = localizedCancelButtonText
            webView.cancelButton?.isEnabled = false
            webView.cancelButton?.tintColor = UIColor(red:0.33, green:0.33, blue:0.33, alpha:1.0)
            webView.navigationController?.navigationBar.barTintColor = UIColor(red:0.33, green:0.33, blue:0.33, alpha:1.0)
            return webView
        }
    }
    
//    // --- Confirm from Shreyas --- //
//    // Customized SAML Step
//    func customSAMLAuthStep() -> OnboardingStep {
//        let samlBundle = Bundle(for: SAMLAuthenticationStep.self)
//
//        let localizedInfoScreenText = NSLocalizedString("SAMLInfoScreenText", tableName: "Example Table name", bundle: samlBundle, value: "Validating SAML credentials", comment: "")
//        let localizedCancelButtonText = NSLocalizedString("CancelButtonText", tableName: "Example Table name", bundle: samlBundle, value: "", comment: "")
//
//        // Custom presenter with disabled/hidden cancel button
//        let presenter = FioriUIWebViewPresenter(webViewDelegate: UIApplication.shared.delegate as? SAPUIWebViewDelegate)
//        presenter.webViewControllerConfigurationHandler = { webView in
//            webView.cancelButton?.title = localizedCancelButtonText
//            webView.cancelButton?.isEnabled = false
//            webView.cancelButton?.tintColor = GlobalConstants.GRAY
//            webView.navigationController?.navigationBar.barTintColor = GlobalConstants.GRAY
//            return webView
//        }
//
//        let step = SAMLAuthenticationStep(presenter: presenter)
//        step.infoScreenText = localizedInfoScreenText
//
//        return step
//    }
    
    // Assign onboarding environment info based on the app environment specified in the Settings app.
    func assignEnvironmentInfo() {
        
         let environmentInfo = Bundle.main.object(forInfoDictionaryKey: "EnvironmentInfo-" + SettingsBundleHelper.environmentCode) as! NSDictionary
        
        environmentURL = environmentInfo["environmentURL"] as! String
        newServiceURL = environmentInfo["newServiceURL"] as! String
        sapAppId = environmentInfo["sapAppId"] as! String
        
        clickDestination = environmentInfo["clickDestination"] as! String
        imageDestination = environmentInfo["imageDestination"] as! String
        reSequenceDestination = environmentInfo["reSequenceDestination"] as! String
        changeStatusDestination = environmentInfo["massUpdateStausDestination"] as! String
        newServiceDestination = environmentInfo["newServiceDestination"] as! String
        oauthAuthorizationEndpoint = environmentInfo["oauthAuthorizationEndpoint"] as! String
        oauthClientID = environmentInfo["oauthClientID"] as! String
        oauthGrantType = environmentInfo["oauthGrantType"] as! String
        oauthRedirectURL = environmentInfo["oauthRedirectURL"] as! String
        oauthEndUserUI = environmentInfo["oauthEndUserUI"] as! String
        oauthTokenEndpoint = environmentInfo["oauthTokenEndpoint"] as! String
        
    }
    
    
    // MARK: - Onboarding
    /// Starts Onboarding or Restoring flow.
    /// - Note: This function changes the `rootViewController` to a splash screen before starting the flow.
    /// The `rootViewController` is expected to be switched back by the caller.
    func onboardOrRestore() {
        // Set the spalsh screen
        let splashViewController = FUIInfoViewController.createSplashScreenInstanceFromStoryboard()
        self.appDelegate.window!.rootViewController = splashViewController
        self.presentationDelegate.setSplashScreen(splashViewController)
        self.presentationDelegate.animated = true
        
        self.onboardOrRestoreWithoutSplashScreen()
    }
    
    /// Starts Onboarding or Restoring flow without displaying a splash screen.
    /// - Note: Should be called when the application screen is already hidden,
    /// for example after `onboardOrRestore` has already been called, like in case of reseting.
    private func onboardOrRestoreWithoutSplashScreen() {
        self.state = .onboarding
        var context = OnboardingContext(presentationDelegate: presentationDelegate)
        logger.logLevel = .debug
        
        ////print("! environmentURL \(environmentURL)")
        ////print("! newServiceURL \(newServiceURL)")
        ////print("! SAPAppID \(sapAppId)")
        
        // Set up items in context that are required to skip welcome screen (SAML Parameters)
        
        let settingsParameters = SAPcpmsSettingsParameters(backendURL: URL(string: environmentURL)!, applicationID: sapAppId)
        userRoles = SAPcpmsUserRoles(sapURLSession: context.sapURLSession, settingsParameters: settingsParameters)
        
        let oauthParameters = OAuth2AuthenticationParameters(authorizationEndpointURL: URL(string: oauthAuthorizationEndpoint)!, clientID: oauthClientID, redirectURL: URL(string: oauthRedirectURL)!, tokenEndpointURL: URL(string: oauthTokenEndpoint)!)
        
        
        context.info[OnboardingInfoKey.sapcpmsSettingsParameters] = settingsParameters
        
        context.info[OnboardingInfoKey.authenticationURL] = URL(string: "\(environmentURL)/\(clickDestination)")!
            
        context.info[.oauth2AuthenticationParameters] = oauthParameters
        
        // Check if we have an existing onboardingID
        if let onboardingID = self.onboardingID {
            LogHelper.shared?.info("Restoring...", loggerObject: self.logger)
            
            context.onboardingID = onboardingID
            OnboardingFlowController.restore(on: self.restoringSteps, context: context, completionHandler: self.restoreCompleted)
            
        } else {
            LogHelper.shared?.info("Onboarding...", loggerObject: self.logger)
            OnboardingFlowController.onboard(on: self.onboardingSteps, context: context, completionHandler: self.onboardingCompleted)
        }
        
        self.showSelectRouteScreen()
    }
    
    /// Resets the Onboarding flow and than calls `onboardOrRestoreWithoutSplashScreen` to start a new flow. Call this for log out.
    func resetOnboarding() {
        LogHelper.shared?.info("Resetting...", loggerObject: self.logger)
        
        guard let onboardingID = self.onboardingID else {
            self.clearUserData()
            self.onboardOrRestoreWithoutSplashScreen()
            return
        }
        
        let configuration = URLSessionConfiguration.default
        
        configuration.timeoutIntervalForRequest = 600
        
        configuration.timeoutIntervalForResource = 600
        
        let sapURLSession = SAPURLSession(configuration: configuration)
        let context = OnboardingContext(onboardingID: onboardingID, sapURLSession:sapURLSession)
        OnboardingFlowController.reset(on: self.restoringSteps, context: context) {
            self.clearUserData()
            self.onboardingID = nil
            self.onboardOrRestoreWithoutSplashScreen()
        }
        
    }
    
    private func showSelectRouteScreen() {
        // Check if logout button pressed
        if GlobalVariables.logoutPressed == true {
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "MainNav", bundle: nil)
            let vc = mainStoryboard.instantiateInitialViewController()
            appdelegate.window!.rootViewController = vc
            GlobalVariables.logoutPressed = false
        }
    }
    
    
    //MARK: - Get User Info.
    
    func getUserName() {
        userRoles?.load {
            [weak self]userInfo, error in
            // use userInfo struct and handle error here
            let id = userInfo?.id
            let name = userInfo?.userName
            _ = userInfo?.detail
            let roles = userInfo?.roles
            _ = userInfo.debugDescription
            
            Utilities.sharedInstance.userName = id ?? name ?? ""
            
            // ---- Called everytime login happens, remain saved in offline storage --- //
            // --- In optional array "roles", find value == "HighLow": If found(True) --- //
            for case "HighLow" in roles ?? [] {
                Utilities.sharedInstance.userRoleHighLow = true
            }
            
            print(Utilities.sharedInstance.userRoleHighLow ?? false)
            
            //Clear Local Store
            let userName = Utilities.sharedInstance.userName
            let previousUserName = UserDefaults.standard.string(forKey: "userName")
            let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
            let completePath = path.appending("/" + userName!)
            
            if let prevName = previousUserName {
                let completePrevPath = path.appending("/" + prevName)
                if(previousUserName != userName){
                    ////print("Clearing the LocalStore")
                    do {
                        //delete the old user profile
                        try FileManager.default.removeItem(atPath: completePrevPath)
                    } catch let error as NSError {
                        NSLog("Unable to delete directory \(error.debugDescription)")
                    }
                    Model.clear()
                }
            }
            do {
                //Create dic for current user
                try FileManager.default.createDirectory(atPath: completePath, withIntermediateDirectories: true, attributes: nil)
            } catch let error as NSError {
                NSLog("Unable to create directory \(error.debugDescription)")
            }
            UserDefaults.standard.set(userName, forKey: "userName")
            self?.delegate?.onboarded(onboardingContext: (self?.onBoardingcontext)!)
        }
    }
    
    
    // MARK: - Completion Handlers
    
    private func onboardingCompleted(_ result: OnboardingResult) {
        switch result {
        case let .success(context):
            LogHelper.shared?.debug("Successfully onboarded.", loggerObject: self.logger)
            self.onBoardingcontext = context
            self.finalizeOnboarding(context)
            
        case let .failed(error):
            LogHelper.shared?.error("Onboarding failed!", loggerObject: self.logger, error: error)
            self.clearUserData()
            self.onboardFailed(error)
        }
    }
    
    private func restoreCompleted(_ result: OnboardingResult) {
        
        
        switch result {
        case let .success(context):
            LogHelper.shared?.debug("Successfully restored.", loggerObject: self.logger)
            self.onBoardingcontext = context
            self.finalizeOnboarding(context)
            
        case let .failed(error):
            LogHelper.shared?.error("Restoring failed!", loggerObject: self.logger, error: error)
            self.restoreFailed(error)
        }
    }
    
    
    
    private func finalizeOnboarding(_ context: OnboardingContext) {
        self.state = .running
        
        self.credentialStore = context.credentialStore
        self.onboardingID = context.onboardingID
        
        self.presentationDelegate.clearSplashScreen()
        self.presentationDelegate.animated = false
        
        self.getUserName()
    }
    
    // MARK: - Error Handlers
    private func onboardFailed(_ error: Error) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        var title: String!
        var message: String!
        
        switch error {
        case WelcomeScreenError.demoModeRequested:
            // TODO: Implement demo mode here.
            
            title = NSLocalizedString("keyDemoModeSelectedTitle",
                                      value: "Demo Mode",
                                      comment: "XTIT: Title of alert action that the demo mode is selected.")
            message = NSLocalizedString("keyDemoModeSelectedMessage",
                                        value: "The Demo mode with Offline OData is not implemented in the generated application.",
                                        comment: "XMSG: Message that the user selected the demo mode.")
            
            let restartTitle = NSLocalizedString("keyGoBackButtonTitle", value: "Go back", comment: "XBUT: Title of go back button.")
            alertController.addAction(UIAlertAction(title: restartTitle, style: .default) { _ in
                // There is no need to call reset in case onboarding fails, since it is called by the SDK.
                self.onboardOrRestore()
            })
            
        default:
            title = NSLocalizedString("keyErrorLogonProcessFailedTitle",
                                      value: "Failed to logon!",
                                      comment: "XTIT: Title of alert message about logon process failure.")
            message = error.localizedDescription
            
            let retryTitle = NSLocalizedString("keyRetryButtonTitle", value: "Retry", comment: "XBUT: Title of Retry button.")
            alertController.addAction(UIAlertAction(title: retryTitle, style: .default) { _ in
                // There is no need to call reset in case onboarding fails, since it is called by the SDK.
                self.onboardOrRestore()
            })
        }
        
        // Set title and message
        alertController.title = title
        alertController.message = message
        
        // Present the alert
        OperationQueue.main.addOperation({
            ModalUIViewControllerPresenter.topPresentedViewController()?.present(alertController, animated: true)
        })
    }
    
    private func restoreFailed(_ error: Error) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        var title: String!
        var message: String!
        
        switch error {
        case StoreManagerError.cancelPasscodeEntry:
            fallthrough
        case StoreManagerError.skipPasscodeSetup:
            fallthrough
        case StoreManagerError.resetPasscode:
            self.resetOnboarding()
            return
            
        case StoreManagerError.passcodeRetryLimitReached:
            title = NSLocalizedString("keyErrorPasscodeRetryLimitReachedTitle",
                                      value: "Passcode Retry Limit Reached!",
                                      comment: "XTIT: Title of alert action that the passcode retry limit has been reached.")
            message = NSLocalizedString("keyErrorPasscodeRetryLimitReachedMessage",
                                        value: "Reached the maximum number of retries. Application should be reset.",
                                        comment: "XMSG: Message that the application shall be reseted because the passcode retry limit has been reached.")
            
            let resetActionTitle = NSLocalizedString("keyResetButtonTitle", value: "Reset", comment: "XBUT: Reset button title.")
            alertController.addAction(UIAlertAction(title: resetActionTitle, style: .destructive) { _ in
                self.resetOnboarding()
            })
            
        default:
            title = NSLocalizedString("keyErrorLogonProcessFailedTitle",
                                      value: "Failed to logon!",
                                      comment: "XTIT: Title of alert message about logon process failure.")
            message = error.localizedDescription
            
            let retryTitle = NSLocalizedString("keyRetryButtonTitle", value: "Retry", comment: "XBUT: Title of Retry button.")
            alertController.addAction(UIAlertAction(title: retryTitle, style: .default) { _ in
                self.onboardOrRestore()
            })
            
            let resetActionTitle = NSLocalizedString("keyResetButtonTitle", value: "Reset", comment: "XBUT: Reset button title.")
            alertController.addAction(UIAlertAction(title: resetActionTitle, style: .destructive) { _ in
                self.resetOnboarding()
            })
        }
        
        // Set title and message
        alertController.title = title
        alertController.message = message
        
        // Present the alert
        OperationQueue.main.addOperation({
            ModalUIViewControllerPresenter.topPresentedViewController()?.present(alertController, animated: true)
        })
    }
    
    /// Clears any locally stored user data.
    private func clearUserData() {
        // Currently we only have to clear the shared URLCache and the shared HTTPCookieStorage,
        // but you might want to clear more data.
        URLCache.shared.removeAllCachedResponses()
        HTTPCookieStorage.shared.removeCookies(since: Date.distantPast)
        
    }
}

// MARK: - Managing the OnboardingID
extension OnboardingManager {
    
    private enum UserDefaultsKeys {
        static let onboardingId = "keyOnboardingID"
    }
    
    private var onboardingID: UUID? {
        
        get {
            guard let storedOnboardingID = UserDefaults.standard.string(forKey: UserDefaultsKeys.onboardingId),
                let onboardingID = UUID(uuidString: storedOnboardingID) else {
                    // There is no OnboardingID stored yet
                    return nil
            }
            return onboardingID
        }
        
        set {
            if let newOnboardingID = newValue {
                // If non-nil value is set, store it in UserDefaults
                UserDefaults.standard.set(newOnboardingID.uuidString, forKey: UserDefaultsKeys.onboardingId)
            } else {
                // If nil value is set, clear previous from UserDefaults
                UserDefaults.standard.removeObject(forKey: UserDefaultsKeys.onboardingId)
            }
        }
    }
}

// MARK: - Handling application put to background
extension OnboardingManager {
    
    private enum State {
        /// Application is either in the Onboarding or Restoring state.
        case onboarding
        /// Application is running in the foreground.
        case running
        /// Application is locked in the background.
        case locked
        /// Application has brought to the front to unlock.
        case unlocking
    }
    
    private var shouldLock: Bool {
        return self.state == .running
    }
    
    /// Handle application put to the background. Displays a lockscreen if the application can be locked to hide any sensitive information.
    func applicationDidEnterBackground() {
        // Return, so that application does not lock (user will not have to re-enter password until they close the app or log out)
        return
    }
    
    /// Displays a view to hide sensitive information.
    private func lockApplication() {
        LogHelper.shared?.debug("Locking the application.", loggerObject: self.logger)
        self.state = .locked
        
        // Present the SnapshotScreen
        let snapshotViewController = SnapshotViewController()
        guard let topViewController = ModalUIViewControllerPresenter.topPresentedViewController() else {
            fatalError("Could not present the SnapshotScreen to hide sensitive inforamtion.")
        }
        topViewController.present(snapshotViewController, animated: false)
    }
    
    private var shouldUnlock: Bool {
        return self.state == .locked
    }
    
    /// Handle application brought to foreground. If applicable, displays the unlock screen.
    func applicationWillEnterForeground() {
        guard self.shouldUnlock else {
            return
        }
        
        self.unlockApplication()
    }
    
    /// Present the unlock screen.
    private func unlockApplication() {
        LogHelper.shared?.debug("Unlocking the application.", loggerObject: self.logger)
        guard let onboardingID = self.onboardingID else {
            LogHelper.shared?.error("OnboardingID is required to unlock.", loggerObject: self.logger)
            return
        }
        
        self.state = .unlocking
        
        // Dismiss SnapshotScreen
        guard let topViewController = ModalUIViewControllerPresenter.topPresentedViewController() else {
            LogHelper.shared?.error("Could not find top ViewController for unlocking.", loggerObject: self.logger)
            return
        }
        
        topViewController.dismiss(animated: false) {
            
            // Present the SplashScreen
            let splashViewController = FUIInfoViewController.createSplashScreenInstanceFromStoryboard()
            self.presentationDelegate.present(splashViewController) { error in
                if let error = error {
                    fatalError("Could not present SplashScreen, terminating the app to hide sensitive information. \(error)")
                }
                self.presentationDelegate.setSplashScreen(splashViewController)
                
                // Calling StoreManagerStep for passcode validation. This will display the PasscodeScreen.
                let context = OnboardingContext(onboardingID: onboardingID, credentialStore: self.credentialStore, presentationDelegate: self.presentationDelegate)
                
                StoreManagerStep().restore(context: context, completionHandler: self.unlockCompleted)
            }
        }
    }
    
    private func unlockCompleted(_ result: OnboardingResult) {
        switch result {
        case .success:
            self.state = .running
            // Dissmiss the SplashScreen, the PasscodeScreen is automatically dismissed.
            OperationQueue.main.addOperation {
                self.presentationDelegate.dismiss { _ in } // Passing an empty completionHandler
            }
            
        case let .failed(error):
            self.unlockFailed(error)
        }
    }
    
    private func unlockFailed(_ error: Error) {
        let title = NSLocalizedString("keyFailedToUnlockTitle", value: "Failed to unlock!", comment: "XTIT: Failed to unlock alert title.")
        let alertViewController = UIAlertController(title: title, message: error.localizedDescription, preferredStyle: .alert)
        
        let resetActionTitle = NSLocalizedString("keyResetButtonTitle", value: "Reset", comment: "XBUT: Reset button title.")
        alertViewController.addAction(UIAlertAction(title: resetActionTitle, style: .destructive) { _ in
            self.resetOnboarding()
        })
        
        OperationQueue.main.addOperation {
            ModalUIViewControllerPresenter.topPresentedViewController()?.present(alertViewController, animated: true)
        }
    }
}

// MARK: - SAPWKNavigationDelegate

// The WKWebView occasionally returns an NSURLErrorCancelled error if a redirect happens too fast.
// In case of OAuth with SAP's identity provider (IDP) we do not treat this as an error.
extension OnboardingManager: SAPWKNavigationDelegate {
    func webView(_: WKWebView, handleFailed _: WKNavigation!, withError error: Error) -> Error? {
        if isCancelledError(error) {
            return nil
        }
        return error
    }
    
    func webView(_: WKWebView, handleFailedProvisionalNavigation _: WKNavigation!, withError error: Error) -> Error? {
        if isCancelledError(error) {
            return nil
        }
        return error
    }
    
    private func isCancelledError(_ error: Error) -> Bool {
        let nsError = error as NSError
        return nsError.domain == NSURLErrorDomain &&
            nsError.code == NSURLErrorCancelled
    }
}

