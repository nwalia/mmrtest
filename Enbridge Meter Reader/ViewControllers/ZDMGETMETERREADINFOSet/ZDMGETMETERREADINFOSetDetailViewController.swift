//
// ZDMGETMETERREADINFOSetDetailViewController.swift
// Dev-SAML
//
// Created by SAP Cloud Platform SDK for iOS Assistant application on 12/03/18
//

import Foundation
import SAPFoundation
import SAPOData
import SAPFiori
import SAPCommon

class ZDMGETMETERREADINFOSetDetailViewController: FUIFormTableViewController, SAPFioriLoadingIndicator {
    private let appDelegate = UIApplication.shared.delegate as! AppDelegate
    private var ec1: Ec1<OnlineODataProvider> {
        return self.appDelegate.ec1
    }

    private var validity = [String: Bool]()
    private var _entity: ZdmGetMeterReadInfo?
    var allowsEditableCells = false
    var entity: ZdmGetMeterReadInfo {
        get {
            if self._entity == nil {
                self._entity = self.createEntityWithDefaultValues()
            }
            return self._entity!
        }
        set {
            self._entity = newValue
        }
    }

    private let logger = Logger.shared(named: "ZDMGETMETERREADINFOSetMasterViewControllerLogger")
    var loadingIndicator: FUILoadingIndicatorView?
    var entityUpdater: EntityUpdaterDelegate?
    var tableUpdater: EntitySetUpdaterDelegate?
    private let okTitle = NSLocalizedString("keyOkButtonTitle",
                                            value: "OK",
                                            comment: "XBUT: Title of OK button.")

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 44
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender _: Any?) {
        if segue.identifier == "updateEntity" {
            // Show the Detail view with the current entity, where the properties scan be edited and updated
            self.logger.info("Showing a view to update the selected entity.")
            let dest = segue.destination as! UINavigationController
            let detailViewController = dest.viewControllers[0] as! ZDMGETMETERREADINFOSetDetailViewController
            detailViewController.title = NSLocalizedString("keyUpdateEntityTitle", value: "Update Entity", comment: "XTIT: Title of update selected entity screen.")
            detailViewController.entity = self.entity
            let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: detailViewController, action: #selector(detailViewController.updateEntity))
            detailViewController.navigationItem.rightBarButtonItem = doneButton
            let cancelButton = UIBarButtonItem(title: NSLocalizedString("keyCancelButtonToGoPreviousScreen", value: "Cancel", comment: "XBUT: Title of Cancel button."), style: .plain, target: detailViewController, action: #selector(detailViewController.cancel))
            detailViewController.navigationItem.leftBarButtonItem = cancelButton
            detailViewController.allowsEditableCells = true
            detailViewController.entityUpdater = self
        }
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            return self.cellForStatus(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: ZdmGetMeterReadInfo.status)
        case 1:
            return self.cellForTimeIntervalEnd(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: ZdmGetMeterReadInfo.timeIntervalEnd)
        case 2:
            return self.cellForTimeIntervalStart(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: ZdmGetMeterReadInfo.timeIntervalStart)
        case 3:
            return self.cellForUserid(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: ZdmGetMeterReadInfo.userid)
        case 4:
            return self.cellForCrewid(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: ZdmGetMeterReadInfo.crewid)
        case 5:
            return self.cellForReadingDate(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: ZdmGetMeterReadInfo.readingDate)
        case 6:
            return self.cellForRouteid(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: ZdmGetMeterReadInfo.routeid)
        case 7:
            return self.cellForMeterInfo(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: ZdmGetMeterReadInfo.meterInfo)
        case 8:
            return self.cellForWorkorderid(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: ZdmGetMeterReadInfo.workorderid)
        case 9:
            return self.cellForCallid(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: ZdmGetMeterReadInfo.callid)
        case 10:
            return self.cellForCallnumber(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: ZdmGetMeterReadInfo.callnumber)
        case 11:
            return self.cellForMeterNum(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: ZdmGetMeterReadInfo.meterNum)
        case 12:
            return self.cellForMatnr(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: ZdmGetMeterReadInfo.matnr)
        case 13:
            return self.cellForCustInfo(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: ZdmGetMeterReadInfo.custInfo)
        case 14:
            return self.cellForSplMsg(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: ZdmGetMeterReadInfo.splMsg)
        case 15:
            return self.cellForMeterLocation(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: ZdmGetMeterReadInfo.meterLocation)
        case 16:
            return self.cellForHouseNum(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: ZdmGetMeterReadInfo.houseNum)
        case 17:
            return self.cellForUnitNum(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: ZdmGetMeterReadInfo.unitNum)
        case 18:
            return self.cellForStreet(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: ZdmGetMeterReadInfo.street)
        case 19:
            return self.cellForCity(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: ZdmGetMeterReadInfo.city)
        case 20:
            return self.cellForPostCode(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: ZdmGetMeterReadInfo.postCode)
        case 21:
            return self.cellForCustName(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: ZdmGetMeterReadInfo.custName)
        case 22:
            return self.cellForHomeNum(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: ZdmGetMeterReadInfo.homeNum)
        case 23:
            return self.cellForBusNum(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: ZdmGetMeterReadInfo.busNum)
        case 24:
            return self.cellForNumOfDials(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: ZdmGetMeterReadInfo.numOfDials)
        case 25:
            return self.cellForHigh1(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: ZdmGetMeterReadInfo.high1)
        case 26:
            return self.cellForLow1(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: ZdmGetMeterReadInfo.low1)
        case 27:
            return self.cellForReqdDate(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: ZdmGetMeterReadInfo.reqdDate)
        case 28:
            return self.cellForNotifType(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: ZdmGetMeterReadInfo.notifType)
        case 29:
            return self.cellForSplInstr(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: ZdmGetMeterReadInfo.splInstr)
        case 30:
            return self.cellForGrid(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: ZdmGetMeterReadInfo.grid)
        case 31:
            return self.cellForPsn(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: ZdmGetMeterReadInfo.psn)
        case 32:
            return self.cellForMtrReadSeq(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: ZdmGetMeterReadInfo.mtrReadSeq)
        case 33:
            return self.cellForTotMeterRecs(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: ZdmGetMeterReadInfo.totMeterRecs)
        case 34:
            return self.cellForLatitude(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: ZdmGetMeterReadInfo.latitude)
        case 35:
            return self.cellForLongitude(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: ZdmGetMeterReadInfo.longitude)
        case 36:
            return self.cellForConsEst(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: ZdmGetMeterReadInfo.consEst)
        case 37:
            return self.cellForWorkTypeInd(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: ZdmGetMeterReadInfo.workTypeInd)
        case 38:
            return self.cellForRiserOnWall(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: ZdmGetMeterReadInfo.riserOnWall)
        case 39:
            return self.cellForRiserDistance(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: ZdmGetMeterReadInfo.riserDistance)
        case 40:
            return self.cellForRiserFrmWall(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: ZdmGetMeterReadInfo.riserFrmWall)
        case 41:
            return self.cellForRfErtid(tableView: tableView, indexPath: indexPath, currentEntity: self.entity, property: ZdmGetMeterReadInfo.rfErtid)
        default:
            return CellCreationHelper.cellForDefault(tableView: tableView, indexPath: indexPath, editingIsAllowed: allowsEditableCells)
        }
    }

    override func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return 42
    }

    override func tableView(_: UITableView, canEditRowAt _: IndexPath) -> Bool {
        return true
    }

    // MARK: - OData property specific cell creators

    private func cellForStatus(tableView: UITableView, indexPath: IndexPath, currentEntity: ZdmGetMeterReadInfo, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.status {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.status = nil
                isNewValueValid = true
            } else {
                if ZdmGetMeterReadInfo.status.isOptional || newValue != "" {
                    currentEntity.status = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForTimeIntervalEnd(tableView: UITableView, indexPath: IndexPath, currentEntity: ZdmGetMeterReadInfo, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.timeIntervalEnd {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.timeIntervalEnd = nil
                isNewValueValid = true
            } else {
                if ZdmGetMeterReadInfo.timeIntervalEnd.isOptional || newValue != "" {
                    currentEntity.timeIntervalEnd = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForTimeIntervalStart(tableView: UITableView, indexPath: IndexPath, currentEntity: ZdmGetMeterReadInfo, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.timeIntervalStart {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.timeIntervalStart = nil
                isNewValueValid = true
            } else {
                if ZdmGetMeterReadInfo.timeIntervalStart.isOptional || newValue != "" {
                    currentEntity.timeIntervalStart = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForUserid(tableView: UITableView, indexPath: IndexPath, currentEntity: ZdmGetMeterReadInfo, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.userid {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.userid = nil
                isNewValueValid = true
            } else {
                if ZdmGetMeterReadInfo.userid.isOptional || newValue != "" {
                    currentEntity.userid = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForCrewid(tableView: UITableView, indexPath: IndexPath, currentEntity: ZdmGetMeterReadInfo, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.crewid {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.crewid = nil
                isNewValueValid = true
            } else {
                if ZdmGetMeterReadInfo.crewid.isOptional || newValue != "" {
                    currentEntity.crewid = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForReadingDate(tableView: UITableView, indexPath: IndexPath, currentEntity: ZdmGetMeterReadInfo, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.readingDate {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.readingDate = nil
                isNewValueValid = true
            } else {
                if ZdmGetMeterReadInfo.readingDate.isOptional || newValue != "" {
                    currentEntity.readingDate = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForRouteid(tableView: UITableView, indexPath: IndexPath, currentEntity: ZdmGetMeterReadInfo, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.routeid {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.routeid = nil
                isNewValueValid = true
            } else {
                if ZdmGetMeterReadInfo.routeid.isOptional || newValue != "" {
                    currentEntity.routeid = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForMeterInfo(tableView: UITableView, indexPath: IndexPath, currentEntity: ZdmGetMeterReadInfo, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.meterInfo {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.meterInfo = nil
                isNewValueValid = true
            } else {
                if ZdmGetMeterReadInfo.meterInfo.isOptional || newValue != "" {
                    currentEntity.meterInfo = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForWorkorderid(tableView: UITableView, indexPath: IndexPath, currentEntity: ZdmGetMeterReadInfo, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.workorderid {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.workorderid = nil
                isNewValueValid = true
            } else {
                if ZdmGetMeterReadInfo.workorderid.isOptional || newValue != "" {
                    currentEntity.workorderid = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForCallid(tableView: UITableView, indexPath: IndexPath, currentEntity: ZdmGetMeterReadInfo, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.callid {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.callid = nil
                isNewValueValid = true
            } else {
                if ZdmGetMeterReadInfo.callid.isOptional || newValue != "" {
                    currentEntity.callid = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForCallnumber(tableView: UITableView, indexPath: IndexPath, currentEntity: ZdmGetMeterReadInfo, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.callnumber {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.callnumber = nil
                isNewValueValid = true
            } else {
                if ZdmGetMeterReadInfo.callnumber.isOptional || newValue != "" {
                    currentEntity.callnumber = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForMeterNum(tableView: UITableView, indexPath: IndexPath, currentEntity: ZdmGetMeterReadInfo, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.meterNum {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.meterNum = nil
                isNewValueValid = true
            } else {
                if ZdmGetMeterReadInfo.meterNum.isOptional || newValue != "" {
                    currentEntity.meterNum = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForMatnr(tableView: UITableView, indexPath: IndexPath, currentEntity: ZdmGetMeterReadInfo, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.matnr {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.matnr = nil
                isNewValueValid = true
            } else {
                if ZdmGetMeterReadInfo.matnr.isOptional || newValue != "" {
                    currentEntity.matnr = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForCustInfo(tableView: UITableView, indexPath: IndexPath, currentEntity: ZdmGetMeterReadInfo, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.custInfo {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.custInfo = nil
                isNewValueValid = true
            } else {
                if ZdmGetMeterReadInfo.custInfo.isOptional || newValue != "" {
                    currentEntity.custInfo = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForSplMsg(tableView: UITableView, indexPath: IndexPath, currentEntity: ZdmGetMeterReadInfo, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.splMsg {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.splMsg = nil
                isNewValueValid = true
            } else {
                if ZdmGetMeterReadInfo.splMsg.isOptional || newValue != "" {
                    currentEntity.splMsg = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForMeterLocation(tableView: UITableView, indexPath: IndexPath, currentEntity: ZdmGetMeterReadInfo, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.meterLocation {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.meterLocation = nil
                isNewValueValid = true
            } else {
                if ZdmGetMeterReadInfo.meterLocation.isOptional || newValue != "" {
                    currentEntity.meterLocation = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForHouseNum(tableView: UITableView, indexPath: IndexPath, currentEntity: ZdmGetMeterReadInfo, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.houseNum {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.houseNum = nil
                isNewValueValid = true
            } else {
                if ZdmGetMeterReadInfo.houseNum.isOptional || newValue != "" {
                    currentEntity.houseNum = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForUnitNum(tableView: UITableView, indexPath: IndexPath, currentEntity: ZdmGetMeterReadInfo, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.unitNum {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.unitNum = nil
                isNewValueValid = true
            } else {
                if ZdmGetMeterReadInfo.unitNum.isOptional || newValue != "" {
                    currentEntity.unitNum = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForStreet(tableView: UITableView, indexPath: IndexPath, currentEntity: ZdmGetMeterReadInfo, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.street {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.street = nil
                isNewValueValid = true
            } else {
                if ZdmGetMeterReadInfo.street.isOptional || newValue != "" {
                    currentEntity.street = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForCity(tableView: UITableView, indexPath: IndexPath, currentEntity: ZdmGetMeterReadInfo, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.city {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.city = nil
                isNewValueValid = true
            } else {
                if ZdmGetMeterReadInfo.city.isOptional || newValue != "" {
                    currentEntity.city = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForPostCode(tableView: UITableView, indexPath: IndexPath, currentEntity: ZdmGetMeterReadInfo, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.postCode {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.postCode = nil
                isNewValueValid = true
            } else {
                if ZdmGetMeterReadInfo.postCode.isOptional || newValue != "" {
                    currentEntity.postCode = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForCustName(tableView: UITableView, indexPath: IndexPath, currentEntity: ZdmGetMeterReadInfo, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.custName {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.custName = nil
                isNewValueValid = true
            } else {
                if ZdmGetMeterReadInfo.custName.isOptional || newValue != "" {
                    currentEntity.custName = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForHomeNum(tableView: UITableView, indexPath: IndexPath, currentEntity: ZdmGetMeterReadInfo, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.homeNum {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.homeNum = nil
                isNewValueValid = true
            } else {
                if ZdmGetMeterReadInfo.homeNum.isOptional || newValue != "" {
                    currentEntity.homeNum = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForBusNum(tableView: UITableView, indexPath: IndexPath, currentEntity: ZdmGetMeterReadInfo, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.busNum {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.busNum = nil
                isNewValueValid = true
            } else {
                if ZdmGetMeterReadInfo.busNum.isOptional || newValue != "" {
                    currentEntity.busNum = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForNumOfDials(tableView: UITableView, indexPath: IndexPath, currentEntity: ZdmGetMeterReadInfo, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.numOfDials {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.numOfDials = nil
                isNewValueValid = true
            } else {
                if ZdmGetMeterReadInfo.numOfDials.isOptional || newValue != "" {
                    currentEntity.numOfDials = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForHigh1(tableView: UITableView, indexPath: IndexPath, currentEntity: ZdmGetMeterReadInfo, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.high1 {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.high1 = nil
                isNewValueValid = true
            } else {
                if ZdmGetMeterReadInfo.high1.isOptional || newValue != "" {
                    currentEntity.high1 = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForLow1(tableView: UITableView, indexPath: IndexPath, currentEntity: ZdmGetMeterReadInfo, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.low1 {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.low1 = nil
                isNewValueValid = true
            } else {
                if ZdmGetMeterReadInfo.low1.isOptional || newValue != "" {
                    currentEntity.low1 = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForReqdDate(tableView: UITableView, indexPath: IndexPath, currentEntity: ZdmGetMeterReadInfo, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.reqdDate {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.reqdDate = nil
                isNewValueValid = true
            } else {
                if ZdmGetMeterReadInfo.reqdDate.isOptional || newValue != "" {
                    currentEntity.reqdDate = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForNotifType(tableView: UITableView, indexPath: IndexPath, currentEntity: ZdmGetMeterReadInfo, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.notifType {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.notifType = nil
                isNewValueValid = true
            } else {
                if ZdmGetMeterReadInfo.notifType.isOptional || newValue != "" {
                    currentEntity.notifType = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForSplInstr(tableView: UITableView, indexPath: IndexPath, currentEntity: ZdmGetMeterReadInfo, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.splInstr {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.splInstr = nil
                isNewValueValid = true
            } else {
                if ZdmGetMeterReadInfo.splInstr.isOptional || newValue != "" {
                    currentEntity.splInstr = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForGrid(tableView: UITableView, indexPath: IndexPath, currentEntity: ZdmGetMeterReadInfo, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.grid {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.grid = nil
                isNewValueValid = true
            } else {
                if ZdmGetMeterReadInfo.grid.isOptional || newValue != "" {
                    currentEntity.grid = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForPsn(tableView: UITableView, indexPath: IndexPath, currentEntity: ZdmGetMeterReadInfo, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.psn {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.psn = nil
                isNewValueValid = true
            } else {
                if ZdmGetMeterReadInfo.psn.isOptional || newValue != "" {
                    currentEntity.psn = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForMtrReadSeq(tableView: UITableView, indexPath: IndexPath, currentEntity: ZdmGetMeterReadInfo, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.mtrReadSeq {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.mtrReadSeq = nil
                isNewValueValid = true
            } else {
                if let validValue = Int(newValue) {
                    currentEntity.mtrReadSeq = validValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForTotMeterRecs(tableView: UITableView, indexPath: IndexPath, currentEntity: ZdmGetMeterReadInfo, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.totMeterRecs {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.totMeterRecs = nil
                isNewValueValid = true
            } else {
                if ZdmGetMeterReadInfo.totMeterRecs.isOptional || newValue != "" {
                    currentEntity.totMeterRecs = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForLatitude(tableView: UITableView, indexPath: IndexPath, currentEntity: ZdmGetMeterReadInfo, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.latitude {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.latitude = nil
                isNewValueValid = true
            } else {
                if ZdmGetMeterReadInfo.latitude.isOptional || newValue != "" {
                    currentEntity.latitude = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForLongitude(tableView: UITableView, indexPath: IndexPath, currentEntity: ZdmGetMeterReadInfo, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.longitude {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.longitude = nil
                isNewValueValid = true
            } else {
                if ZdmGetMeterReadInfo.longitude.isOptional || newValue != "" {
                    currentEntity.longitude = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForConsEst(tableView: UITableView, indexPath: IndexPath, currentEntity: ZdmGetMeterReadInfo, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.consEst {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.consEst = nil
                isNewValueValid = true
            } else {
                if ZdmGetMeterReadInfo.consEst.isOptional || newValue != "" {
                    currentEntity.consEst = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForWorkTypeInd(tableView: UITableView, indexPath: IndexPath, currentEntity: ZdmGetMeterReadInfo, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.workTypeInd {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.workTypeInd = nil
                isNewValueValid = true
            } else {
                if ZdmGetMeterReadInfo.workTypeInd.isOptional || newValue != "" {
                    currentEntity.workTypeInd = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForRiserOnWall(tableView: UITableView, indexPath: IndexPath, currentEntity: ZdmGetMeterReadInfo, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.riserOnWall {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.riserOnWall = nil
                isNewValueValid = true
            } else {
                if ZdmGetMeterReadInfo.riserOnWall.isOptional || newValue != "" {
                    currentEntity.riserOnWall = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForRiserDistance(tableView: UITableView, indexPath: IndexPath, currentEntity: ZdmGetMeterReadInfo, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.riserDistance {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.riserDistance = nil
                isNewValueValid = true
            } else {
                if ZdmGetMeterReadInfo.riserDistance.isOptional || newValue != "" {
                    currentEntity.riserDistance = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForRiserFrmWall(tableView: UITableView, indexPath: IndexPath, currentEntity: ZdmGetMeterReadInfo, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.riserFrmWall {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.riserFrmWall = nil
                isNewValueValid = true
            } else {
                if ZdmGetMeterReadInfo.riserFrmWall.isOptional || newValue != "" {
                    currentEntity.riserFrmWall = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    private func cellForRfErtid(tableView: UITableView, indexPath: IndexPath, currentEntity: ZdmGetMeterReadInfo, property: Property) -> UITableViewCell {
        var value = ""
        if let propertyValue = currentEntity.rfErtid {
            value = "\(propertyValue)"
        }
        return CellCreationHelper.cellForProperty(tableView: tableView, indexPath: indexPath, entity: self.entity, property: property, value: value, editingIsAllowed: allowsEditableCells, changeHandler: { (newValue: String) -> Bool in
            var isNewValueValid = false
            // The property is optional, so nil value can be accepted
            if newValue.isEmpty {
                currentEntity.rfErtid = nil
                isNewValueValid = true
            } else {
                if ZdmGetMeterReadInfo.rfErtid.isOptional || newValue != "" {
                    currentEntity.rfErtid = newValue
                    isNewValueValid = true
                }
            }
            self.validity[property.name] = isNewValueValid
            self.barButtonShouldBeEnabled()
            return isNewValueValid
        })
    }

    // MARK: - OData functionalities

    @objc func createEntity() {
        self.showFioriLoadingIndicator()
        self.view.endEditing(true)
        self.logger.info("Creating entity in backend.")
        self.ec1.createEntity(self.entity) { error in
            self.hideFioriLoadingIndicator()
            if let error = error {
                self.logger.error("Create entry failed. Error: \(error)", error: error)
                let alertController = UIAlertController(title: NSLocalizedString("keyErrorEntityCreationTitle", value: "Create entry failed", comment: "XTIT: Title of alert message about entity creation error."), message: error.localizedDescription, preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: self.okTitle, style: .default))
                OperationQueue.main.addOperation({
                    // Present the alertController
                    self.present(alertController, animated: true)
                })
                return
            }
            self.logger.info("Create entry finished successfully.")
            OperationQueue.main.addOperation({
                self.dismiss(animated: true) {
                    FUIToastMessage.show(message: NSLocalizedString("keyEntityCreationBody", value: "Created", comment: "XMSG: Title of alert message about successful entity creation."))
                    self.tableUpdater?.entitySetHasChanged()
                }
            })
        }
    }

    func createEntityWithDefaultValues() -> ZdmGetMeterReadInfo {
        let newEntity = ZdmGetMeterReadInfo()

        // Key properties without default value should be invalid by default for Create scenario
        if newEntity.status == nil || newEntity.status!.isEmpty {
            self.validity["STATUS"] = false
        }
        if newEntity.timeIntervalEnd == nil || newEntity.timeIntervalEnd!.isEmpty {
            self.validity["TIME_INTERVAL_END"] = false
        }
        if newEntity.timeIntervalStart == nil || newEntity.timeIntervalStart!.isEmpty {
            self.validity["TIME_INTERVAL_START"] = false
        }
        if newEntity.userid == nil || newEntity.userid!.isEmpty {
            self.validity["USERID"] = false
        }
        self.barButtonShouldBeEnabled()
        return newEntity
    }

    @objc func updateEntity(_: AnyObject) {
        self.showFioriLoadingIndicator()
        self.view.endEditing(true)
        self.logger.info("Updating entity in backend.")
        self.ec1.updateEntity(self.entity) { error in
            self.hideFioriLoadingIndicator()
            if let error = error {
                self.logger.error("Update entry failed. Error: \(error)", error: error)
                let alertController = UIAlertController(title: NSLocalizedString("keyErrorEntityUpdateTitle", value: "Update entry failed", comment: "XTIT: Title of alert message about entity update failure."), message: error.localizedDescription, preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: self.okTitle, style: .default))
                OperationQueue.main.addOperation({
                    // Present the alertController
                    self.present(alertController, animated: true)
                })
                return
            }
            self.logger.info("Update entry finished successfully.")
            OperationQueue.main.addOperation({
                self.dismiss(animated: true) {
                    FUIToastMessage.show(message: NSLocalizedString("keyUpdateEntityFinishedTitle", value: "Updated", comment: "XTIT: Title of alert message about successful entity update."))
                    self.entityUpdater?.entityHasChanged(self.entity)
                }
            })
        }
    }

    // MARK: - other logic, helper

    @objc func cancel() {
        OperationQueue.main.addOperation({
            self.dismiss(animated: true)
        })
    }

    // Check if all text fields are valid
    private func barButtonShouldBeEnabled() {
        let anyFieldInvalid = self.validity.values.first { field in
            return field == false
        }
        self.navigationItem.rightBarButtonItem?.isEnabled = anyFieldInvalid == nil
    }
}

extension ZDMGETMETERREADINFOSetDetailViewController: EntityUpdaterDelegate {
    func entityHasChanged(_ entityValue: EntityValue?) {
        if let entity = entityValue {
            let currentEntity = entity as! ZdmGetMeterReadInfo
            self.entity = currentEntity
            DispatchQueue.main.async { [weak self] in
                self?.tableView.reloadData()
            }
        }
    }
}
