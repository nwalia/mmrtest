//
//  GlobalConstants.swift
//  Enbridge Meter Reader
//
//  Created by Abramsky, Lauren (CA - Toronto) on 2017-12-13.
//  Copyright © 2017 Enbridge. All rights reserved.
//

import Foundation
import UIKit

struct GlobalConstants {
    
    public static let ANYLINE_LICENSE_KEY = "eyAiYW5kcm9pZElkZW50aWZpZXIiOiBbICJjb20uZW5icmlkZ2UubWV0ZXJyZWFkLnRlc3QiLCAiY29tLmVuYnJpZGdlLm1ldGVycmVhZCIgXSwgImRlYnVnUmVwb3J0aW5nIjogIm9wdC1vdXQiLCAiaW9zSWRlbnRpZmllciI6IFsgImNvbS5lbmJyaWRnZS5tZXRlcnJlYWQiLCAiY29tLmVuYnJpZGdlLm1ldGVycmVhZC50ZXN0IiBdLCAibGljZW5zZUtleVZlcnNpb24iOiAyLCAibWFqb3JWZXJzaW9uIjogIjQiLCAibWF4RGF5c05vdFJlcG9ydGVkIjogOTAsICJwaW5nUmVwb3J0aW5nIjogdHJ1ZSwgInBsYXRmb3JtIjogWyAiaU9TIiwgIkFuZHJvaWQiIF0sICJzY29wZSI6IFsgIkVORVJHWSIsICJCQVJDT0RFIiBdLCAic2hvd1BvcFVwQWZ0ZXJFeHBpcnkiOiBmYWxzZSwgInNob3dXYXRlcm1hcmsiOiBmYWxzZSwgInRvbGVyYW5jZURheXMiOiA5MCwgInZhbGlkIjogIjIwMTktMDgtMzEiIH0KVDNBMHI5RXplc2RYR0tDcDNiVUdVSWdsMnpXZWNWQ0k5ak04bTJXZXJrSkc2UjlIZ1BpWGcvTmFxRm8rK2RFdQo0TlF0ODF4Ny9vQkwraVRtVHZTOXRHeEFDcW9rYmlsSjlpQmNWdW94aEt3MGpXWlVra3Z5VkROYVdOVGF6by9CCnJ5UTZZRmk5cEtPclJ4WnNNakZWYXp1UnZ4d3M3dzB6SFZmb2JBaGdPbkxvOVBzZDk5NUQwNllxL0RrcFhqZVAKWTNEZ3hYT1Z5cHhHN1FxK1ZuZGpmSFlaMnRtbGdRK01CRzFWMDVlc3lHeGgxUktQNlFHQnpHRUsxOUtUUUFGWgpnUTl4UU5ZWUxDd3lMZGVVSlJuamEzcDEybDN2UzRDMU5GZmtTeWxneElISHlqUElrSktoZlQySlJyWmZpYVVBCkZydDFnVTkwdVNzODhBTUpFTEVVU3c9PQo="
    
    public static let ORANGE:UIColor = UIColor(red:1, green:0.59, blue:0.31, alpha:1)
    
    public static let TEAL:UIColor = UIColor(red:0.64, green:0.98, blue:0.93, alpha:1)
    
    public static let GRAY:UIColor = UIColor(red:0.33, green:0.33, blue:0.33, alpha:1.0)
    
    public static let LIGHT_GRAY:UIColor = UIColor(red:0.59, green:0.59, blue:0.59, alpha:1)
    
    public static let DARK_GRAY:UIColor = UIColor(red:0.2, green:0.2, blue:0.2, alpha:1)
    
    public static let RED:UIColor = UIColor(red:0.78, green:0.06, blue:0.18, alpha:1)
    
    public static let YELLOW:UIColor = UIColor(red:1, green:0.72, blue:0.11, alpha:1)
    
    public static let LIGHT_YELLOW:UIColor = UIColor(red:1, green:0.8, blue:0.38, alpha:1)
        
    public static let PROGRESS_YELLOW: UIColor = UIColor(red:1, green:0.86, blue:0.55, alpha:1)
    
    public static let GREEN:UIColor = UIColor(red:0.36, green:0.67, blue:0.23, alpha:1)
    
    public static let LIGHT_GREEN:UIColor = UIColor(red:0.71, green:0.85, blue:0.65, alpha:1.0)
    
    public static let SELECTED_CELL: UIColor = UIColor(red:1, green:0.98, blue:0.92, alpha:1)
    
    public static let COMPLETED_CELL: UIColor = UIColor(red: 0.9098, green: 0.9098, blue: 0.9098, alpha: 1.0)
    
    public static let OFFCYCLE_ROUTE_ID = "OFFCYCLE"
    
    public static let OUTCARD_ROUTE_ID = "OUTCARD"
    
    public static let PERIODIC_ROUTE_ID = "PERIODIC"
    
    public static var CurrentRoute = ""
    
    public static var ROUTE_UPLOADING_MESSAGE = "Updating status in Click. "
    
    public static var ROUTE_UPLOADING_FAILED_MESSAGE = "Update failed, please pull to refresh"
    
    public static let NETWORK_STATUS_GREEN = UIColor(red: 0.4648, green: 0.836, blue: 0.441, alpha: 1)
    
    public static let OFFLINE_STATUS_BAR = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.3)

}

