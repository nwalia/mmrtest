//
//  GlobalVariables.swift
//  Enbridge Meter Reader
//
//  Created by Abramsky, Lauren (CA - Toronto) on 2017-12-12.
//  Copyright © 2017 Enbridge. All rights reserved.
//

import Foundation

class GlobalVariables {
    
    static var specialInstructionsDict: [String:Bool] = [:]
    static var logoutPressed: Bool = false
    static var badgeCount: Int = 0 // Number of unread messages
}
