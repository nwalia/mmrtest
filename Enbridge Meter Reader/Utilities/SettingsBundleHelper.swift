//
//  SettingsBundleHelper.swift
//  Enbridge Meter Reader
//
//  Created by Marian, Alex (CA - Toronto) on 2018-06-13.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import Foundation
class SettingsBundleHelper {
    
    static var environmentCode = ""
    
    struct SettingsBundleKeys {
        static let ClickDestination = "build_preference"
        static let AppVersionKey = "version_preference"
    }
    
    class func setVersionAndBuildNumber() {
        let version: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
        let build: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
         let VersionAndBuild: String = version + "." + build
        UserDefaults.standard.set(VersionAndBuild, forKey: "version_preference")
//        UserDefaults.standard.set(build, forKey: "build_preference")
    }
    
    class func setEnvironment(){
        
        let isProd = Bundle.main.object(forInfoDictionaryKey: "isProd") as! String
        var env = ""
        
        if isProd == "YES" {
            environmentCode = "PROD"
            env = "PROD"
        }
        else {
            env = UserDefaults.standard.string(forKey: "env_preference")!
            print("ENVIRONMENT_AT_APP_LAUNCH: " + env);
            
            environmentCode = env
            
            if env == "DEV" || env == "TR" || env == "QA" || env == "TEST"{
                //print("Environment: \(env)")
            }
            else {
                //print("ERROR setting destinations from SettingsBundle")
            }
        }
        
        // ----- Getting path for documents directory ---- //
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        if let prevEnv = UserDefaults.standard.string(forKey: "preEnv") {
            if prevEnv != env {
                if let userName = UserDefaults.standard.string(forKey: "userName") {
                    let completePrevPath = path.appending("/" + userName)
                    do {
                        //delete the old user profile
                        try FileManager.default.removeItem(atPath: completePrevPath)
                    } catch let error as NSError {
                        NSLog("Unable to delete directory \(error.debugDescription)")
                    }
                    Model.clear()
                }
            }
        }
        UserDefaults.standard.set(environmentCode, forKey: "preEnv")
    }
}

