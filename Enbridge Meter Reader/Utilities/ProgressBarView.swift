
//
//  ProgressBarView.swift
//  Enbridge Meter Reader
//
//  Created by Wentzel, Johann (CA - Alberta) on 2017-12-15.
//  Copyright © 2017 Enbridge. All rights reserved.
//
//
// HOW TO USE:
// 1. Drop a UIView onto your screen and add Auto Layout constraints as usual.
// 2. Set that UIView's custom class to ProgressBarView
// 3. Call PopulateBar() with the route number, the amount completed, and the total amount.
// 4. ???
// 5. Profit!

import UIKit

class ProgressBarView: ReusableView {
    
    enum DashboardType {
        case offcycle
        case periodic
        case outcard
    }
    
    @IBOutlet weak var fillBarContainer: UIView!
    @IBOutlet weak var barLabel: UILabel!
    var routeKey: String?
    var currentType: DashboardType?

    func populate(routeKey: String = GlobalConstants.OFFCYCLE_ROUTE_ID, type: DashboardType, shouldRefreshWithNetworkChange: Bool = false){
        
        // If the bar had a previous fill bar, remove it.
        if let previousFillBar = self.viewWithTag(101){
            previousFillBar.removeFromSuperview()
        }
        
        self.routeKey = routeKey
        self.currentType = type
        
        // calculates width of the "progress" component of the bar.
        layoutIfNeeded()
        
        guard let route = Model.sharedInstance.routes[routeKey] else { return }
        
        let barWidth = Double(fillBarContainer.bounds.size.width)
        var fillWidth: CGFloat
        var fillColor: UIColor
        
        if route.status == .completed {
            // Route is submitted: full green with text "Submitted"
            fillWidth = CGFloat(barWidth)
            fillColor = GlobalConstants.NETWORK_STATUS_GREEN
            barLabel.text = "SUBMITTED"
            barLabel.textColor = .white
        }
        else if route.status == .pending || (route.type == .OFFCYCLE && route.pendingReads > 0) {
            // Route is pending (submitted but device hasn't been online to send it to server): full red with text "PENDING"
            fillWidth = CGFloat(barWidth)
            fillColor = GlobalConstants.RED
            if route.type == .OFFCYCLE && route.pendingReads > 0 {
                barLabel.text = "\(route.completedOrSkippedReads + route.pendingReads) / \(route.totalReads) work orders completed (\(route.pendingReads) PENDING)"
            }
            else {
                barLabel.text = "PENDING - NO NETWORK CONNECTION"
            }
            barLabel.textColor = .white
        }
        else {
            // Route is available/in progress - partial yellow based on progress.
            fillColor = GlobalConstants.PROGRESS_YELLOW
            var completionPercent: Double?
            
            if route.totalReads == 0 {
                completionPercent = 0
            }
            else {
                completionPercent = Double(route.completedOrSkippedReads) / Double(route.totalReads)
            }
            
            fillWidth = CGFloat(completionPercent! * barWidth)
            barLabel.textColor = .gray
            
            // Change copy based on the dashboard type
            if type == .offcycle {
                barLabel.text = "\(route.completedReads) / \(route.totalReads) work orders completed"
            }
            else if type == .outcard {
                // Outcard progress bar updates with last completed meter's MRU #
                barLabel.text = "Cycle \(route.cycleNo!) - \(route.completedReads) / \(route.totalReads) meters completed"
            }
            else {
                barLabel.text = "MRU \(route.id) - \(route.completedOrSkippedReads) / \(route.totalReads) meters completed"
            }
        }
        
        let progressFillBar = UIView(frame: CGRect(x: 0, y: 0, width: fillWidth, height: fillBarContainer.bounds.size.height))
        progressFillBar.backgroundColor = fillColor
        progressFillBar.tag = 101
        fillBarContainer.addSubview(progressFillBar)
        
        if shouldRefreshWithNetworkChange {
            Model.sharedInstance.progressBarRefreshDelegate = self
        }


    }
    
//    deinit {
//        Model.sharedInstance.progressBarRefreshDelegate = nil
//    }
}

extension ProgressBarView: ModelDelegate {
    func didRefreshModel() {
        guard routeKey != nil else { return }
        guard currentType != nil else { return }
        
        populate(routeKey: self.routeKey!, type: self.currentType!, shouldRefreshWithNetworkChange: true)
    }
}
