//
//  ServiceRequestColorStyle.swift
//  Enbridge Meter Reader
//
//  Created by Wentzel, Johann (CA - Alberta) on 2018-01-09.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import UIKit

struct ServiceRequestColorStyle {
    let description: String
    let backgroundColor: UIColor
    
    var textColor = UITextField().textColor
    var borderWidth = 0
    var borderColor = UIColor.clear
    
    init(code: String){
        switch(code){
        case "ZB":
            description = "Battery Order"
            backgroundColor = GlobalConstants.ORANGE
            break
        case "ZT":
            description = "Tamper Lock"
            backgroundColor = GlobalConstants.TEAL
            break
        case "ZV":
            description = "Move Orders"
            backgroundColor = UIColor.white
            borderWidth = 2
            borderColor = GlobalConstants.LIGHT_GRAY
            break
        case "ZR":
            description = "Special Reads"
            backgroundColor = GlobalConstants.YELLOW
            break
        case "YD":
            description = "Dunning Redlock"
            backgroundColor = GlobalConstants.RED
            textColor = .white
            break
        case "YC":
            description = "Temp. Lock"
            backgroundColor = .black
            textColor = .white
            break
        case "YE":
            description = "Lock"
            backgroundColor = .black
            textColor = .white
            break
        default:
            //print("ERROR: Invalid Service Request Code provided: \(code)")
            description = ""
            backgroundColor = .black
            textColor = .white
        }
    }
}
