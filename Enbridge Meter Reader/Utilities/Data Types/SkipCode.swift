//
//  SkipCode.swift
//  Enbridge Meter Reader
//
//  Created by Wentzel, Johann (CA - Alberta) on 2018-01-19.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import Foundation

struct SkipCode: Codable {
    let id: String
    let description: String
    
    init(id: String, description: String){
        self.id = id
        self.description = description
    }
    
    static let fullList = [
            SkipCode(id: "0", description: "No Skip Code"),
            SkipCode(id: "1", description: "Customer Out"),
            SkipCode(id: "2", description: "Meter Blocked"),
            SkipCode(id: "3", description: "No Access"),
            SkipCode(id: "4", description: "Meter Out"),
            SkipCode(id: "5", description: "Condensation"),
            SkipCode(id: "6", description: "Gate Locked"),
            SkipCode(id: "7", description: "Meter Buried"),
            SkipCode(id: "8", description: "Can't Locate Meter"),
            SkipCode(id: "9", description: "Dog On Premise"),
            SkipCode(id: "10", description: "Meter Room locked"),
            SkipCode(id: "11", description: "Vacant"),
            SkipCode(id: "12", description: "Unsafe Condition"),
            SkipCode(id: "13", description: "Meter Under Porch"),
            SkipCode(id: "14", description: "Snow Covered"),
            SkipCode(id: "15", description: "Left Card"),
            SkipCode(id: "16", description: "Meter Too Low"),
            SkipCode(id: "17", description: "Meter Too High"),
            SkipCode(id: "18", description: "Meter Enclosed"),
            SkipCode(id: "19", description: "Night Lock On"),
            SkipCode(id: "20", description: "Super Out"),
            SkipCode(id: "21", description: "Occupied"),
            SkipCode(id: "22", description: "Bsmt Flooded"),
            SkipCode(id: "23", description: "Gate Frozen"),
            SkipCode(id: "24", description: "Glass Scratched"),
            SkipCode(id: "25", description: "Contact Meter reading"),
            SkipCode(id: "26", description: "Garage Locked"),
            SkipCode(id: "27", description: "Can't Reach Latch"),
            SkipCode(id: "28", description: "Gate Blocked"),
            SkipCode(id: "30", description: "Wrong Route"),
            SkipCode(id: "32", description: "No Key"),
            SkipCode(id: "33", description: "Key Wont Turn"),
            SkipCode(id: "34", description: "Lock Changed"),
            SkipCode(id: "35", description: "Cant OMR"),
            SkipCode(id: "36", description: "Boxed-Cant Read"),
            SkipCode(id: "39", description: "Refused Access"),
            SkipCode(id: "40", description: "Closed"),
            SkipCode(id: "42", description: "Key Not Available"),
            SkipCode(id: "50", description: "Special Arrangement"),
            SkipCode(id: "51", description: "Stray Dog"),
            SkipCode(id: "52", description: "Loose Dog"),
            SkipCode(id: "53", description: "Dog tied - meter"),
            SkipCode(id: "55", description: "Extreme Weather"),
            SkipCode(id: "56", description: "Door Frozen"),
            SkipCode(id: "69", description: "Screen Dr Locked"),
            SkipCode(id: "70", description: "Low Battery"),
            SkipCode(id: "71", description: "Dead Battery"),
            SkipCode(id: "73", description: "Cross Meter Exchange"),
            SkipCode(id: "81", description: "Mtr Locked in Cage"),
            SkipCode(id: "86", description: "Bugs/Debris Inside Meter"),
            SkipCode(id: "91", description: "Not Reached - Estimate"),
            SkipCode(id: "98", description: "CSR - Meter Reading Reported by Customer"),
            SkipCode(id: "99", description: "Contact Meter reading")
    ]
    
    // So we can equate two skip codes.
    static func ==(first: SkipCode, second: SkipCode?) -> Bool {
        if second == nil { return false }
        return first.id == second!.id && first.description == second!.description
    }
    
}
