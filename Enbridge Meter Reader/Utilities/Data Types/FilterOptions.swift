//
//  FilterOptions.swift
//  Enbridge Meter Reader
//
//  Created by Wentzel, Johann (CA - Alberta) on 2018-02-07.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import Foundation

class FilterOptions {
    
    static let periodic: [String: (MeterRead) -> Bool] = [
        "All Meters": {(read: MeterRead) -> Bool in
            return true
        },
        "Meters Skipped": {(read: MeterRead) -> Bool in
            return read.skipCode != nil
        },
        "Meters Missed": {(read: MeterRead) -> Bool in
            return read.status != .completed && read.skipCode == nil
        },
        "Meters with Trouble Codes": {(read: MeterRead) -> Bool in
            return read.troubleCodes.count > 0
        },
        "High Estimates": {(read: MeterRead) -> Bool in
            return read.critical
        }
    ]
    
    static let outcard: [String: (MeterRead) -> Bool] = [
        "All Meters": {(read: MeterRead) -> Bool in
            return true
        },
        "Meters Missed": {(read: MeterRead) -> Bool in
            return read.status != .completed
        },
        "Meters with Trouble Codes": {(read: MeterRead) -> Bool in
            return read.troubleCodes.count > 0
        },
        "Estimates of 3": {(read: MeterRead) -> Bool in
            guard read.skips != nil else {return false}
            return read.skips! == 3
        },
        "Estimates of 4+": {(read: MeterRead) -> Bool in
            guard read.skips != nil else {return false}
            return read.skips! >= 4
        }
    ]
    
    static let offCycle: [String: (MeterRead) -> Bool] = [
        "All Meters": {(read: MeterRead) -> Bool in
            return true
        },
        "Meters Missed": {(read: MeterRead) -> Bool in
            return (read.status != .completed && read.status != .pending)
        },
        "Meters with Trouble Codes": {(read: MeterRead) -> Bool in
            return read.troubleCodes.count > 0
        },
        "Cancelled": {(read: MeterRead) -> Bool in
            return read.typeCode == "YD" && read.cancelled
        },
        "ZV - Move Orders": {(read: MeterRead) -> Bool in
            return read.typeCode == "ZV"
        },
        "ZR - Special Reads": {(read: MeterRead) -> Bool in
            return read.typeCode == "ZR"
        },
        "ZB - Battery Orders": {(read: MeterRead) -> Bool in
            return read.typeCode == "ZB"
        },
        "YD - Dunning Redlock": {(read: MeterRead) -> Bool in
            return read.typeCode == "YD"
        },
        "YC - Temp. Lock": {(read: MeterRead) -> Bool in
            return read.typeCode == "YC"
        },
        "YE - Lock": {(read: MeterRead) -> Bool in
            return read.typeCode == "YE"
        },
        "ZT - Tamper Lock": {(read: MeterRead) -> Bool in
            return read.typeCode == "ZT"
        }
    ]
}



