//
//  Utilities.swift
//  Enbridge Meter Reader
//
//  Created by Deshpande, Ankush (CA - Toronto) on 2018-01-15.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import Foundation
import UIKit
import AudioToolbox
import SAPOData
import SAPCommon

class Utilities {

    static let sharedInstance = Utilities()
    var userName : String?
    var userRoleHighLow:Bool?
    static var currentMeter : MeterRead!
    
    func fetchCurrentSystemDate() -> (start:String,end:String) {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let formattedCurrentDateString = formatter.string(from: Date().twentyFiveDaysBeforeNow)
        let formattedTwoWeekAheadDateString = formatter.string(from: Date().twoWeeksfromNow)
        
        
        //print("formatted Current Date String: \(formattedCurrentDateString)")
        //print("Two Week ahead date: \(formattedTwoWeekAheadDateString)")
        
        return ("\(formattedCurrentDateString)T00:00:00",
            "\(formattedTwoWeekAheadDateString)T23:00:00")
    }
    
    func fetchDate() -> (String) {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let formattedDateString = formatter.string(from: Date())
        
        //print("formattedDateString: \(formattedDateString)")
        
        return ("\(formattedDateString)")
    }
    
    func checkForNetworkStatus(_ meterRead: MeterRead) -> MeterReadStatus {
        switch meterRead.mmrStatus {
        case MeterReadNetworkStatus.DISP.rawValue:
            return .available
        case MeterReadNetworkStatus.CANCEL.rawValue:
            return .completed
        case MeterReadNetworkStatus.ACK.rawValue:
            return .available
        case MeterReadNetworkStatus.EN_Route.rawValue:
            return .available
        case MeterReadNetworkStatus.ON_SITE.rawValue:
            return .inProgress
        case MeterReadNetworkStatus.FIELD_COMPLETE.rawValue:
            return .completed
        case MeterReadNetworkStatus.MISSED.rawValue:
            return .available
        default:
            return .completed
        }
    }
    
    // Alert that submits work order - used for offcycle, calls post work service to SCP
    static func showAlertOn(_ viewController: UIViewController, segueName: String){
        AudioServicesPlaySystemSound(1315);
        let alertController = UIAlertController(title: "Submit Work Order", message: "\n Are you sure you want to submit the work order?", preferredStyle: .alert)
        
        let confirmAction = UIAlertAction(title: "Confirm", style: .default) { [weak viewController] action in
            
            DispatchQueue.main.async {
                if let tabsController = UIApplication.shared.delegate?.window??.rootViewController as? MainNavViewController {
                    if let selectVC = tabsController.viewControllers![0].children.first as? SelectRouteTableViewController{
                        selectVC.loadBlurView()
                    }
                }
            }
            
            
            do {
                Utilities.currentMeter.mmrStatus = "FIELD COMPLETE"
                if !AppDelegate.hasMockData {
                    let result = try LocalStoreManager.ShareInstance?.updateOperation(type: RouteType.OFFCYCLE.rawValue, operation: MeterRead.mapTo_Operation(meterRead: Utilities.currentMeter, withCompletionCode: String(describing: Utilities.currentMeter.completionCode!.number!)))
                    
//                    if result?.0 == 200 {
//                        LogHelper.shared?.info("Change Status to Field-Complete successfully with work ID:" + Utilities.currentMeter.jobID, loggerObject: (self.logger)!)
//                    }else{
//                        LogHelper.shared?.error("Change Status to Field-Complete failed with work ID:" + Utilities.currentMeter.jobID, loggerObject: (self.logger)!)
//                    }
                    print("Fetched From UpdateEntity Function Call:\(String(describing: result?.0)),\(String(describing: result?.1))")
                }
            } catch {
                print("Update Operation Failed: \(error)")
//                 LogHelper.shared?.error("Change Status to Field-Complete failed with work ID:" + Utilities.currentMeter.jobID, loggerObject: (self.logger)!)
            }
            LocalStoreManager.ShareInstance?.submitOffCycleRoute{error in
                DispatchQueue.main.async {
                    SelectRouteTableViewController.refreshControlRef?.endRefreshing()
                    if let tabsController = UIApplication.shared.delegate?.window??.rootViewController as? MainNavViewController {
                        if let selectVC = tabsController.viewControllers![0].children.first as? SelectRouteTableViewController{
                            selectVC.unloadBlurView()
                        }
                    }
                }
            }
            viewController?.performSegue(withIdentifier: segueName, sender: self)
            NotesViewController.caller = nil
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default) { action in
            // ...
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(confirmAction)
        
        viewController.present(alertController, animated: true) {
            // ...
        }
    }
}

// Checks if the device is the iOS Simulator.
// Example of usage: UIDevice.current.isSimulator
extension UIDevice {
    var isSimulator: Bool {
        #if IOS_SIMULATOR
            return true
        #else
            return false
        #endif
    }
}

extension UITableView {
    func scrollToFirstRow() {
        if self.numberOfRows(inSection: 0) == 0 { return }
        let indexPath = IndexPath(row: 0, section: 0)
        self.scrollToRow(at: indexPath, at: .top, animated: true)
    }
}


