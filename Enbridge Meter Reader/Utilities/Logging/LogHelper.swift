//
//  LogHelper.swift
//  Enbridge Meter Reader
//
//  Created by Wentzel, Johann (CA - Alberta) on 2018-06-26.
//  Copyright © 2018 Enbridge. All rights reserved.
//

// ----- HOW TO USE -----
// 1) Create an object of type Logger by using: let logger = Logger.shared(named: “<Your custom logger name>“)
// 2) Set the logger’s log level to debug by using: logger.logLevel = .debug
// 3) Create the log by using: LogHelper.shared?.debug(“Your log message here“, loggerObject: self.logger)
// You can log at different levels by swapping .debug() for .warn(), .error(), .info()

import UIKit
import CoreData
import SAPFiori
import SAPFioriFlows
import SAPFoundation
import SAPCommon
import UserNotifications
import SAPOData
import MapKit

class LogHelper: NSObject {
    
    static var shared: LogHelper?
    let urlSession: SAPURLSession?
    let settingsParameters: SAPcpmsSettingsParameters?
    var uploading = false
    
    // this logger object actually tracks the performance of the
    // LogHelper itself. So meta!
    let selfLogger = Logger.shared(named: "LogHelper")
    
    // Custom prefix so we can see exactly what was logged by iOS.
    let customPrefix = "EGD_iOS: "
    
    init(urlSession: SAPURLSession, settingsParameters: SAPcpmsSettingsParameters) {
        self.urlSession = urlSession
        self.settingsParameters = settingsParameters
        selfLogger.logLevel = .debug
    }
    
    private var currentTime: String {
        get {
            let time = Date()
            let formatter = DateFormatter()
            formatter.timeZone = TimeZone.current
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            return formatter.string(from: time)
        }
    }
    
    // MARK: - Log uploading
    // These funcitions work similarly to the SAP Logger.info, Logger.warn, etc. functions,
    // but they also upload the log automatially upon completion.
    
    func error(_ msg: String, loggerObject: Logger, error: Error? = nil, fileName: String = #file, functionName: String = #function, lineNumber: Int = #line){
        
        if error != nil {
            loggerObject.error(customPrefix + "(\(currentTime)) " + msg, error: error, fileName: fileName, functionName: functionName, lineNumber: lineNumber)
        }
        else {
            loggerObject.error(customPrefix + "(\(currentTime)) " + msg, fileName: fileName, functionName: functionName, lineNumber: lineNumber)
        }
        //upload()
    }
    
    func warn(_ msg: String, loggerObject: Logger, error: Error? = nil, fileName: String = #file, functionName: String = #function, lineNumber: Int = #line){
        if error != nil {
            loggerObject.warn(customPrefix + "(\(currentTime)) " + msg, error: error, fileName: fileName, functionName: functionName, lineNumber: lineNumber)
        }
        else {
            loggerObject.warn(customPrefix + "(\(currentTime)) " + msg, fileName: fileName, functionName: functionName, lineNumber: lineNumber)
        }
        //upload()
    }
    
    func info(_ msg: String, loggerObject: Logger, fileName: String = #file, functionName: String = #function, lineNumber: Int = #line){
        loggerObject.info(customPrefix + "(\(currentTime)) " + msg, fileName: fileName, functionName: functionName, lineNumber: lineNumber)
        //upload()
    }
    
    func debug(_ msg: String, loggerObject: Logger, fileName: String = #file, functionName: String = #function, lineNumber: Int = #line){
        //print("Log entry text: \(msg)")
        loggerObject.debug(customPrefix + "(\(currentTime)) " + msg, fileName: fileName, functionName: functionName, lineNumber: lineNumber)
        //upload()
    }
    
    func upload() {
        do {
            Logger.shared(named: "SAP.OData").logLevel = .debug
            Logger.shared(named: "SAP.OData.Offline").logLevel = .debug
            guard urlSession != nil && settingsParameters != nil else { return }
            uploadLogs(urlSession!, settingsParameters!)
        }
//        } catch {
//            print(error)
//        }
    }
    
    func test(){
        selfLogger.info("test 1")
        selfLogger.info("test 2")
        selfLogger.info("test 3")
        selfLogger.info("test 4")
        selfLogger.info("test 5")
        upload()
    }
    
    // MARK: - SAP Functions
    // SAP built-in log upload function
    private func uploadLogs(_ urlSession: SAPURLSession, _ settingsParameters: SAPcpmsSettingsParameters) {
        if !uploading {
            uploading = true
            SAPcpmsLogUploader.uploadLogs(sapURLSession: urlSession, settingsParameters: settingsParameters) { error in
                if error != nil {
                    self.selfLogger.error("Error happened during log upload.", error: error!)
                    print("ERROR: \(error!)")
                    return
                }
                self.selfLogger.info("Logs have been uploaded successfully.")
                self.uploading = false
            }
        }
    }
}
