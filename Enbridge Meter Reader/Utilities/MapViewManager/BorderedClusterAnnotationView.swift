//
//  BorderedClusterAnnotationView.swift
//  Enbridge Meter Reader
//
//  Created by Deshpande, Ankush (CA - Toronto) on 2018-01-19.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class BorderedClusterAnnotationView: ClusterAnnotationView {
    let brColor: UIColor
    
    init(annotation: MKAnnotation?, reuseIdentifier: String?, style: ClusterAnnotationStyle, brColor: UIColor) {
        self.brColor = brColor
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier, style: style)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func configure(with style: ClusterAnnotationStyle) {
        super.configure(with: style)
        
        switch style {
        case .image:
            layer.borderWidth = 0
        case .color:
            layer.borderColor = brColor.cgColor
            layer.borderWidth = 2
        }
    }
}
