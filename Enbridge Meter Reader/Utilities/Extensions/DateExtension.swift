//
//  DateExtension.swift
//  Enbridge Meter Reader
//
//  Created by Wentzel, Johann (CA - Alberta) on 2018-07-03.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import Foundation

extension Date {
    var fiveDaysAgo: Date {
        return Calendar.current.date(byAdding: .day, value: -5, to: noon)!
    }
    var aAgo: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
    }
    var tomorrow: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: noon)!
    }
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    var month: Int {
        return Calendar.current.component(.month,  from: self)
    }
    var sixPMOfTodayEvening: Date {
        var calendar = Calendar.current
        calendar.timeZone = .current
        return calendar.date(bySettingHour: 18, minute: 0, second: 0, of: Date())!
    }
    var lastSecondOfFiveDaysAgo: Date {
        var calendar = Calendar.current
        calendar.timeZone = .current
        return calendar.date(bySettingHour: 23, minute: 59, second: 59, of: fiveDaysAgo)!
    }
    var lastSecondOfDayAgo: Date {
        var calendar = Calendar.current
        calendar.timeZone = .current
        return calendar.date(bySettingHour: 23, minute: 59, second: 59, of: aAgo)!
    }
    var isLastDayOfMonth: Bool {
        return tomorrow.month != month
    }
}
