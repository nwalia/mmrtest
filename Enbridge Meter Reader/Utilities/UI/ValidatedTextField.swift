//
//  ValidatedTextField.swift
//  Enbridge Meter Reader
//
//  Created by Wentzel, Johann (CA - Alberta) on 2017-12-13.
//  Copyright © 2017 Enbridge. All rights reserved.
//

import UIKit

class ValidatedTextField: UITextField, UITextFieldDelegate {
    
    private var isValid: Bool = true
    
    @IBInspectable
    public var mandatory: Bool = false
    
    @IBInspectable
    public var minLength: Int = 0
    
    @IBInspectable
    public var maxLength: Int = Int.max
    
    @IBInspectable
    public var allowNumbers: Bool = true {
        didSet(newValue){
            if newValue == false {
                self.allCharacters = false
            }
        }
    }

    @IBInspectable
    public var allowLetters: Bool = true {
        didSet(newValue){
            if newValue == false {
                self.allCharacters = false
            }
        }
    }
    
    @IBInspectable
    public var specialChars: String = "" {
        didSet(newValue){
            if newValue != "" {
                self.allCharacters = false
            }
        }
    }
    
    @IBInspectable
    public var allCharacters: Bool = false {
        didSet (newValue){
            if newValue == true {
                allowNumbers = true
                allowLetters = true
            }
        }
    }

    
    // --- Confirm from Shreyas: 2 validate method which one to use --- //
    // -- IF validate, then change argument to command, otherwise rename it --- //
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        self.delegate = self
        self.addTarget(self, action: #selector(valideTxtFld(_:)), for: .editingChanged)
        
        valideTxtFld(self)

    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    @objc func valideTxtFld(_ textField: UITextField){
        // Specifically sets the isValid variable, to be checked by parent views.
        
        if mandatory && textField.text!.isEmpty {
            isValid = false
            return
        }
        
        if textField.text!.count < minLength {
            isValid = false
            return
        }
        
        if textField.text!.count > maxLength {
            isValid = false
            return
        }
        
        isValid = true
    }
    
    public func getValidity() -> Bool {
        valideTxtFld(self)
        return isValid
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        // Specifically blocks character input if outside validity parameters.
        
        var availableChars = CharacterSet()
        
        if allowLetters {
            availableChars = availableChars.union(CharacterSet.letters)
        }
        
        if allowNumbers {
            availableChars = availableChars.union(CharacterSet.decimalDigits)
        }
        
        if specialChars != "" {
            availableChars.insert(charactersIn: specialChars)
        }
        
        // format string so that if the user typed backspace, isBackspace is true
        let char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = (strcmp(char, "\\b") == -92)

        // validation for characters
        if !allCharacters
            && string.rangeOfCharacter(from: availableChars) == nil
            && !isBackSpace {
            return false
        }
        
        // validation for length
        if (textField.text! + string).count > maxLength {
            return false
        }
        
        return true
    }
    
    

}
