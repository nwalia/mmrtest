  //
//  Customer Information.swift
//  Enbridge Meter Reader
//
//  Created by Deshpande, Ankush (CA - Toronto) on 2017-12-28.
//  Copyright © 2017 Enbridge. All rights reserved.
//

import Foundation
import UIKit

protocol CustomerInformationCellDelegate: class {
    func editMeterLocationButtonPressed()
    func editMeterNumberButtonPressed()
}

@IBDesignable

class CustomerInformation: Visit, DashboardDelegate, ServiceRequestDelegate, PeriodicDashboardDelegate, PeriodicOutcardDelegate {
    
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var locationDescriptionLabel: UILabel!
    @IBOutlet weak var locationShortLabel: UILabel!
    @IBOutlet weak var editMeterNumberButton: UIButton!
    @IBOutlet weak var editLocationButton: UIButton!
    @IBOutlet weak var keyView: UIView!
    @IBOutlet weak var keyNumberLabel: UILabel!
    @IBOutlet weak var keyStackView: UIStackView!
    @IBOutlet weak var timeView: UIView!
    @IBOutlet weak var timeOrSkipsLabel: UILabel!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var riserOnWall: UILabel!
    @IBOutlet weak var riserFromWall: UILabel!
    @IBOutlet weak var riserDistance: UILabel!
    
    static weak var delegate: CustomerInformationCellDelegate?
    
    override func awakeFromNib() {
        
        // Listening for commands from Dashboard and Service Request
        DashboardViewController.customerInformationDelegate = self
        PeriodicDashboardViewController.delegate = self
        ServiceRequestScreenViewController.delegate = self
        PeriodicOutcardScreenViewController.delegate = self
        super.awakeFromNib()
        
    }
    
    
    @IBAction func editMeterNumberButtonPressed(_ sender: UIButton) {
        CustomerInformation.delegate?.editMeterNumberButtonPressed()
    }
    
    @IBAction func editMeterLocationButtonPressed(_ sender: UIButton) {
        CustomerInformation.delegate?.editMeterLocationButtonPressed()
    }
    
    // --- This method is called whenever customer information screen appears --- //
    func populateCustomerInformation(onDashboard: Bool, hideLine: Bool, newRead: MeterRead, buttonsEnabled: Bool) {
        
        
        // Hide edit buttons and remove line if on Dashboard
        if onDashboard {
            editLocationButton!.isHidden = true
            editMeterNumberButton!.isHidden = true
            
        } else {
            editLocationButton!.isHidden = false
            
            // --- IF screen is not dashboard, then in case of offcycle: hide editmeterbutton --- //
            if let newReadType = newRead.type , newReadType == "Offcycle" {
                editMeterNumberButton!.isHidden = true
            }
            else{
                editMeterNumberButton!.isHidden = false
            }
        }
        
        if hideLine {
            lineView.isHidden = true
        } else {
            lineView.isHidden = false
        }
        
        // Set customer information data
        if let newMeterNumber = newRead.newMeterNumber, newMeterNumber != "" {
            meterNumberLabel.text = "\(newRead.newSizeCode!)\(newMeterNumber)"
        } else {
            meterNumberLabel.text = "\(newRead.sizeCode)\(newRead.meterNumber)"
        }
        nameLabel.text = newRead.nameLastFirstComma
        addressLine1Label.text = newRead.fullAddress
        addressLine2Label.text = newRead.addressLine2
        phoneNumberLabel.text = newRead.phoneNumber
        var locationShortDescriptionText = " "
        newRead.locationCode.id.isEmpty ? (locationShortDescriptionText = " ") : (locationShortDescriptionText = "\(newRead.locationCode.id)")
        let attrStringForShrtLocationDescription = NSMutableAttributedString(string: locationShortDescriptionText)
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 8
        attrStringForShrtLocationDescription.addAttribute(NSAttributedString.Key.paragraphStyle, value: style, range: NSRange(location: 0, length: locationShortDescriptionText.count))
        locationShortLabel.attributedText = attrStringForShrtLocationDescription
        
        self.checkForRiserDetail(meterRead: newRead)
        
        let locationDescriptionText = "\(newRead.locationCode.number) \(newRead.locationCode.id) \(newRead.locationCode.description)"
        let attrString = NSMutableAttributedString(string: locationDescriptionText)
        attrString.addAttribute(NSAttributedString.Key.paragraphStyle, value: style, range: NSRange(location: 0, length: locationDescriptionText.count))
        locationDescriptionLabel.attributedText = attrString
        
        // Set time or skip UI
        if newRead.type == "Offcycle" && newRead.appointmentTime != nil && newRead.appointmentTime != "" {
            timeOrSkipsLabel.isHidden = false
            timeOrSkipsLabel.text = newRead.appointmentTime
            timeView.isHidden = false
            switch newRead.appointmentTime {
            case "9AM-12PM"?:
                self.timeView.backgroundColor = UIColor.enbridgeYellow
            case "12PM-4PM"?:
                self.timeView.backgroundColor = UIColor.clementine
            case "4PM-9PM"?:
                self.timeView.backgroundColor = UIColor.dusk
            default:
                self.timeView.backgroundColor = UIColor.clear
            }
        } else {
            timeOrSkipsLabel.isHidden = true
            timeView.isHidden = true
            if newRead.type == "Periodic" || newRead.type == "Outcard"{
                timeOrSkipsLabel.isHidden = false
                if let skips = newRead.skips {
                    if skips != 0{
                        timeOrSkipsLabel.text = "Est: \(skips)"
                    }else{
                        timeOrSkipsLabel.text = "Est: \("")"
                    }
                }
            }
        }
        
        // Set key UI
        if newRead.keyNumber != nil && !(newRead.keyNumber ?? "").isEmpty {
            keyStackView.isHidden = false
            keyNumberLabel.text = newRead.keyNumber
        } else {
            keyStackView.isHidden = true
        }
        
        setButtonState(isEnabled: buttonsEnabled)
        
    }
    
    func didRefreshDataModel() {
        //
    }
    
    func checkForRiserDetail(meterRead: MeterRead) {
            switch meterRead.locationCode.id {
            case "AB", "AF", "AL", "AO", "AG", "AR", "AT":
                hideRiserLabel()
            default:
                self.riserDistance.text = (meterRead.mmrRiserDist ?? "").isEmpty ? "" : "RiserDistance: \(meterRead.mmrRiserDist!)"
                self.riserOnWall.text = (meterRead.mmrRiserOnWall ?? "").isEmpty ? "" : "RiserOnWall: \(meterRead.mmrRiserOnWall!)"
                self.riserFromWall.text = (meterRead.mmrRiserFromWall ?? "").isEmpty ? "" : "RiserFromWall: \(meterRead.mmrRiserFromWall!)"
            }
    }
    
    func hideRiserLabel() {
        self.riserDistance.text = ""
        self.riserOnWall.text = ""
        self.riserFromWall.text = ""
    }
    
    func setButtonState(isEnabled: Bool){
        editLocationButton.isEnabled = isEnabled
        editLocationButton.backgroundColor = isEnabled ? GlobalConstants.GREEN : GlobalConstants.LIGHT_GRAY
        
        editMeterNumberButton.isEnabled = isEnabled
        editMeterNumberButton.backgroundColor = isEnabled ? GlobalConstants.GREEN : GlobalConstants.LIGHT_GRAY
    }
    
    
}
