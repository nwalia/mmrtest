//
//  NumKeypad.swift
//  Enbridge Meter Reader
//
//  Created by Deshpande, Ankush (CA - Toronto) on 2018-01-05.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import Foundation
import UIKit
import AudioToolbox

protocol NumKeypadDelegate: class {
    func didTapOn(number:String?)
    func didBackspace()
}

class NumKeypad: UIView {
    @IBOutlet var numberButton: [UIButton]!
    static let recommendedHeight = 313.0
    
    static weak var delegate: NumKeypadDelegate? {
        willSet(newValue){
            self.delegate = newValue
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeSubviews()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeSubviews()
    }
    
    func initializeSubviews() {
        let xibFileName = "NumKeypad" // xib extention not included
        let view = Bundle.main.loadNibNamed(xibFileName, owner: self, options: nil)![0] as! UIView
        self.addSubview(view)
        view.frame = self.bounds
    }
    
    @IBAction func didPressNumber(_ sender: UIButton) {
//        UIDevice.current.playInputClick()
        AudioServicesPlaySystemSound(1104);
        if (sender.titleLabel?.text == "X"){
            NumKeypad.delegate?.didBackspace()
        }
        else{
            NumKeypad.delegate?.didTapOn(number: sender.titleLabel?.text)
        }
    }
}
