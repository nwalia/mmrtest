//
//  TimeElapsedView.swift
//  Enbridge Meter Reader
//
//  Created by Wentzel, Johann (CA - Alberta) on 2018-01-02.
//  Copyright © 2018 Enbridge. All rights reserved.

// HOW TO USE:
// Add a view in interface builder, declare its class to be TimeElapsedView.
// Connect to your ViewController, and call either populate() or populateWithDummyData()

import UIKit

@IBDesignable
class TimeElapsedView: ReusableView {
  
    @IBOutlet weak var barLabel: UILabel!
    @IBOutlet weak var fillBarContainer: UIView!
    
    var timer: Timer?
    var startTime: Date?
    
    fileprivate func getStartTime(_ routeId: String) {
        // check if StartTimes already exists
        if UserDefaults.standard.dictionary(forKey: "StartTimes") != nil {
            // check if current route already has a startTime
            if UserDefaults.standard.dictionary(forKey: "StartTimes")![routeId] != nil {
                startTime = UserDefaults.standard.dictionary(forKey: "StartTimes")![routeId] as? Date
            }
            else {
                // no startTime found. creating new entry in StartTimes dict.
                startTime = Date()
                var startTimesDict = UserDefaults.standard.dictionary(forKey: "StartTimes")!
                startTimesDict[routeId] = startTime
                UserDefaults.standard.set(startTimesDict, forKey: "StartTimes")
            }
        }
        else {
            // no previous routes started. Creating StartTimes dict.
            startTime = Date()
            let newDict = [routeId:startTime]
            UserDefaults.standard.set(newDict, forKey: "StartTimes")
        }
    }
    
    func resetTimer(_ routeId: String){
        if UserDefaults.standard.dictionary(forKey: "StartTimes") != nil {
            // check if current route already has a startTime
            if UserDefaults.standard.dictionary(forKey: "StartTimes")![routeId] != nil {
                var startTimesDict = UserDefaults.standard.dictionary(forKey: "StartTimes")!
                startTimesDict[routeId] = nil
                UserDefaults.standard.set(startTimesDict, forKey: "StartTimes")
            }
        }
    }
    
    fileprivate func setBarColor() {
        // updating UI
        fillBarContainer.backgroundColor = GlobalConstants.GREEN
        barLabel.textColor = .white
    }
    
    func populate(routeId: String){
        
        getStartTime(routeId)
        
        setBarColor()
        
        
        // start timer - recalculate time every second so the time elapsed updates every second
        if Model.sharedInstance.routes[routeId]?.completionTime == nil {
            updateTimeLabel()
            if timer == nil {
                timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(updateTimeLabel)), userInfo: nil, repeats: true)
                RunLoop.current.add(self.timer!, forMode: RunLoop.Mode.common)
            }
        }
        else {
            updateTimeLabelWithEndTime((Model.sharedInstance.routes[routeId]?.completionTime))
        }
    
    }

    func generateTestDate() -> Date {
        var dateComponents = DateComponents()
        dateComponents.year = 2018
        dateComponents.month = 1
        dateComponents.day = 2
        dateComponents.timeZone = TimeZone(abbreviation: "EST")
        dateComponents.hour = 18
        dateComponents.minute = 0
        
        
        
        let userCalendar = Calendar.current // user calendar
        let date = userCalendar.date(from: dateComponents)
        
        return date!
    }
    
    func populateWithDummyData(){
        // Stub code - adds a test dict [String:Date] to UserDefaults to facilitate reading start times by meter number
        var dateComponents = DateComponents()
        dateComponents.year = 2018
        dateComponents.month = 1
        dateComponents.day = 2
        dateComponents.timeZone = TimeZone(abbreviation: "EST")
        dateComponents.hour = 12
        dateComponents.minute = 0
        
        let userCalendar = Calendar.current // user calendar
        let date = userCalendar.date(from: dateComponents)
        
        let testDict = ["1234": date]
        UserDefaults.standard.set(testDict, forKey: "StartTimes")
        
        self.populate(routeId:"1234")

    }
    
    // sets the time label to an appropriate format.
    @objc func updateTimeLabel(){
        
        let diffTimeHours = Calendar.current.dateComponents([.hour], from: startTime!, to: Date()).hour!
        let diffTimeMinutes = Calendar.current.dateComponents([.minute], from: startTime!, to: Date()).minute! % 60
        let diffTimeSeconds = Calendar.current.dateComponents([.second], from: startTime!, to: Date()).second! % 60
        
        var timeString: String!
        
        if diffTimeHours > 0 {
            timeString = "\(diffTimeHours):\(String(format: "%02d", arguments:[diffTimeMinutes])):\(String(format: "%02d", arguments:[diffTimeSeconds]))"
        }
        else {
            timeString = "\(diffTimeMinutes):\(String(format: "%02d", arguments:[diffTimeSeconds]))"
        }
        
        barLabel.text = "Elapsed Time - " + timeString
    }
    
    func stopTimer(){
        if timer != nil {
            timer!.invalidate()
            timer = nil
        }
        updateTimeLabelWithEndTime((Model.sharedInstance.routes[Model.selectedRouteKey]?.completionTime))
    }
    
    func updateTimeLabelWithEndTime(_ endTime: Date?){
        
        if startTime == nil {
            getStartTime(Model.selectedRouteKey)
        }
        guard let endingTime = endTime else {
            return
        }
        let diffTimeHours = Calendar.current.dateComponents([.hour], from: startTime!, to: endingTime).hour!
        let diffTimeMinutes = Calendar.current.dateComponents([.minute], from: startTime!, to: endingTime).minute! % 60
        let diffTimeSeconds = Calendar.current.dateComponents([.second], from: startTime!, to: endingTime).second! % 60
        
        var timeString: String!
        
        if diffTimeHours > 0 {
            timeString = "\(diffTimeHours):\(String(format: "%02d", arguments:[diffTimeMinutes])):\(String(format: "%02d", arguments:[diffTimeSeconds]))"
        }
        else {
            timeString = "\(diffTimeMinutes):\(String(format: "%02d", arguments:[diffTimeSeconds]))"
        }
        
        barLabel.text = "Elapsed Time - " + timeString
        
        setBarColor()
    }
}
