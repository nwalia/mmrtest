//
//  NetworkStatusBarView.swift
//  Enbridge Meter Reader
//
//  Created by Wentzel, Johann (CA - Alberta) on 2018-04-23.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import UIKit

class NetworkStatusBarView: ReusableView {
    
    // Used to populate the bar.
    enum NetworkStatus {
        case offline
        case online
    }
    
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var viewText: UILabel!
    
    
    // Populates the bar based on specified status.
    func populate(withStatus status: NetworkStatus){
        if status == .offline {
            backgroundView.backgroundColor = .red
            viewText.text = "You are not connected to the network. Route will be sent when connection is restored."
        }
        else {
            backgroundView.backgroundColor = .green
            viewText.text = "Connected. Route has been sent."
        }
    }
    
    
}
