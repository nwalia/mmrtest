//
//  Special Instruction.swift
//  Enbridge Meter Reader
//
//  Created by Deshpande, Ankush (CA - Toronto) on 2017-12-28.
//  Copyright © 2017 Enbridge. All rights reserved.
//

import Foundation
import UIKit

protocol SpecialInstructionDelegate: class {
    func segue()
}
@IBDesignable
class SpecialInstruction: Visit {
    
    static weak var delegate: SpecialInstructionDelegate? {
        willSet(newValue){
            self.delegate = newValue
        }
    }
    
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var instructionLabel: UILabel!
    
    @IBAction func didPressMeterInformationButton(_ sender: UIButton) {
        
        /* dirty hack saved the day :) */
//        let storyBoard = UIStoryboard(name: "ServiceRequestScreen", bundle: nil)
//        let initViewController: UIViewController? = storyBoard.instantiateInitialViewController()
//        let navigationController = window?.rootViewController as? UINavigationController
//        navigationController?.pushViewController(initViewController!, animated: true)
        
        //actual way of doing it.
        SpecialInstruction.delegate?.segue()
    }
}
