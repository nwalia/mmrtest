//
//  SelectCompletionCodeViewController.swift
//  Enbridge Meter Reader
//
//  Created by Johann Wentzel on 2017-11-29.
//  Copyright © 2017 Enbridge. All rights reserved.
//

import Foundation
import UIKit
import AudioToolbox

class SelectCompletionCodeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    // MARK: - Interface outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableViewBottomConstraint: NSLayoutConstraint!
    
    
    
    // MARK: - Variables
    var codes = [CompletionCode]()
    var filteredCodes = [CompletionCode]()
    var selectedCode : CompletionCode!
    
    var newMeterNumber: String?
    var newMeterSizeCode: String?
    var newMeterNumberOfDials: Int?
    
    var returnNote: String?
    
    var type: String!
    var numberOfDials: Int!
    var meterReadEntry: String?
    var secondMeterReadEntry: String? // For battery exchanges
    
    var wantsToSegueOut = false
    
    private var _currentMeter: MeterRead?
    var currentMeter : MeterRead {
        get {
            return self._currentMeter!
        }
        set {
            self._currentMeter = newValue
            Utilities.currentMeter = newValue
        }
    }
    

    
    // MARK: - View Controller Functions
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        tableView.delegate = self
        tableView.dataSource = self

        searchBar.delegate = self
        searchBar.barTintColor = .white
        
        selectedCode = CompletionCode(number: -1, description: "", hasNoteIcon: false, type: "")
        
        codes = getDummyCodes(type)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        if wantsToSegueOut {
            performSegue(withIdentifier: "ExitToServiceRequestScreen", sender: nil)
            wantsToSegueOut = false
        }
        selectedCode = CompletionCode(number: -1, description: "", hasNoteIcon: false, type: "")
        tableView.reloadData()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func keyboardWillShow(_ notification: NSNotification){
        guard let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {return}
        let keyboardRectangle = keyboardFrame.cgRectValue
        let keyboardHeight = keyboardRectangle.height
        UIView.animate(withDuration: 0.25, animations: {() -> Void in
            self.tableViewBottomConstraint.constant = keyboardHeight * -1
        })
    }
    

    
    // MARK: - Table View Config
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isFiltering() ? filteredCodes.count : codes.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectCodeTableViewCell", for: indexPath) as! SelectCodeTableViewCell
        
        let codeForCell = isFiltering() ? filteredCodes[indexPath.row] : codes[indexPath.row]
        
        cell.iconView.layer.cornerRadius = 5
        
        if codeForCell.hasNoteIcon {
            cell.iconView.isHidden = false
            cell.iconLabel.isHidden = false
            cell.iconLabel.text = "NOTE"
            cell.iconImage.image = UIImage(named: "Note")
        }
        else if codeForCell.number == 25 {
            cell.iconView.isHidden = false
            cell.iconLabel.isHidden = false
            cell.iconLabel.text = "METER"
            cell.iconImage.image = UIImage(named: "Meter")
        }
        else {
            cell.iconView.isHidden = true
            cell.iconLabel.isHidden = true
        }
        
        if codeForCell.number == selectedCode.number
        && codeForCell.type == selectedCode.type
        && codeForCell.description == selectedCode.description
        && codeForCell.hasNoteIcon == selectedCode.hasNoteIcon {
            cell.backgroundColor = GlobalConstants.SELECTED_CELL
        }
        else{
            cell.backgroundColor = .white
        }
        
        cell.descriptionLabel.text = codeForCell.description
        if let number = codeForCell.number {
            cell.numberLabel.text = String(describing: number)
        }
        
        return cell
    }
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let codeForCell = isFiltering() ? filteredCodes[indexPath.row] : codes[indexPath.row]
        let cell = tableView.cellForRow(at: indexPath) as! SelectCodeTableViewCell
        if self.currentMeter.completionCode == nil {
            self.currentMeter.completionCode = CompletionCode(number: Int(cell.numberLabel.text!), description: "", hasNoteIcon: false, type: "")
        }else{
            self.currentMeter.completionCode?.number = Int(cell.numberLabel.text!) ?? Int()
        }
        
        selectedCode = codeForCell
        cell.backgroundColor = GlobalConstants.SELECTED_CELL
        NotesViewController.caller = self

        if (codeForCell.hasNoteIcon && codeForCell.number != 10){
            let sb = UIStoryboard(name: "NotesView", bundle: nil)
            let notesVC = sb.instantiateInitialViewController() as? NotesViewController
            notesVC?.currentMeter = self.currentMeter
            notesVC?.maxCharacters = 80
            self.navigationController?.pushViewController(notesVC!, animated: true)
            
        }
        else if (codeForCell.number == 25) {
            self.performSegue(withIdentifier: "ToExchangeMeterViewController", sender: nil)
            
        }
        else if (codeForCell.number == 10) {
            
            // If meter number changed, compare against new number of dials
            if let newNumDials = newMeterNumberOfDials {
              
                if type == "ZB" {
                    if (meterReadEntry?.count != newNumDials) || (secondMeterReadEntry?.count != newNumDials) {
                        showAlert(code: codeForCell.number)
                    } else {
                        //Utilities.showAlertOn(self, segueName: "ExitToServiceRequestScreen")
                        let sb = UIStoryboard(name: "NotesView", bundle: nil)
                        let notesVC = sb.instantiateInitialViewController() as? NotesViewController
                        notesVC?.maxCharacters = 80
                        notesVC?.currentMeter = self.currentMeter
                        self.navigationController?.pushViewController(notesVC!, animated: true)
                    }
                } else {
                    if (meterReadEntry?.count != newNumDials) {
                        showAlert()
                    } else {
                        //Utilities.showAlertOn(self, segueName: "ExitToServiceRequestScreen")
                        let sb = UIStoryboard(name: "NotesView", bundle: nil)
                        let notesVC = sb.instantiateInitialViewController() as? NotesViewController
                        notesVC?.maxCharacters = 80
                        notesVC?.currentMeter = self.currentMeter
                        self.navigationController?.pushViewController(notesVC!, animated: true)
                    }
                }
                
                
            } else {
                
                // Meter number has not been changed, compare against original number of dials
                if type == "ZB" {
                    if (meterReadEntry?.count != numberOfDials) || (secondMeterReadEntry?.count != numberOfDials) {
                        showAlert(code: codeForCell.number)
                    } else {
                        //Utilities.showAlertOn(self, segueName: "ExitToServiceRequestScreen")
                        let sb = UIStoryboard(name: "NotesView", bundle: nil)
                        let notesVC = sb.instantiateInitialViewController() as? NotesViewController
                        notesVC?.maxCharacters = 80
                        notesVC?.currentMeter = self.currentMeter
                        self.navigationController?.pushViewController(notesVC!, animated: true)
                    }
                } else {
                    if (meterReadEntry?.count != numberOfDials) {
                        showAlert()
                    } else {
                        //Utilities.showAlertOn(self, segueName: "ExitToServiceRequestScreen")
                        let sb = UIStoryboard(name: "NotesView", bundle: nil)
                        let notesVC = sb.instantiateInitialViewController() as? NotesViewController
                        notesVC?.maxCharacters = 80
                        notesVC?.currentMeter = self.currentMeter
                        self.navigationController?.pushViewController(notesVC!, animated: true)
                    }
                }
            }
            
            
            
        } else if (type == "ZB" && codeForCell.number == 27) {
            
            // If meter number changed, compare against new number of dials
            if let newNumDials = newMeterNumberOfDials {
                
                // For completion code 27, post-exchange meter read entry is required
                if ((secondMeterReadEntry?.count != newNumDials) || (meterReadEntry?.count != 0)) {
                    showAlert(code: codeForCell.number)
                } else {
                    Utilities.showAlertOn(self, segueName: "ExitToServiceRequestScreen")
                }
                
            } else {
                
                // Meter number has not been changed, compare against original number of dials
                
                // For completion code 27, post-exchange meter read entry is required
                if ((secondMeterReadEntry?.count != numberOfDials) || (meterReadEntry?.count != 0)) {
                    showAlert(code: codeForCell.number)
                } else {
                    Utilities.showAlertOn(self, segueName: "ExitToServiceRequestScreen")
                }
            }
            
            
            
        } else {
            Utilities.showAlertOn(self, segueName: "ExitToServiceRequestScreen")
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.backgroundColor = .white
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }
    
    

    

    // MARK: - Search
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filterContentForSearchText(searchBar.text!)
        tableView.reloadData()
    }
    
    func searchBarIsEmpty() -> Bool {
        return searchBar.text?.isEmpty ?? true
    }
    
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        filteredCodes = codes.filter({(code: CompletionCode) -> Bool in
            return (
                // returns true if search text matches either the number or the description.
                code.description.lowercased().contains(searchText.lowercased()) ||
                    String(describing: code.number).lowercased().contains(searchText.lowercased())
            )
        })
        
        tableView.reloadData()
    }
    
    func isFiltering() -> Bool {
        // potential additional criteria for isFiltering: searchBar.isFocused
        return !searchBarIsEmpty()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        UIView.animate(withDuration: 0.25, animations: {() -> Void in
            self.tableViewBottomConstraint.constant = 0
        })

    }
    
    
    //MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ToExchangeMeterViewController" {
            let destinationVC = (segue.destination as? UINavigationController)?.topViewController as? ExchangeMeterViewController
            destinationVC?.currentMeterRead = self.currentMeter
        }
    }
    
    @IBAction func unwindToHome(segue: UIStoryboardSegue){
        if segue.source is NotesViewController {
            if let senderVC = segue.source as? NotesViewController {
                
                if let note = senderVC.note{
                    //print("Submitted note: \(note)")
                    //currentMeter.mmrActivityText = note
                    returnNote = note
                }
                else{
                    //print("returned without note")
                }
                
                // Integrations would go here!
                wantsToSegueOut = true
            }
        }
        if segue.source is ExchangeMeterViewController {
            let sourceVC = segue.source as! ExchangeMeterViewController
            newMeterNumber = sourceVC.returnMeterNumber
            newMeterNumberOfDials = Int(sourceVC.returnNumberOfDials ?? "0")
            newMeterSizeCode = sourceVC.returnMeterSizeCode
//            performSegue(withIdentifier: "ExitToServiceRequestScreen", sender: nil)
            wantsToSegueOut = true
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // Show error alert when user misses required meter read entry for a completion code
    func showAlert(code: Int? = nil) {
        
        AudioServicesPlaySystemSound(1315);
        
        var alert = UIAlertController()
        
        // For battery exchanges, pass in completion code number because there are unique messages for completion codes 10 and 27
        if code != nil{
            
            if code == 10 {
                alert = UIAlertController(title: "Error", message: "This completion code requires both meter readings. Please return to the previous screen to correct your entry.", preferredStyle: .alert)
                
            } else if code == 27 {
                alert = UIAlertController(title: "Error", message: "This completion code only requires a post-exchange meter reading. Please return to the previous screen to correct your entry.", preferredStyle: .alert)
            }
            
        } else {
            
            // For non battery exchanges, do not pass in completion code number
            alert = UIAlertController(title: "Error", message: "This completion code requires a meter reading. Please return to the previous screen to complete a meter reading.", preferredStyle: .alert)
        }
        
        alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: {(alert: UIAlertAction!) in self.dismissAlert()}))
        self.present(alert, animated: true, completion: nil)
    
    }
    
    
    // Empty function to dismiss alert
    func dismissAlert() {
        
    }
    
    func getDummyCodes(_ type: String) -> [CompletionCode] {
        if type == "ZV" {
            return [
                CompletionCode(number: 10, description: "Completed with read", hasNoteIcon: true, type: "ZV"),
                CompletionCode(number: 20, description: "Not Completed", hasNoteIcon: true, type: "ZV"),
                CompletionCode(number: 25, description: "Meter exchanged", hasNoteIcon: false, type: "ZV")
            ]
            
        } else if type == "ZB" {
            return [
                CompletionCode(number: 10, description: "Completed with read", hasNoteIcon: true, type: "ZB"),
                CompletionCode(number: 25, description: "Meter exchanged", hasNoteIcon: false, type: "ZB"),
                CompletionCode(number: 27, description: "Completed with read", hasNoteIcon: false, type: "ZB"),
                CompletionCode(number: 50, description: "Cancelled by customer service", hasNoteIcon: false, type: "ZB"),
                CompletionCode(number: 70, description: "Unable to complete exchange", hasNoteIcon: true, type: "ZB")
            ]
        } else if type == "ZT" {
            return [
                CompletionCode(number: 10, description: "Completed with read", hasNoteIcon: true, type: "ZT"),
                CompletionCode(number: 20, description: "Not Completed", hasNoteIcon: true, type: "ZT"),
                CompletionCode(number: 25, description: "Meter exchanged", hasNoteIcon: false, type: "ZT")
            ]
        } else if type == "ZR" {
            return [
                CompletionCode(number: 10, description: "Completed with read", hasNoteIcon: true, type: "ZR"),
                CompletionCode(number: 20, description: "Not Completed", hasNoteIcon: true, type: "ZR"),
                CompletionCode(number: 25, description: "Meter exchanged", hasNoteIcon: false, type: "ZR")
            ]
        } else if type == "YD" {
            return [
                CompletionCode(number: 10, description: "Completed with read", hasNoteIcon: true, type: "YD"),
                CompletionCode(number: 20, description: "Not Completed", hasNoteIcon: true, type: "YD")
            ]
        } else if type == "YC" {
            return [
                CompletionCode(number: 10, description: "Completed with read", hasNoteIcon: true, type: "YC"),
                CompletionCode(number: 20, description: "Not Completed", hasNoteIcon: true, type: "YC"),
                CompletionCode(number: 25, description: "Meter exchanged", hasNoteIcon: false, type: "YC"),
                CompletionCode(number: 70, description: "Unable to complete exchange", hasNoteIcon: true, type: "YC")
            ]
        } else {
            return [
                CompletionCode(number: 10, description: "Completed with read", hasNoteIcon: true, type: "YE"),
                CompletionCode(number: 20, description: "Not Completed", hasNoteIcon: true, type: "YE"),
                CompletionCode(number: 25, description: "Meter exchanged", hasNoteIcon: false, type: "YE"),
                CompletionCode(number: 70, description: "Unable to complete exchange", hasNoteIcon: true, type: "YE")
            ]
        }
    }
    
}


