//
//  PeriodicMeterReadTableViewCell.swift
//  Enbridge Meter Reader
//
//  Created by Wentzel, Johann (CA - Alberta) on 2018-01-25.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import UIKit

class PeriodicMeterReadTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLine1Label: UILabel!
    @IBOutlet weak var addressLine2Label: UILabel!
    @IBOutlet weak var meterNumberLabel: UILabel!
    @IBOutlet weak var locationCodeLabel: UILabel!
    @IBOutlet weak var estimationLabel: UILabel!
    @IBOutlet weak var mruLabel: UILabel!
    @IBOutlet weak var troubleCodeIconStackView: UIStackView!
    @IBOutlet weak var keyIconStackView: UIStackView!
    @IBOutlet weak var instructionIconStackView: UIStackView!
    @IBOutlet weak var troubleCodeString1: UILabel!
    @IBOutlet weak var troubleCodeString2: UILabel!
    
    
    var defaultColor = UIColor.white
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        

        keyIconStackView.backgroundColor = .black
        instructionIconStackView.backgroundColor = .black
        backgroundColor = selected ? GlobalConstants.SELECTED_CELL : defaultColor

        // Configure the view for the selected state
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        keyIconStackView.backgroundColor = .black
        instructionIconStackView.backgroundColor = .black
        backgroundColor = highlighted ? GlobalConstants.SELECTED_CELL : defaultColor
    }
    
    
    

}
