//
//  ChangeMeterLocationTableViewCell.swift
//  Enbridge Meter Reader
//
//  Created by Wentzel, Johann (CA - Alberta) on 2017-12-05.
//  Copyright © 2017 Enbridge. All rights reserved.
//

import UIKit

class ChangeMeterLocationTableViewCell: UITableViewCell {
    @IBOutlet weak var locationNumberLabel: UILabel!
    @IBOutlet weak var locationDescriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
