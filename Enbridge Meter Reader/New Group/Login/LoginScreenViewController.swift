//
//  LoginScreenViewController.swift
//  Enbridge Meter Reader
//
//  Created by Lauren Abramsky on 2017-11-29.
//  Copyright © 2017 Enbridge. All rights reserved.
//

import UIKit

class LoginScreenViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var logInButton: UIButton!
    @IBOutlet weak var userIDField: ValidatedTextField!
    @IBOutlet weak var passwordField: ValidatedTextField!
    
    var userIDValid: Bool!
    var passwordValid: Bool!
    var logInButtonEnabled: Bool!
    var credentialsValid: Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // Add radius
        logInButton.layer.cornerRadius = 10
        
        passwordField.keyboardDistanceFromTextField = 150
        
        userIDValid = false
        passwordValid = false
        logInButtonEnabled = false
        credentialsValid = true
        
        logInButton.setTitleColor(UIColor(red:0.2, green:0.2, blue:0.2, alpha:1), for: .normal)
        logInButton.setTitleColor(UIColor.gray, for: .disabled)
        
        userIDField.addTarget(self, action: #selector(checkAllValid), for: .editingChanged)
        
        passwordField.addTarget(self, action: #selector(checkAllValid), for: .editingChanged)
        
        checkAllValid()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }    
    
    @objc func checkAllValid(){
        // Check if all the fields are valid. If so, enable the submit button.
        if userIDField.getValidity() && passwordField.getValidity() {
            // enable button
            if !(logInButtonEnabled){
                logInButtonEnabled = true
                logInButton.isEnabled = true
                logInButton.backgroundColor = GlobalConstants.YELLOW
            }
        }
        else {
            logInButtonEnabled = false
            logInButton.isEnabled = false
            logInButton.backgroundColor = GlobalConstants.LIGHT_YELLOW
        }
        
    }
    
    // Re-do this with integrations, for now made boolean function that gives login error if user enters password as "wrong"
    func authenticateCredentials() -> Bool {
        if passwordField.text == "wrong" {
            return false
        } else {
            return true
        }
    }
    
    // Empty function to dismiss login error alert
    func dismissAlert() {
    }
    
    // When user enters invalid login credentials, set field borders to red
    func setFieldBorders(_ color: CGColor, _ width: CGFloat) {
        userIDField.layer.borderColor = color
        userIDField.layer.borderWidth = width
        userIDField.layer.cornerRadius = 6
        passwordField.layer.borderColor = color
        passwordField.layer.borderWidth = width
        passwordField.layer.cornerRadius = 6
        
    }
    
    // MARK: - Actions
    
    @IBAction func loginButtonPressed(_ sender: UIButton) {
        
        // TODO: Actually authenticate
        credentialsValid = authenticateCredentials()
        
        if credentialsValid {
            
            // Ensure red borders are removed
            setFieldBorders(UIColor.white.cgColor, 0.0)
            
            // Take user to select route screen
            let appdelegate = UIApplication.shared.delegate as! AppDelegate
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "MainNav", bundle: nil)
            let vc = mainStoryboard.instantiateInitialViewController()
            appdelegate.window!.rootViewController = vc
            
        } else {
            
            // Alert telling user their login credentials are wrong
            let alert = UIAlertController(title: "Login Error", message: "\nIncorrect email and/or password.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: {(alert: UIAlertAction!) in self.dismissAlert()}))
            self.present(alert, animated: true, completion: nil)
            
            // Set border of username and password fields to red
            setFieldBorders(UIColor.red.cgColor, 2.0)
            
        }
        
        // TODO: Segue to "configure route" screen
        
    }
    
    
}
