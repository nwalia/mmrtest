//
//  OffCycleCell + MKMapViewDelegate.swift
//  Enbridge Meter Reader
//
//  Created by Deshpande, Ankush (CA - Toronto) on 2018-02-06.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import Foundation
import MapKit

extension OffCycleCell: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        return displayAnnotation(mapView: mapView, viewFor: annotation)
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        manager.reload(mapView, visibleMapRect: mapView.visibleMapRect)
    }
}
