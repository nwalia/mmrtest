//
//  OffCycleCell.swift
//  Enbridge Meter Reader
//
//  Created by Deshpande, Ankush (CA - Toronto) on 2018-01-17.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import UIKit
import MapKit

class OffCycleCell: UITableViewCell {
    
    @IBOutlet weak var location: UILabel?
    @IBOutlet weak var offCycleMapView: MKMapView!
    @IBOutlet weak var beginRoute: UIButton?
    @IBOutlet weak var returnToRoute: UIButton?
    @IBOutlet weak var progressBar: ProgressBarView?
    @IBOutlet weak var missesLabel: UILabel?
    @IBOutlet weak var blurView: UIView?
    @IBOutlet weak var uploadText: UILabel?
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView?
    
    static var hasBeenStarted = false
    
    let manager = ClusterManager()
    var uploading = false
    var blockCell = false
    var uploadingText = ""
    var routeKey = ""
    var associatedRoute: Route? {
        didSet {
            MapViewManager.setUpMapView(manager: manager, mapView: offCycleMapView, annotations: (associatedRoute?.mapAnnotations)!)
            progressBar?.populate(type: .offcycle)
            
            if associatedRoute?.status == .available {
                missesLabel?.text = ""
            }
            else {
                missesLabel?.text = "\(associatedRoute?.totalMissed ?? 0) Misses"
            }
            
            location?.text = "\(associatedRoute?.region ?? "")"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        offCycleMapView.delegate = self
//        MapView.sharedInstance.disableUserInteration(mapView: offCycleMapView)
   
    }
    
    override func setNeedsLayout() {
        if OffCycleCell.hasBeenStarted{
            beginRoute?.isHidden = true
            returnToRoute?.isHidden = false
        }else{
            beginRoute?.isHidden = false
            returnToRoute?.isHidden = true
        }
        if uploading {
            activityIndicator?.startAnimating()
        }else{
            activityIndicator?.stopAnimating()
        }
        
        blurView?.isHidden = !blockCell
        uploadText?.text = uploadingText
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func didSelectBeginRoute(_ sender: UIButton) {
        guard associatedRoute != nil else { return }
        ConfigureRouteViewController.caller = "offCycle"
        Model.selectedRouteKey = routeKey

    }
}
