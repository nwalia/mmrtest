//
//  OutCard + Helper.swift
//  Enbridge Meter Reader
//
//  Created by Deshpande, Ankush (CA - Toronto) on 2018-02-06.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import Foundation
import MapKit

extension OutCardCell {
    func displayAnnotation(mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if let annotation = annotation as? ClusterAnnotation {
            guard let style = annotation.style else { return nil }
            let identifier = "Cluster"
            var view = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
            if let view = view as? BorderedClusterAnnotationView {
                view.annotation = annotation
                view.configure(with: style)
            } else {
                view = BorderedClusterAnnotationView(annotation: annotation, reuseIdentifier: identifier, style: style, brColor: .white)
            }
            return view
        }
        else {
            guard let annotation = annotation as? Annotation else { return nil }
            let identifier = "Pin"
            
            var annotationView: MKAnnotationView?
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            
            if let annotationView = annotationView {
                annotationView.image = UIImage(named: "Pin")
            }
            
            return annotationView
            
            // MARK :-
            
            /**
             use this if you need a pin droped instade of a custom image
             */
            
            //            var view = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKPinAnnotationView
            //            if let view = view {
            //                view.annotation = annotation
            //            } else {
            //                view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            //            }
            //            if #available(iOS 9.0, *), case let .color(color, _) = style {
            //                view?.pinTintColor =  color
            //            } else {
            //                view?.pinTintColor = .green
            //            }
            //            return view
        }
    }
}
