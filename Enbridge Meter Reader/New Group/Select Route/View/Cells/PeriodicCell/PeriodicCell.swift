//
//  PeriodicCell.swift
//  Enbridge Meter Reader
//
//  Created by Deshpande, Ankush (CA - Toronto) on 2018-01-17.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import UIKit
import MapKit

class PeriodicCell: UITableViewCell {
    
    @IBOutlet weak var Location: UILabel?
    @IBOutlet weak var mruNumber: UILabel?
    @IBOutlet weak var viewRouteSubmission: UIButton?
    @IBOutlet weak var returnToSummary: UIButton?
    @IBOutlet weak var summary: UIButton?
    @IBOutlet weak var beginRoute: UIButton?
    
    @IBOutlet weak var missesLabel: UILabel?
    @IBOutlet weak var skipsLabel: UILabel?
    @IBOutlet weak var progressBar: ProgressBarView?
    @IBOutlet weak var periodicMapSummary: MKMapView!
    @IBOutlet weak var blurView: UIView?
    @IBOutlet weak var uploadText: UILabel?
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView?
    
    let manager = ClusterManager()
    static var showViewRouteSubmissionButtonFlag = false
    var submitted: Bool = false
    
    var hasBeenStarted = false
    var routeKey = ""
    var uploading = false
    var blockCell = false
    var uploadingText = "Loading the route, please wait"
    var associatedRoute: Route? {
        didSet {
            
            MapViewManager.setUpMapView(manager: manager, mapView: periodicMapSummary, annotations: (associatedRoute?.mapAnnotations)!)
            progressBar?.populate(routeKey: routeKey, type: .periodic)
            
            Location?.text = "\(associatedRoute?.region ?? "")"
            
            if associatedRoute?.status == .available {
                missesLabel?.text = ""
                skipsLabel?.text = ""
            }
            else {
                missesLabel?.text = "\(associatedRoute?.totalMissed ?? 0) Misses"
                skipsLabel?.text = "\(associatedRoute?.totalSkips ?? 0) Skips"
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        MapView.sharedInstance.disableUserInteration(mapView:periodicMapSummary)
        periodicMapSummary?.delegate = self
    
    }
    
    override func setNeedsLayout() {
        if hasBeenStarted{
            beginRoute?.isHidden = true
        }else{
            beginRoute?.isHidden = false
        }
        if submitted {
            showViewRouteSubmissionButton()
        }
        if uploading {
            activityIndicator?.startAnimating()
        }else{
            activityIndicator?.stopAnimating()
        }
        blurView?.isHidden = !blockCell
        uploadText?.text = uploadingText
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func didPressSummary(_ sender: UIButton) {
        Model.selectedRouteKey = routeKey
    }
    
    @IBAction func didPressReturnToRoute(_ sender: UIButton) {
        Model.selectedRouteKey = routeKey
    }
    
    @IBAction func didSelectBeginRoute(_ sender: UIButton) {
        guard associatedRoute != nil else { return }
        ConfigureRouteViewController.caller = "periodic"
        Model.selectedRouteKey = routeKey
        
    }
    
    @IBAction func didSelectViewRouteSubmitted(_ sender: UIButton) {
        Model.selectedRouteKey = routeKey
        RouteSummaryViewController.submissionCompleted = true
        RouteSummaryViewController.caller = "PeriodicCell"
    }
}

extension PeriodicCell {
    
    func showViewRouteSubmissionButton() {
        self.returnToSummary?.isHidden = true
        self.summary?.isHidden = true
        self.beginRoute?.isHidden = true
        self.viewRouteSubmission?.isHidden = true
//        self.viewRouteSubmission.setTitleColor(UIColor.gray, for: .normal)
        self.viewRouteSubmission?.backgroundColor = UIColor(red: 254/255, green: 196/255, blue: 78/255, alpha: 1.0)
    }
    
    override func layoutSubviews() {
        if PeriodicCell.showViewRouteSubmissionButtonFlag{
            showViewRouteSubmissionButton()
        }
    }
}


