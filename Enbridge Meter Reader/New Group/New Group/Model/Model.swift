//
//  Model.swift
//  Enbridge Meter Reader
//
//  Created by Deshpande, Ankush (CA - Toronto) on 2018-01-07.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import Foundation
import UIKit

protocol ModelDelegate {
    func didRefreshModel()
}

/// The data model used to populate the table view on app launch.
struct Model {
    
    static var sharedInstance = Model()
    static var selectedRouteKey = ""
    
    var progressBarRefreshDelegate: ModelDelegate?
    var selectRouteRefreshDelegate: ModelDelegate?
    
    var routes = [String:Route]()
    var routeStatus = [String:Route]()
    
    
    static var testMeterReadListings = [
        MeterRead(firstName: "Johann", lastName: "Wentzel", locationCode: LocationCode("QR"), meterNumber: "123451", typeCode: "ZT", sizeCode: "SD", addressLine1: "123 Happy St", addressLine2: "Calgary, AB T2R 1L6", jobID: "123456", gridNumber: "C05", phoneNumber: "123-123-1230", keyNumber: "12345", appointmentTime: "9AM-1PM", numberOfDials: 6),
        MeterRead(firstName: "Alex", lastName: "Marian", locationCode: LocationCode("AF"), meterNumber: "123452",  typeCode: "ZT", sizeCode: "SD", addressLine1: "1237 Test St", addressLine2: "Toronto, ON A1B 2C3", jobID: "123456", gridNumber: "C05", phoneNumber: "123-123-1231", keyNumber: "72356", numberOfDials: 6),
        MeterRead(firstName: "Ennio", lastName: "Crescenzi", locationCode: LocationCode("AB"), meterNumber: "123453", typeCode: "YD", sizeCode: "SD", addressLine1: "1236 Test St", addressLine2: "Toronto, ON A1B 2C3", jobID: "123456", gridNumber: "C05", phoneNumber: "123-123-1232", numberOfDials: 6),
        MeterRead(firstName: "Lauren", lastName: "Abramsky", locationCode: LocationCode("SA"), meterNumber: "123454", typeCode: "YD", sizeCode: "SD", addressLine1: "1231 Test St", addressLine2: "Toronto, ON A1B 2C3", jobID: "123456", gridNumber: "C05", phoneNumber: "123-123-1233", numberOfDials: 6),
        MeterRead(firstName: "Angie", lastName: "Kwan", locationCode: LocationCode("AF"), meterNumber: "123455", typeCode: "YD", sizeCode: "SD", addressLine1: "1232 Test St", addressLine2: "Toronto, ON A1B 2C3", jobID: "123456", gridNumber: "C05", phoneNumber: "123-123-1234", appointmentTime: "12PM-4PM", numberOfDials: 6),
        MeterRead(firstName: "Ankush", lastName: "Deshpande", locationCode: LocationCode("AB"), meterNumber: "123456", typeCode: "ZB", sizeCode: "SD", addressLine1: "1233 Test St", addressLine2: "Toronto, ON A1B 2C3", jobID: "123456", gridNumber: "C05", phoneNumber: "123-123-1235", keyNumber: "54321", numberOfDials: 6),
        MeterRead(firstName: "John", lastName: "Appleseed", locationCode: LocationCode("AB"), meterNumber: "123457", typeCode: "ZR", sizeCode: "SD", addressLine1: "1231 Test St", addressLine2: "Toronto, ON A1B 2C3", jobID: "123456", gridNumber: "C05", phoneNumber: "123-123-1234", numberOfDials: 6),
        MeterRead(firstName: "Hannah", lastName: "Hart", locationCode: LocationCode("AB"), meterNumber: "123458", typeCode: "YD", sizeCode: "SD", addressLine1: "1231 Test St", addressLine2: "Toronto, ON A1B 2C3", jobID: "123456", gridNumber: "C05", phoneNumber: "123-123-1233", keyNumber: "63528", appointmentTime: "5PM-9PM", numberOfDials: 6),
        MeterRead(firstName: "Dean", lastName: "Basset", locationCode: LocationCode("AB"), meterNumber: "123459", typeCode: "ZV", sizeCode: "SD", addressLine1: "1231 Test St", addressLine2: "Toronto, ON A1B 2C3", jobID: "123456", gridNumber: "C05", phoneNumber: "123-123-1214", appointmentTime: "9AM-1PM", numberOfDials: 6),
        MeterRead(firstName: "Erwin", lastName: "Bird", locationCode: LocationCode("AB"), meterNumber: "123450", typeCode: "ZV", sizeCode: "SD", addressLine1: "12131 Test St", addressLine2: "Toronto, ON A1B 2C3", jobID: "123456", gridNumber: "C05", phoneNumber: "123-123-5234", keyNumber: "93475", appointmentTime: "12PM-4PM", numberOfDials: 6),
    ]
    
    private(set) var testPeriodicMeterReadListings = [
        MeterRead(firstName: "Kanye", lastName: "West", locationCode: LocationCode("AB"), meterNumber: "123222", sizeCode: "SD", addressLine1: "123 Happy St", addressLine2: "Toronto, ON M5M 1L6", jobID: "123456", gridNumber: "C05", phoneNumber: "416-123-1230", keyNumber: "12345", skips: 2, numberOfDials: 6, lowParameter: 111111, highParameter: 333333)
    ]
    
    /// The traditional method for rearranging rows in a table view.
    mutating func moveItem(at sourceIndex: Int, to destinationIndex: Int) {
        guard sourceIndex != destinationIndex && destinationIndex < (routes[Model.selectedRouteKey]?.meterReads.count)! else { return }
        
//        let place = testMeterReadListings[sourceIndex]
//        testMeterReadListings.remove(at: sourceIndex)
//        testMeterReadListings.insert(place, at: destinationIndex)
        
        let place = routes[Model.selectedRouteKey]?.meterReads[sourceIndex]
        routes[Model.selectedRouteKey]?.meterReads.remove(at: sourceIndex)
        routes[Model.selectedRouteKey]?.meterReads.insert(place!, at: destinationIndex)
    }
    
    /// The method for adding a new item to the table view's data model.
    mutating func addItem(_ place: MeterRead, at index: Int) {
//        testMeterReadListings.insert(place, at: index)
        routes[Model.selectedRouteKey]?.meterReads.insert(place, at: index)
    }
    
    mutating func reverseDataModelForSelected(segment: Int) {
        switch segment {
        case 0:
//            self.testMeterReadListings = self.testMeterReadListings.reversed()
            routes[Model.selectedRouteKey]?.meterReads = (routes[Model.selectedRouteKey]?.meterReads.reversed())!
        case 1:
            routes[Model.selectedRouteKey]?.meterReads = (routes[Model.selectedRouteKey]?.meterReads.reversed())!
        default:
            return
        }
    }
    
    // Submits all routes that have been queued offline.
    func submitQueuedRoutes(){
        var shouldShowToolbar = false
        
        for key in routes.keys {
            if routes[key]?.status == .pending {
                routes[key]?.status = .completed
                shouldShowToolbar = true
                if key != GlobalConstants.OFFCYCLE_ROUTE_ID {
                    RouteSummaryViewController.routeSummeryShareInstance?.removeSubmittedRoute(routeKey: key)
                }
            }
            else if key == GlobalConstants.OFFCYCLE_ROUTE_ID {
                for read in (routes[key]?.meterReads)! {
                    if read.status == .pending {
                        shouldShowToolbar = true
                        read.status = .completed
                        read.updateReadDate()
                    }
                }
            }
            checkRouteCompleted(routeId: key)
        }
        
        selectRouteRefreshDelegate?.didRefreshModel()
        progressBarRefreshDelegate?.didRefreshModel()
        
        if shouldShowToolbar {
            let delegate = UIApplication.shared.delegate as! AppDelegate
            delegate.showNetworkToolbar(isOnline: true)
        }
        
        Persistence.shared.saveRoutes()
    }
    
    func checkRouteCompleted(routeId: String){
        if routes[routeId]?.totalMissed == 0 {
            
            if routeId == GlobalConstants.OFFCYCLE_ROUTE_ID {
                routes[routeId]?.status = .completed
            }
            
            routes[routeId]?.setCompletedTime()
        }
    }
    
    
    func returnRouteStatus(type:String) -> String?{
        if let aRoutes = routes[type] {
            return aRoutes.status.rawValue
        }
        return nil
    }
    
    static func clear(){
        Model.sharedInstance = Model()
        Model.selectedRouteKey = ""
    }
}
