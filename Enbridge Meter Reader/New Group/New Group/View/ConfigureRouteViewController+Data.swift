//
//  ConfigureRouteViewController+Data.swift
//  Enbridge Meter Reader
//
//  Created by Deshpande, Ankush (CA - Toronto) on 2018-01-07.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import Foundation
import UIKit

// Logic that connects `TableViewController`'s data model with its user interface.
extension ConfigureRouteViewController: UITableViewDataSource, UITableViewDelegate {
    // MARK: - UITableViewDataSource
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (Model.sharedInstance.routes[Model.selectedRouteKey]?.meterReads.count)!
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if ConfigureRouteViewController.caller == "offCycle" {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "offCycleCell") as! ConfigureRoute_offCycleTableViewCell
            guard let readForCell = Model.sharedInstance.routes[Model.selectedRouteKey]?.meterReads[indexPath.row] else { return UITableViewCell() }
            
            if readForCell.newFullMeterId != "" {
                     cell.populate(name: readForCell.nameLastFirstComma, addressLine1:readForCell.fullAddress, addressLine2: readForCell.addressLine2, meterNumber: readForCell.newFullMeterId, locationCode: readForCell.locationCode.id, jobID: readForCell.jobID, gridNumber: readForCell.gridNumber, readType: readForCell.typeCode ?? "", mmrNotificationCode: readForCell.mmrNotificationCode ?? "", mmrRedLockType: readForCell.mmrRedLockType ?? "", cancelled: readForCell.cancelled, unscheduled: readForCell.unscheduled, mruNumber: "MS/NO: \(readForCell.sizeCode)\(readForCell.meterNumber)")
            } else {
                cell.populate(name: readForCell.nameLastFirstComma, addressLine1:readForCell.fullAddress, addressLine2: readForCell.addressLine2, meterNumber: readForCell.newFullMeterId, locationCode: readForCell.locationCode.id, jobID: readForCell.jobID, gridNumber: readForCell.gridNumber, readType: readForCell.typeCode ?? "", mmrNotificationCode: readForCell.mmrNotificationCode ?? "", mmrRedLockType: readForCell.mmrRedLockType ?? "", cancelled: readForCell.cancelled, unscheduled: readForCell.unscheduled, mruNumber: "MS/NO: \(readForCell.sizeCode)\(readForCell.meterNumber)")
            }
            
            
            cell.keyIconStackView.isHidden = readForCell.keyNumber == nil || (readForCell.keyNumber ?? "").isEmpty
            cell.instructionIconStackView.isHidden = readForCell.specialInstructions == nil || readForCell.specialInstructions == ""
            
            switch readForCell.appointmentTime {
            case "9AM-12PM"?:
                cell.timeIconButton.backgroundColor = UIColor.enbridgeYellow
                cell.timeIconLabel.text = readForCell.appointmentTime
                cell.timeIconStackView.isHidden = false
            case "12PM-4PM"?:
                cell.timeIconButton.backgroundColor = UIColor.clementine
                cell.timeIconLabel.text = readForCell.appointmentTime
                cell.timeIconStackView.isHidden = false
            case "4PM-9PM"?:
                cell.timeIconButton.backgroundColor = UIColor.dusk
                cell.timeIconLabel.text = readForCell.appointmentTime
                cell.timeIconStackView.isHidden = false
            default:
                cell.timeIconStackView.isHidden = true
            }
            
            
            return cell
        }
        else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "periodicCell", for: indexPath) as! ConfigureRoute_PeriodicTableViewCell
            guard let readForCell = Model.sharedInstance.routes[Model.selectedRouteKey]?.meterReads[indexPath.row] else { return UITableViewCell() }
            cell.customerName?.text = readForCell.nameLastFirstComma
            cell.addressLine1.text = readForCell.fullAddress
            cell.addressLine2.text = readForCell.addressLine2
            cell.locationCodeLabel.text = "LOC: \(readForCell.locationCode.id)"
            if readForCell.newFullMeterId != "" {
                cell.meterNumberLabel.text = readForCell.newFullMeterId
                cell.mruLabel.text = "MRU: \(readForCell.routeId)"
                if let est_Count = readForCell.skips {
                    cell.estimationLabel.text = "Est: \(est_Count)"
                }
            } else {
                cell.meterNumberLabel.text = readForCell.fullMeterId
                cell.mruLabel.text = "MRU: \(readForCell.routeId)"
                if let est_Count = readForCell.skips {
                    cell.estimationLabel.text = "Est: \(est_Count)"
                }
            }
            cell.keyStackView.isHidden = readForCell.keyNumber == nil || (readForCell.keyNumber ?? "").isEmpty
            cell.instructionStackView.isHidden = readForCell.specialInstructions == nil || readForCell.specialInstructions == ""
            return cell
        }
    }
    
    // MARK: - UITableViewDelegate
    
     func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
     func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        Model.sharedInstance.moveItem(at: sourceIndexPath.row, to: destinationIndexPath.row)
    }
}

