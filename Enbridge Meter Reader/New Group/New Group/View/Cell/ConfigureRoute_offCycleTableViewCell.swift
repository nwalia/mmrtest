//
//  ConfigureRoute_offCycleTableViewCell.swift
//  Enbridge Meter Reader
//
//  Created by Deshpande, Ankush (CA - Toronto) on 2018-02-14.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import UIKit

class ConfigureRoute_offCycleTableViewCell: UITableViewCell {

    @IBOutlet weak var typeView: UIView!
    @IBOutlet weak var meterReadType: UILabel!
    @IBOutlet weak var jobID: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var addressLine1: UILabel!
    @IBOutlet weak var addressLine2: UILabel!
    @IBOutlet weak var mruLabel: UILabel!
    @IBOutlet weak var meterNumber: UILabel!
    @IBOutlet weak var locationCode: UILabel!
    @IBOutlet weak var timeIconStackView: UIStackView!
    @IBOutlet weak var keyIconStackView: UIStackView!
    @IBOutlet weak var instructionIconStackView: UIStackView!
    @IBOutlet weak var timeIconButton: UIButton!
    @IBOutlet weak var timeIconLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        
        // name: String, addressLine1: String, addressLine2: String, meterNumber: String, locationCode: String, jobID: String, gridNumber:String, readType: String
    }
    
    func populate(name: String, addressLine1: String, addressLine2: String, meterNumber: String, locationCode: String, jobID: String, gridNumber:String, readType: String, mmrNotificationCode:String, mmrRedLockType:String, cancelled: Bool, unscheduled: Bool, mruNumber: String){
        
        self.name.text = name
        self.addressLine1.text = addressLine1
        self.addressLine2.text = addressLine2
        self.mruLabel.text = mruNumber
        self.meterNumber.text = meterNumber
        self.locationCode.text = locationCode
        
        let style = ServiceRequestColorStyle(code: readType)
        self.jobID.text = "#\(jobID ) / \(gridNumber )"
        self.jobID.textColor = style.textColor
        
        self.meterReadType.text = "\(readType ) - \(mmrNotificationCode )\((mmrRedLockType.isEmpty) ? "" : " - \(String(describing: mmrRedLockType ))" )"
        self.meterReadType.textColor = style.textColor
        
        self.typeView.backgroundColor = style.backgroundColor
        self.typeView.borderColor = style.borderColor
        self.typeView.borderWidth = CGFloat(style.borderWidth)
        
        // When work order is cancelled, square should read "WORK ORDER CANCELLED"
        if cancelled {
            self.meterReadType.text = "WORK ORDER"
            self.jobID.text = "CANCELLED"
            self.backgroundColor = GlobalConstants.COMPLETED_CELL
        }
        
        // When work order is cancelled, square should read "WORK ORDER CANCELLED"
        if unscheduled {
            self.meterReadType.text = "WORK ORDER"
            self.jobID.text = "UNSCHEDULED"
            self.backgroundColor = GlobalConstants.COMPLETED_CELL
        }
                
    }

}
