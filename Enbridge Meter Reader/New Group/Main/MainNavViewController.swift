//
//  MainNavViewController.swift
//  Enbridge Meter Reader
//
//  Created by Wentzel, Johann (CA - Alberta) on 2018-01-15.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import UIKit


class MainNavViewController: UITabBarController {

    @IBOutlet weak var tabBarOutlet: UITabBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setMessagesBadge(badgeCount: GlobalVariables.badgeCount)
    }
    
    // Add badge on messages tab if count is greater than 0, otherwise set nil
    func setMessagesBadge(badgeCount: Int) {
        if badgeCount != 0 {
            tabBar.items?[1].badgeValue = String(badgeCount)
            tabBar.items?[1].badgeColor = .red
        } else {
            tabBar.items?[1].badgeValue = nil
        }

    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
