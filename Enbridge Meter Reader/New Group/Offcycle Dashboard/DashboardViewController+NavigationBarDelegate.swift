//
//  DashboardViewController+NavigationBarDelegate.swift
//  Enbridge Meter Reader
//
//  Created by Deshpande, Ankush (CA - Toronto) on 2018-02-09.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import Foundation
import UIKit

extension DashboardViewController : UINavigationControllerDelegate {
    func navigationController(_: UINavigationController, willShow: UIViewController, animated: Bool){
        if willShow == navigationController?.viewControllers[1] {
            navigationController?.popToRootViewController(animated: true)
        }
    }
}
