//
//  DataStructures.swift
//  MapView
//
//  Created by Deshpande, Ankush (CA - Toronto) on 2017-12-20.
//  Copyright © 2017 Deshpande, Ankush (CA - Toronto). All rights reserved.
//

import Foundation
import MapKit

enum Location {
    case source(latitude:CLLocationDegrees,longitude:CLLocationDegrees)
    case destination(latitude:CLLocationDegrees,longitude:CLLocationDegrees)
    
    var setCoordinates: CLLocationCoordinate2D {
        switch self {
        case .source(let lat, let long):
            return CLLocationCoordinate2D(latitude: lat, longitude: long)
        case .destination(let lat, let long):
            return CLLocationCoordinate2D(latitude: lat, longitude: long)
        }
    }
}

enum Marker {
    case source(location: CLLocationCoordinate2D)
    case destination (Location: CLLocationCoordinate2D)
    
    var placeMarker: MKPlacemark {
        switch self {
        case .source(let sourceLocation):
            return MKPlacemark(coordinate: sourceLocation, addressDictionary: nil)
        case .destination(let destinationLocation):
            return MKPlacemark(coordinate: destinationLocation, addressDictionary: nil)
        }
    }
}

enum MapItem {
    case source(marker: MKPlacemark)
    case destination (marker: MKPlacemark)
    
    var placeMapItem: MKMapItem {
        switch self {
        case .source(let sourcePlacemark):
            return MKMapItem(placemark: sourcePlacemark)
        case .destination(let destinationPlacemark):
            return MKMapItem(placemark: destinationPlacemark)
        }
    }
    
}

// MARK: - Structs
struct CompletionCode: Codable {
    var number: Int?
    var description: String
    var hasNoteIcon: Bool
    var type: String
}
