//
//  DashboardViewController.swift
//  Enbridge Meter Reader
//
//  Created by Johann Wentzel on 2017-11-29.
//  Copyright © 2017 Enbridge. All rights reserved.
//

import Foundation
import UIKit
import MapKit

protocol DashboardDelegate: class {
    func populateCustomerInformation(onDashboard: Bool, hideLine: Bool, newRead: MeterRead, buttonsEnabled: Bool)
    func didRefreshDataModel()
}

protocol OffcycleDashboardViewDidAppearDelegate: class {
    func selectFirstIncompleteOnViewDidAppear()
}

class DashboardViewController: UIViewController, CLLocationManagerDelegate {
   
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var infoTableView: UITableView!
    @IBOutlet weak var progressBar: ProgressBarView!
//    @IBOutlet weak var timeElapsedBar: TimeElapsedView!
    
    static weak var customerInformationDelegate: DashboardDelegate?
    static weak var routeListingTableViewDelegate: DashboardDelegate?
    static weak var tableSelectFirstDelegate: OffcycleDashboardViewDidAppearDelegate? {
        didSet(newValue){
        DashboardViewController.tableSelectFirstDelegate?.selectFirstIncompleteOnViewDidAppear()
        }
    }
    
    static var shouldShowNetworkToolbar = false
    static var currentMeterLocation: LocationCode?
    
    let regionRadius: CLLocationDistance = 100
    let locationManager = CLLocationManager()
    let mapManager = MapViewManager()
    
    var selectedMeter: MeterRead? {
        willSet(newValue){
            DashboardViewController.currentMeterLocation = newValue?.locationCode
        }
    }
    var filteredMeterReads: [MeterRead]?
    
    // Set to false after first appearance, so navigating back to the dashboard doesn't reset the
    // Customer Information xib.
    var firstAppearance = true
    
    private var toggle: Bool = false
    internal var editButtonToggle : Bool = false
    var numberOfCell = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Set data model for route listing table view
        DashboardViewController.routeListingTableViewDelegate?.didRefreshDataModel()
        
        // Let LocationCodeViewController know that the previous screen was this one
        ChangeMeterLocationViewController.previous = .offcycle
        
        // Set UI attributes
        navigationController?.navigationBar.tintColor = UIColor(red:1, green:0.72, blue:0.11, alpha:1)
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        navigationController?.navigationBar.barTintColor = UIColor.darkGray
        
        // map view is hidden by default
        topConstraint.constant = mapView.bounds.height * -1
        toggle = true
        infoTableView.reloadData()
        view.layoutIfNeeded()

        // Set table view attributes
        self.numberOfCell = 3
        self.infoTableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        self.infoTableView.rowHeight = UITableView.automaticDimension
        self.infoTableView.estimatedRowHeight = UITableView.automaticDimension

        // Update user location in map view
        
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        // Set up notification observer so AppDelegate can reload offcycle dashboard table when a service request is cancelled or unscheduled or new work is added
        NotificationCenter.default.addObserver(self, selector: #selector(loadOffcycleDashboardTable), name: NSNotification.Name(rawValue: "newWork"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(loadOffcycleDashboardTable), name: NSNotification.Name(rawValue: "reloadTables"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(alertEmptyRoute), name: NSNotification.Name(rawValue: "emptyRoute"), object: nil)

    }
    
    // Function called by notification in AppDelegate to reload offcycle dashboard table when a service request is cancelled or unscheduled
    @objc func loadOffcycleDashboardTable() {
        progressBar.populate(type: .offcycle, shouldRefreshWithNetworkChange: true)
        self.infoTableView.reloadData()
        DashboardViewController.routeListingTableViewDelegate?.didRefreshDataModel()
    }
    
    @objc func alertEmptyRoute() {
        let alert = UIAlertController(title: "Unscheduled Work", message: "This work order has been unscheduled. Please select a new route.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Return to Select Route", style: .default, handler: {(alert: UIAlertAction!) in self.returnToSelectRoute()}))
        self.present(alert, animated: true, completion: nil)

    }
    
    func returnToSelectRoute() {
        _ = navigationController?.popToRootViewController(animated: true)
    }
    
    @objc func popToRootViewcontroller(sender: UIBarButtonItem) {
        navigationController?.popToRootViewController(animated: true)
    }
    

    
//    override func viewDidDisappear(_ animated: Bool) {
////        navigationController?.popViewController(animated: true)
//    }
//
//    override func viewWillDisappear(_ animated: Bool) {
//        navigationController?.popToViewController(SelectRouteTableViewController(), animated: true)
//        navigationController?.popToRootViewController(animated: true)
//    }
    

    
    func setupMapView() {
        
        mapView.removeOverlays(mapView.overlays)
        mapView.removeAnnotations(mapView.annotations)
        
        //Set the source and destinatin for the route
        
        guard let deviceLatitude = locationManager.location?.coordinate.latitude else { return }
        guard let deviceLongitude = locationManager.location?.coordinate.longitude else { return }
        guard selectedMeter != nil else { return }
        
        let source = Location.source(latitude: deviceLatitude, longitude: deviceLongitude)
        let destination = Location.destination(latitude: selectedMeter!.latitude ?? deviceLatitude, longitude: selectedMeter!.longitude ?? deviceLongitude)
        
        // Generate marker to plot on the mapView
        let mapItem = mapManager.generateMarkerFor(source: source, destination: destination)
        
        let sourceMapItem = mapItem.0
        let destinationMapItem = mapItem.1
        
        
        //get directions from source to destination
        mapManager.addRouteToMap(sourceMapItem: sourceMapItem, destinationMapItem: destinationMapItem, mapView: self.mapView)
        
        
        //set annoataion title and display on the mapView.
        let annotation = mapManager.setAnnotation(sourceTitle: "Current Location", destinationTitle: "Nathan Phillips Square")

        self.mapView.showAnnotations([annotation.sourceAnnotation,annotation.destinationAnnotation], animated: true )
        
        //create custom view for source and destination, navigate to the Apple Maps apps from here.
        //let artworkSource = Artwork(title: "Current Location" , coordinate: CLLocationCoordinate2D(latitude: (locationManager.location?.coordinate.latitude)!, longitude: (locationManager.location?.coordinate.longitude)!))
        let artworkDestination = Artwork(title: (selectedMeter!.nameLastFirstComma != "" ? selectedMeter!.nameLastFirstComma : "Selected Customer"), subtitle: selectedMeter!.fullAddress, coordinate: CLLocationCoordinate2D(latitude: selectedMeter!.latitude ?? deviceLatitude, longitude: selectedMeter!.longitude ?? deviceLongitude))
        
        mapView.addAnnotations([artworkDestination])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        //super.viewWillAppear(animated)
        
        // Set delegates
        SpecialInstruction.delegate = self
        OrderDetailCell.delegate = self
        self.mapView.delegate = self
        CustomerInformation.delegate = self
        
        // Upon the view showing up for the first time, sets the Customer Information cell's data
        // to that in the first item in the route listing table view.
        
        Persistence.shared.saveRoutes()
        
         if firstAppearance {
            selectedMeter = Model.sharedInstance.routes[GlobalConstants.OFFCYCLE_ROUTE_ID]?.meterReads[0]
            DashboardViewController.customerInformationDelegate?.populateCustomerInformation(onDashboard: true, hideLine: true, newRead: selectedMeter!, buttonsEnabled: true)
            firstAppearance = false
         }
        
        // sends a request to the table view to select the first non-complete route listing.
        if selectedMeter == nil || selectedMeter?.status == .completed || selectedMeter?.status == .pending {
            DashboardViewController.tableSelectFirstDelegate?.selectFirstIncompleteOnViewDidAppear()
        }
        
        self.setupMapView()
        
        // Populate both progress and elapsed time bars
        progressBar.populate(type: .offcycle, shouldRefreshWithNetworkChange: true)
        
        if Model.sharedInstance.routes[Model.selectedRouteKey]?.totalMissed == 0 {
//            timeElapsedBar.stopTimer()
        }
        else {
//            timeElapsedBar.populate(routeId: Model.selectedRouteKey)
        }
        
        self.infoTableView.reloadData()
        self.loadOffcycleDashboardTable()
        DashboardViewController.routeListingTableViewDelegate?.didRefreshDataModel()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if DashboardViewController.shouldShowNetworkToolbar {
            let delegate = UIApplication.shared.delegate as! AppDelegate
            delegate.showNetworkToolbar(isOnline: false)
            DashboardViewController.shouldShowNetworkToolbar = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //    func centerMapOnLocation(location: CLLocation) {
    //        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
    //                                                                  regionRadius, regionRadius)
    //        mapView.setRegion(coordinateRegion, animated: true)
    //    }
    
        func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
            let locValue:CLLocationCoordinate2D = manager.location!.coordinate
            print("locations = \(locValue.latitude) \(locValue.longitude)")
//            let initialLocation = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
//            centerMapOnLocation(location: initialLocation)
//            setupMapView()
        }
    
    @IBAction func scrollBottomView(_ sender:UIButton) {
        
        switch toggle {
        // animate downward
        case true:
            UIView.animate(withDuration: 0.5, animations: { [weak self] in
                // change your constraints here
                self?.topConstraint.constant = 0
                self?.toggle = false
                self?.view.layoutIfNeeded()
                }, completion: { (Bool) -> Void in
                    // de-allocates third cell from memory after animating
//                    self.numberOfCell = 2
            })
            sender.imageView?.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
            break
            
        case false:
//            self.numberOfCell = 3
            // animate upward
            UIView.animate(withDuration: 0.5, animations: { [weak self] in
                // change your constraints here
                self?.topConstraint.constant = (self?.mapView.bounds.height)! * -1
                self?.toggle = true
                self?.view.layoutIfNeeded()
                }, completion: nil)
            sender.imageView?.transform = CGAffineTransform(rotationAngle: 0)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.destination is ChangeMeterLocationViewController {
            let destinationVC = segue.destination as? ChangeMeterLocationViewController
            // TODO: Assign selected location code so it shows up as highlighted in the table.
            print(destinationVC as Any)
        }
    
        if segue.destination is ChangeMeterNumberViewController {
            let destinationVC = segue.destination as? ChangeMeterNumberViewController
            destinationVC?.selectedMeter = selectedMeter
        }
        
        if segue.destination is ServiceRequestScreenViewController {
            let destinationVC = segue.destination as? ServiceRequestScreenViewController
            destinationVC?.currentMeterRead = selectedMeter
            destinationVC?.previousDelegate = DashboardViewController.customerInformationDelegate
            destinationVC?.filteredMeterReads = filteredMeterReads
        }
    }
    
}

extension DashboardViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = UIColor.red
        renderer.lineWidth = 4.0
        //print("render")
        return renderer
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let annotation = annotation as? Artwork else {return nil}
        
        let identifier = "marker"
        var view: MKMarkerAnnotationView
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKMarkerAnnotationView {
            dequeuedView.annotation = annotation
            view = dequeuedView
        }else {
            view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            view.canShowCallout = true
            view.calloutOffset = CGPoint(x: -5, y: 5)
            view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        return view
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        let location = view.annotation as! Artwork
        let launchOptions = [MKLaunchOptionsDirectionsModeKey:MKLaunchOptionsDirectionsModeDriving]
        location.mapItem().openInMaps(launchOptions: launchOptions)
    }
}

extension DashboardViewController: SpecialInstructionDelegate {
    func segue() {
        performSegue(withIdentifier: "showServiceRequest", sender: self)
    }
}

extension DashboardViewController: CustomerInformationCellDelegate {
    func editMeterLocationButtonPressed() {
        performSegue(withIdentifier: "toChangeMeterLocation", sender: self)
    }
    
    @objc func editMeterNumberButtonPressed() {
        performSegue(withIdentifier: "toChangeMeterNumber", sender: self)
    }
}

extension DashboardViewController: OrderDetailCellDelegate {
    func didAssignNewFilteredSet(_ newSet: [MeterRead]?) {
        filteredMeterReads = newSet
    }
    
    func didSelectNewMeter(_ newRead: MeterRead) {
        // TODO: Issue - when loading this view with a few work orders completed, it always sets up the map
        // view with the first meter read in the list (along with the correct one),
        // leading to a slight detour in the directions to the first incomplete meter.
        // Goes away once you select another meter in the list.
        
        selectedMeter = newRead
        DashboardViewController.customerInformationDelegate?.populateCustomerInformation(onDashboard: true, hideLine: true, newRead: newRead, buttonsEnabled: true)
        setupMapView()
    }
}

