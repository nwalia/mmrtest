//
//  LocationInformationCell.swift
//  MapView
//
//  Created by Deshpande, Ankush (CA - Toronto) on 2017-12-22.
//  Copyright © 2017 Deshpande, Ankush (CA - Toronto). All rights reserved.
//

import UIKit

class LocationInformationCell: UITableViewCell {

    @IBOutlet weak var visit: Visit!
    var defaultColor: UIColor = .white
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    // TODO: Need some way to call this to populate cells for periodic reads (with no read type)
    func populate(read: MeterRead){
        visit.lateInit(read: read)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        backgroundColor = selected ? GlobalConstants.SELECTED_CELL : defaultColor
        visit.backgroundColor = selected ? GlobalConstants.SELECTED_CELL : defaultColor
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        backgroundColor = highlighted ? GlobalConstants.SELECTED_CELL : defaultColor
        visit.backgroundColor = highlighted ? GlobalConstants.SELECTED_CELL : defaultColor

    }
}
