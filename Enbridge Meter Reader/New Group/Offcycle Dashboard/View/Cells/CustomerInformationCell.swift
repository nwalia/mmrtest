//
//  CustomerInformationCellTableViewCell.swift
//  MapView
//
//  Created by Deshpande, Ankush (CA - Toronto) on 2017-12-21.
//  Copyright © 2017 Deshpande, Ankush (CA - Toronto). All rights reserved.
//

import UIKit

class CustomerInformationCell: UITableViewCell {
    private var toggle = false
    @IBOutlet weak var customerInformation: CustomerInformation!
    
    @IBOutlet weak var scrollButton: UIButton!
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        // Initialization code
        self.layoutMargins = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0)
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
