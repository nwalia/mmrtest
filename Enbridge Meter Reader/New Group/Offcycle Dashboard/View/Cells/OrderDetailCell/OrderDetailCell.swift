//
//  OrderDetailCell.swift
//  MapView
//
//  Created by Deshpande, Ankush (CA - Toronto) on 2017-12-22.
//  Copyright © 2017 Deshpande, Ankush (CA - Toronto). All rights reserved.
//

import UIKit

protocol OrderDetailCellDelegate: class {
    func didSelectNewMeter(_ newRead: MeterRead)
    func didAssignNewFilteredSet(_ newSet: [MeterRead]?)
}

class OrderDetailCell: UITableViewCell, DashboardDelegate {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet weak var reconfigureButton: UIButton!
    @IBOutlet weak var routeListingTableView: RouteListingTableView!
    
    static weak var delegate: OrderDetailCellDelegate?
    
    var filteredMeterReads: [MeterRead]? {
        willSet(newValue){
            OrderDetailCell.delegate?.didAssignNewFilteredSet(newValue)
        }
    }
    //    static var model: Model?
    internal var toggleEdit = false
    
    let invisibleTextField = UITextField(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    var filterPicker: UIPickerView!
    
    let pickerOptions = Array(FilterOptions.offCycle.keys).sorted()
    var filter: ((MeterRead) -> Bool) = {(MeterRead) -> Bool in return true}
    var filterAssigned = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        searchBar.delegate = self
        
        // --- Code for white background search bar for iOS 13 --- //
        if #available(iOS 13.0, *) {
           searchBar.searchTextField.backgroundColor = UIColor.white
        }
        
        
        // Invisible text field, to facilitate popping up the UIPickerView
        invisibleTextField.isHidden = true
        invisibleTextField.inputAssistantItem.leadingBarButtonGroups.removeAll()
        invisibleTextField.inputAssistantItem.trailingBarButtonGroups.removeAll()
        self.addSubview(invisibleTextField)
        
        let firstMeter = (Model.sharedInstance.routes[Model.selectedRouteKey]?.meterReads[0])!
        OrderDetailCell.delegate?.didSelectNewMeter(firstMeter)
        DashboardViewController.tableSelectFirstDelegate = self
        
        // Set up notification observer so AppDelegate can reload service request cell in offcycle dashboard when service request is cancelled or unscheduled or new work is added
        NotificationCenter.default.addObserver(self, selector: #selector(refresh), name: NSNotification.Name(rawValue: "newWork"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(refresh), name: NSNotification.Name(rawValue: "reloadTables"), object: nil)
        
        refresh()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        refresh()
        // Configure the view for the selected state
    }
    
    // Function called by notification in AppDelegate to reload service request cell in offcycle dashboard when service request is cancelled or unscheduled
    @objc func refresh() {
        didRefreshDataModel()
        if Model.sharedInstance.routes[Model.selectedRouteKey]?.meterReads != nil {
            selectFirstNonComplete()
        }
        routeListingTableView.reloadData()
    }
    
    @IBAction func filterButtonClicked(_ sender: UIButton) {
        
        
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: (window?.frame.width)!, height: 45))
        toolbar.barStyle = .default
        
        var items = [UIBarButtonItem]()
        
        let backButton = UIBarButtonItem(title: "<", style: .plain, target: self, action: #selector(backToolbarButton))
        let forwardButton = UIBarButtonItem(title: ">", style: .plain, target: self, action: #selector(forwardToolbarButton))
        
        if let font = UIFont(name: "Arial", size: 24) {
            backButton.setTitleTextAttributes([NSAttributedString.Key.font: font], for: .normal)
            forwardButton.setTitleTextAttributes([NSAttributedString.Key.font: font], for: .normal)
            backButton.setTitleTextAttributes([NSAttributedString.Key.font: font], for: .highlighted)
            forwardButton.setTitleTextAttributes([NSAttributedString.Key.font: font], for: .highlighted)
        }
        
        
        items.append(backButton)
        items.append(forwardButton)
        items.append(UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil) )
        items.append(UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(hidePickerView)) )
        
        toolbar.items = items
        
        filterPicker = UIPickerView(frame: CGRect(x:0, y:0, width:(window?.frame.width)!, height: 220))
        
        filterPicker.delegate = self
        filterPicker.dataSource = self
        filterPicker.backgroundColor = .white
        invisibleTextField.inputView = filterPicker
        invisibleTextField.inputAccessoryView = toolbar
        
        invisibleTextField.becomeFirstResponder()
    }
    
    @IBAction func reconfigureButtonClicked(_ sender: UIButton) {
        !toggleEdit ? enableDragDrop(sender): disableDragDrop(sender)
        (self.viewController() as? DashboardViewController)?.editButtonToggle = toggleEdit
        (self.viewController() as? DashboardViewController)?.infoTableView.reloadData()
        (self.viewController() as? DashboardViewController)?.infoTableView.beginUpdates()
        (self.viewController() as? DashboardViewController)?.infoTableView.endUpdates()
        
    }
    
    func enableDragDrop(_ sender:UIButton) {
        searchBar.isUserInteractionEnabled = false
        filterButton.isEnabled = false
        filterButton.borderColor = .lightGray
        searchBar.alpha = 0.65
        
        self.routeListingTableView.dragDelegate = self
        self.routeListingTableView.dropDelegate = self
        self.routeListingTableView.dragInteractionEnabled = true
        sender.setTitle("Done", for: .normal)
        self.toggleEdit = true
    }
    
    func disableDragDrop(_ sender:UIButton) {
        searchBar.isUserInteractionEnabled = true
        searchBar.alpha = 1.0
        filterButton.isEnabled = true
        filterButton.borderColor = .white
        
        self.routeListingTableView.dragDelegate = nil
        self.routeListingTableView.dropDelegate = nil
        self.routeListingTableView.dragInteractionEnabled = false
        sender.setTitle("Edit", for: .normal)
        self.toggleEdit = false
    }
    
    @objc func hidePickerView(){
        invisibleTextField.resignFirstResponder()
        let row = filterPicker.selectedRow(inComponent: 0)
        if FilterOptions.offCycle[pickerOptions[row]] != nil {
            filter = FilterOptions.offCycle[pickerOptions[row]]!
        }
        else {
            filter = {(MeterRead) -> Bool in return true}
        }
        filterAssigned = pickerOptions[row] != "All Meters"
        if filterAssigned {
            filterButton.setTitle("Filtering", for: .normal)
            filterButton.setTitleColor(GlobalConstants.YELLOW, for: .normal)
        }
        else {
            filterButton.setTitle("Filter", for: .normal)
            filterButton.setTitleColor(.white, for: .normal)
        }
        
        filterContentForSearchText(searchBar.text!)
        routeListingTableView.scrollToFirstRow()
    }
    
    @objc func backToolbarButton(){
        let selectedRow = filterPicker.selectedRow(inComponent: 0)
        
        if selectedRow > 0 {
            filterPicker.selectRow(selectedRow - 1, inComponent: 0, animated: true)
        }
    }
    
    @objc func forwardToolbarButton(){
        let selectedRow = filterPicker.selectedRow(inComponent: 0)
        
        if selectedRow < filterPicker.numberOfRows(inComponent: 0) - 1 {
            filterPicker.selectRow(selectedRow + 1, inComponent: 0, animated: true)
        }
    }
    
    func populateCustomerInformation(onDashboard: Bool, hideLine: Bool, newRead: MeterRead, buttonsEnabled: Bool) {
        //print("Table view found the new meter read")
        // unnecessary?
    }
    
    func didRefreshDataModel() {
        routeListingTableView.reloadData()
    }
    
    func selectFirstNonComplete(){
        routeListingTableView.reloadData()
        
        if self.routeListingTableView.numberOfSections > 0 && self.routeListingTableView.numberOfRows(inSection: 0) > 0 {
            var firstNonCompleteIndex = 0
            
            let meterReadSet = isFiltering() ? filteredMeterReads! : Model.sharedInstance.routes[Model.selectedRouteKey]?.meterReads
            
            for (currentIndex,currentItem) in (meterReadSet?.enumerated())! {
                if !(currentItem.status == .completed || currentItem.status == .pending) {
                    firstNonCompleteIndex = currentIndex
                    break
                }
            }
            
            DispatchQueue.main.async {
                let path = IndexPath(row: firstNonCompleteIndex, section: 0)
                if firstNonCompleteIndex < self.routeListingTableView.numberOfRows(inSection: 0) {
                    self.routeListingTableView.scrollToRow(at: path, at: .top, animated: false)
                }
            }
            
            if Model.sharedInstance.routes[Model.selectedRouteKey]?.meterReads.count != 0 {
            }
        }
        
    }
    
    func selectItemInModel(_ index: Int){
        
        let meterReadSet = isFiltering() ? filteredMeterReads! : Model.sharedInstance.routes[Model.selectedRouteKey]?.meterReads
        
        let selectedRead = meterReadSet![index]
        
        OrderDetailCell.delegate?.didSelectNewMeter(selectedRead)
    }
}

extension OrderDetailCell: OffcycleDashboardViewDidAppearDelegate{
    func selectFirstIncompleteOnViewDidAppear() {
        //selectFirstNonComplete()
        if Model.sharedInstance.routes[Model.selectedRouteKey]?.meterReads != nil {
            selectFirstNonComplete()
        }
    }
}


