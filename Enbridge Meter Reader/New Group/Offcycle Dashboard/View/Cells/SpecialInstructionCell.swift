//
//  SpecialInstructionTableViewCell.swift
//  MapView
//
//  Created by Deshpande, Ankush (CA - Toronto) on 2017-12-21.
//  Copyright © 2017 Deshpande, Ankush (CA - Toronto). All rights reserved.
//

import UIKit

class SpecialInstructionCell: UITableViewCell {
    
    var delegate: SpecialInstructionDelegate?

    @IBAction func enterMeterInfoButtonPressed(_ sender: UIButton) {
        self.delegate?.segue()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    

}
