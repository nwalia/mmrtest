//
//  Artwork.swift
//  MapView
//
//  Created by Deshpande, Ankush (CA - Toronto) on 2017-12-20.
//  Copyright © 2017 Deshpande, Ankush (CA - Toronto). All rights reserved.
//

import Foundation
import MapKit
import Contacts

class Artwork: NSObject, MKAnnotation {
    let title: String?
    let coordinate: CLLocationCoordinate2D
    var storedSubtitle: String?
    
    var subtitle: String? {
        get {
            if storedSubtitle != nil {
                return storedSubtitle
            }
            else {
                return title
            }
        }
        set(newValue) {
            self.storedSubtitle = newValue
        }
    }
    
    var imageName: String? {
        return "pin"
    }
    
    init(title:String,coordinate:CLLocationCoordinate2D) {
        self.title = title
        self.coordinate = coordinate
        
        super.init()
    }
    
    init(title:String, subtitle: String, coordinate:CLLocationCoordinate2D) {

        self.title = title
        self.coordinate = coordinate
        
        super.init()
        
        self.subtitle = subtitle

    }
    
    func mapItem() -> MKMapItem {
        let addressDict = [CNPostalAddressStateKey:subtitle!]
        let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: addressDict)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = title
        return mapItem
    }
}

