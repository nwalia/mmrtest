//
//  SelectTroubleCodeViewController.swift
//  Enbridge Meter Reader
//
//  Created by Wentzel, Johann (CA - Alberta) on 2017-12-07.
//  Copyright © 2017 Enbridge. All rights reserved.
//

import UIKit
import AudioToolbox

class SelectTroubleCodeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewBottomConstraint: NSLayoutConstraint!
    
    var codes: [TroubleCode]!
    var filteredCodes: [TroubleCode] = []
    var selectedCode: TroubleCode?
    var selectedCell: UITableViewCell?
    var selectedIndexPath: IndexPath?
    
    var formattedCodeString: String?
    var returnedCodes: [TroubleCode]?
    var didSubmitCode: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Submit", style: .plain, target: self, action: #selector(submitCodes))
        codes = makeTroubleCodes()
        
        searchBar.delegate = self
        tableView.delegate = self
        tableView.dataSource = self
        
        formattedCodeString = ""
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        // For green editing checkmarks
        tableView.isEditing = true

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
     
        // If didSubmitCode remains false (is not passed back as true from Notes - aka Submit button not tapped), and a row/code has been selected, unselect it in the table.
        if didSubmitCode == false && selectedIndexPath != nil && selectedCode != nil {
            tableView.deselectRow(at: selectedIndexPath!, animated: false)
            selectedCode?.selected = false
        }
        
        // If trouble codes are returned from previous screen, mark as selected
        for selectedCode in returnedCodes ?? [TroubleCode]() {
            for code in codes {
                if code.number == selectedCode.number {
                    code.selected = true
                    code.note = selectedCode.note
                    code.image = selectedCode.image
                }
            }
        }
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tableViewBottomConstraint.constant = 0
//        for code in returnedCodes ?? TroubleCode
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        didSubmitCode = false
    }

    @objc func keyboardWillShow(_ notification: NSNotification){
        guard let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {return}
        let keyboardRectangle = keyboardFrame.cgRectValue
        let keyboardHeight = keyboardRectangle.height
        UIView.animate(withDuration: 0.25, animations: {() -> Void in
            self.tableViewBottomConstraint.constant = keyboardHeight
        })
    }
    
    
    // MARK: - Table View
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isFiltering() ? filteredCodes.count : codes.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectTroubleCodeTableViewCell", for: indexPath) as! SelectTroubleCodeTableViewCell
        
        let codeForCell = isFiltering() ? filteredCodes[indexPath.row] : codes[indexPath.row]
        
        // If codes are passed to this screen as "selected," select the row in the table
        if codeForCell.selected {
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        } else {
            tableView.deselectRow(at: indexPath, animated: false)
        }
            
        // Populate icons on each cell based on the trouble code
        if codeForCell.noteMandatory && codeForCell.photoMandatory {
            cell.iconContainer1.isHidden = false
            cell.iconImage1.image = UIImage(named:"Camera")
            cell.iconLabel1.text = "PHOTO"
            
            cell.iconContainer2.isHidden = false
            cell.iconImage2.image = UIImage(named: "Note")
            cell.iconLabel2.text = "NOTE"
        }
        else if codeForCell.noteMandatory {
            cell.iconContainer1.isHidden = false
            cell.iconImage1.image = UIImage(named: "Note")
            cell.iconLabel1.text = "NOTE"
            
            cell.iconContainer2.isHidden = true
        }
        else if codeForCell.photoMandatory {
            cell.iconContainer1.isHidden = false
            cell.iconImage1.image = UIImage(named: "Camera")
            cell.iconLabel1.text = "PHOTO"
            
            cell.iconContainer2.isHidden = true
        }
        else {
            cell.iconContainer1.isHidden = true
            cell.iconContainer2.isHidden = true
        }
        
        cell.codeDescriptionLabel.text = codeForCell.description
        cell.codeNumberLabel.text = String(codeForCell.number)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }

    
    // Called when user selects a row
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! SelectTroubleCodeTableViewCell
        let codeForCell = isFiltering() ? filteredCodes[indexPath.row] : codes[indexPath.row]
        selectedCode = codeForCell
        selectedCell = cell
        selectedIndexPath = indexPath // used to identify which row in table is selected
        
        CameraCaptureViewController.picreason = getCodeHavingPhotoRequirement(cell: tableView.cellForRow(at: indexPath) as! SelectTroubleCodeTableViewCell)

        if getSelectedCodes().count < 2 {
            conditionalSegue(code: codeForCell)
        }
        else {
            AudioServicesPlaySystemSound(1315);
            let popup = UIAlertController(title: "Error", message: "Sorry, you can only select a maximum of 2 trouble codes.", preferredStyle: .alert)
            popup.addAction(UIAlertAction(title: "Close", style: .default, handler: nil))
            self.present(popup,animated: true)
            selectedCode?.selected = false
            selectedCell?.isSelected = false
        }
    }
    
    func getCodeHavingPhotoRequirement(cell: SelectTroubleCodeTableViewCell) -> String {
        return cell.codeNumberLabel.text!
    }
    
    // Called when user de-selects a row
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! SelectTroubleCodeTableViewCell
        let codeForCell = isFiltering() ? filteredCodes[indexPath.row] : codes[indexPath.row]
        
        codes[indexPath.row].selected = false
        returnedCodes = returnedCodes?.filter{$0.number != codes[indexPath.row].number}
        
        codeForCell.selected = false
        cell.isSelected = false
        cell.backgroundColor = .white
        tableView.reloadRows(at: [indexPath], with: .none)
    }
    
    func conditionalSegue(code:TroubleCode){
        var identifier: String?
        
        if code.photoMandatory {
            // segue to both notes and camera views
            identifier = "launchCameraWithNotes"
        } else {
            // segue to note view only
            identifier = "launchNotes"
        }
        
        if identifier != nil {
            performSegue(withIdentifier: identifier!, sender: nil)
        }
    }
    
    @objc func submitCodes(){
        let submitedCodes = getSelectedCodes()
        if submitedCodes.contains(where: {value in value.photoMandatory == true }) {
            LocalStoreImage.offlineLS?.uploadToDB()
        }
        performSegue(withIdentifier: "toPreviousScreen", sender: nil)
    }
    
    
    // MARK: - Search
    
    func isFiltering() -> Bool {
        return !(searchBar.text?.isEmpty)!
    }
    
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        filteredCodes = codes.filter({(code: TroubleCode) -> Bool in
            return (
                code.description.lowercased().contains(searchText.lowercased())
                || String(code.number).lowercased().contains(searchText.lowercased())
            )
        })
        
        tableView.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filterContentForSearchText(searchText)
        
        if (tableView.numberOfRows(inSection: 0) > 0) {
            let topOfTable = IndexPath(row: 0, section: 0)
            tableView.scrollToRow(at: topOfTable, at: .top, animated: true)
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        UIView.animate(withDuration: 0.25, animations: {() -> Void in
            self.tableViewBottomConstraint.constant = 0
        })
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        switch(segue.identifier ?? ""){
            case "launchCamera":
                if segue.destination is CameraCaptureViewController {
                }
                break
            case "launchNotes":
                if let destinationVC = segue.destination as? NotesViewController {
                    destinationVC.maxCharacters = 40
                    destinationVC.title = "Trouble Message"
                    destinationVC.noteMandatory = selectedCode?.noteMandatory ?? false
                    destinationVC.placeholderText = destinationVC.noteMandatory ? "Insert required note" : "Insert optional note"
                    destinationVC.selectedCode = selectedCode ?? nil
                }
                break
            case "launchCameraWithNotes":
                if let destinationVC = segue.destination as? CameraCaptureViewController {
                    destinationVC.seguesToNote = true
                    destinationVC.maxCharacters = 40
                    destinationVC.titleText = "Trouble Message"
                    destinationVC.noteMandatory = selectedCode?.noteMandatory ?? false
                    destinationVC.placeholderText = destinationVC.noteMandatory! ? "Insert required note" : "Insert optional note"
                }
            case "toPreviousScreen":
                if segue.destination is ServiceRequestScreenViewController {
                    formattedCodeString = createFormattedCodeString()
                    returnedCodes = getSelectedCodes()
                } else if segue.destination is PeriodicOutcardScreenViewController {
                    formattedCodeString = createFormattedCodeString()
                    returnedCodes = getSelectedCodes()
                }
                break
            default:
                print("Error with segue!")
        }

        
    }
 
    @IBAction func unwindToHome(segue: UIStoryboardSegue){
        if segue.source is NotesViewController {
            if let sourceVC = segue.source as? NotesViewController {
                
                if let note = sourceVC.note{
                    //print("Submitted note: \(note)")
                    selectedCode?.note = note
                }
                
                if let imageFromView = sourceVC.image {
                    //print("found image!")
                    selectedCode?.image = imageFromView
                }
            }
        }
        else if let sourceVC = segue.source as? CameraCaptureViewController{
            //print("Unwound from camera")
            if let imageFromView = sourceVC.image {
                selectedCode?.image = imageFromView
            }
        }
      
        toggleSelected(selected: true, code: selectedCode!, cell: selectedCell!)

    }
    
    // Called when unwinding back from Notes or Camera (after Submit button tapped in Notes)
    func toggleSelected(selected: Bool, code: TroubleCode, cell: UITableViewCell){
        if selected {
            code.selected = true
            cell.isSelected = true
        } else {
            code.selected = false
            cell.isSelected = false
        }
        
    }
    
    
    
    // MARK: - Codes
    
    func getSelectedCodes() -> [TroubleCode]{
        return codes.filter({ (code:TroubleCode) -> Bool in
            return code.selected
        })
    }
    
    func createFormattedCodeString() -> String? {
        let codes = getSelectedCodes()
        
        if codes.count == 1 {
            return "Trouble Code: \(String(format: "%02d", codes[0].number))"

        }
        else if codes.count == 2 {
            return "Trouble Code: \(String(format: "%02d", codes[0].number))/\(String(format: "%02d", codes[1].number))"
        }
        else {
            return nil
        }
    }
    
    func makeTroubleCodes() -> [TroubleCode]{
        return [
            TroubleCode(number: 2, description: "METER OUT", noteMandatory: false, photoMandatory: false),
            TroubleCode(number: 3, description: "METER INST WRONG", noteMandatory: false, photoMandatory: false),
            TroubleCode(number: 6, description: "REG ENCLOSED", noteMandatory: true, photoMandatory: false),
            TroubleCode(number: 7, description: "STOPPED METER", noteMandatory: false, photoMandatory: false),
            TroubleCode(number: 8, description: "THEFT/TAMPERING", noteMandatory: false, photoMandatory: true),
            TroubleCode(number: 11, description: "NO EXCHANGE TAG", noteMandatory: false, photoMandatory: false),
            TroubleCode(number: 13, description: "WRONG ROUTE", noteMandatory: false, photoMandatory: false),
            TroubleCode(number: 18, description: "CAN'T READ", noteMandatory: true, photoMandatory: true),
            TroubleCode(number: 20, description: "SHUT OFF BURIED", noteMandatory: true, photoMandatory: true),
            TroubleCode(number: 21, description: "KEY NOT AVAILABLE", noteMandatory: false, photoMandatory: false),
            TroubleCode(number: 31, description: "CAN'T LOCATE METER", noteMandatory: false, photoMandatory: false),
            TroubleCode(number: 35, description: "NOISY METER", noteMandatory: false, photoMandatory: false),
            TroubleCode(number: 36, description: "VACANT", noteMandatory: false, photoMandatory: false),
            TroubleCode(number: 38, description: "GAS ESCAPE", noteMandatory: true, photoMandatory: false),
            TroubleCode(number: 40, description: "METER BURIED/SNOW", noteMandatory: true, photoMandatory: true),
            TroubleCode(number: 41, description: "FROZEN REGULATOR", noteMandatory: true, photoMandatory: true),
            TroubleCode(number: 46, description: "CONDENSATION/FOG", noteMandatory: false, photoMandatory: false),
            TroubleCode(number: 48, description: "ERT ISSUES", noteMandatory: false, photoMandatory: false),
            TroubleCode(number: 60, description: "METER UNDER DECK", noteMandatory: false, photoMandatory: false),
            TroubleCode(number: 62, description: "VEGETATION", noteMandatory: false, photoMandatory: false),
            TroubleCode(number: 63, description: "SURVEY D", noteMandatory: false, photoMandatory: true),
            TroubleCode(number: 64, description: "SURVEY E", noteMandatory: false, photoMandatory: true),
            TroubleCode(number: 65, description: "SURVEY F", noteMandatory: false, photoMandatory: true),
            TroubleCode(number: 66, description: "SURVEY G", noteMandatory: false, photoMandatory: false),
            TroubleCode(number: 67, description: "SURVEY H", noteMandatory: false, photoMandatory: false),
            TroubleCode(number: 68, description: "SURVEY I", noteMandatory: false, photoMandatory: false),
            TroubleCode(number: 69, description: "SURVEY J", noteMandatory: false, photoMandatory: false),
            TroubleCode(number: 71, description: "BATTERY/EXCHANGE REQ'D", noteMandatory: false, photoMandatory: false),
            TroubleCode(number: 78, description: "DANGEROUS METER CONDITIONS", noteMandatory: true, photoMandatory: true),
        ]
    }

}
