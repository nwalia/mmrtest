//
//  PrefsViewController.swift
//  Enbridge Meter Reader
//
//  Created by Lauren Abramsky on 2017-11-29.
//  Copyright © 2017 Enbridge. All rights reserved.
//

import UIKit

class PrefsViewController: UIViewController {
    
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var logOutButton: UIButton!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var versionLabel: UILabel?
    
  
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // Add radius on button
        logOutButton.layer.cornerRadius = 10
        navigationController?.navigationBar.barTintColor = GlobalConstants.GRAY
        username.text = Utilities.sharedInstance.userName ?? ""
        
        // Set read direction segmented control
        let readDirection = UserDefaults.standard.object(forKey: "ReadDirection") as? String
        if readDirection == "Left to Right"{
            segmentedControl.selectedSegmentIndex = 1
        } else {
            segmentedControl.selectedSegmentIndex = 0
        }
        
        //Grab version of the appliation
        let appVersionString: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
        
        //Grab build number of the appliation
        let buildNumber: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
        
        self.versionLabel?.text = "Version: \(appVersionString).\(buildNumber)"
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Set User Defaults setting for read direction
    @IBAction func setReadDirection(_ sender: UISegmentedControl) {
        
        switch segmentedControl.selectedSegmentIndex
        {
        case 0:
            //print("Right to Left selected");
            UserDefaults.standard.set("Right to Left", forKey: "ReadDirection")
        case 1:
            //print("Left to Right selected");
            UserDefaults.standard.set("Left to Right", forKey: "ReadDirection")
        default:
            break;
        }
    }
    
    // Call log out function from SAP SDK Onboarding Class
    @IBAction func logoutButtonPressed(_ sender: UIButton) {
        
        GlobalVariables.logoutPressed = true
        Persistence.shared.saveRoutes()
        Model.clear()
        OnboardingManager.shared.resetOnboarding()
        //URLCache.shared.removeAllCachedResponses()
        //HTTPCookieStorage.shared.removeCookies(since: Date.distantPast)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.unregisterForRemoteNotification()
        appDelegate.dereferenceLocalStore()

    }
}

