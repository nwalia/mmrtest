//
//  SummaryOrderDetailCell.swift
//  Enbridge Meter Reader
//
//  Created by Wentzel, Johann (CA - Alberta) on 2018-01-11.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import UIKit

protocol SummaryOrderDetailCellDelegate: class {
    func didLaunchSegue(withMeterRead read: MeterRead)
}

class SummaryOrderDetailCell: UITableViewCell, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var readingTable: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var filterButton: UIButton!
    
    
    var filteredMeterReads = [MeterRead]()
    var filter: ((MeterRead) -> Bool) = { (MeterRead) -> Bool in return true }
    
    let invisibleTextField = UITextField(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    var filterPicker: UIPickerView!
    var filterAssigned = false
    
    var delegate: SummaryOrderDetailCellDelegate?
    let pickerOptions = Array(FilterOptions.periodic.keys).sorted()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        readingTable.delegate = self
        readingTable.dataSource = self
        searchBar.delegate = self
        
        
        // --- Code for white background search bar for iOS 13 --- //
        if #available(iOS 13.0, *) {
            searchBar.searchTextField.backgroundColor = UIColor.white
        }
        
        // setting button attributes
        filterButton.layer.borderColor = UIColor.white.cgColor
        filterButton.layer.borderWidth = 2
        filterButton.layer.cornerRadius = 10
        
        // Invisible text field, to facilitate popping up the UIPickerView
        invisibleTextField.isHidden = true
        invisibleTextField.inputAssistantItem.leadingBarButtonGroups.removeAll()
        invisibleTextField.inputAssistantItem.trailingBarButtonGroups.removeAll()
        self.addSubview(invisibleTextField)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
    // MARK: - Table View
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isFiltering() ? filteredMeterReads.count : Model.sharedInstance.routes[Model.selectedRouteKey]?.meterReads.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = readingTable.dequeueReusableCell(withIdentifier: "SummaryLocationInformationCell") as! SummaryLocationInformationCell
        
        guard let readForCell = isFiltering() ? filteredMeterReads[indexPath.row] : Model.sharedInstance.routes[Model.selectedRouteKey]?.meterReads[indexPath.row] else { return UITableViewCell() }
        
        cell.read = readForCell
        cell.delegate = self
        
        
        
        
        //         set background color for selected state
        let bgView = UIView()
        bgView.backgroundColor = GlobalConstants.SELECTED_CELL
        cell.selectedBackgroundView = bgView

        
        // Populate cell with values
        cell.mruLabel.text = "MRU :\(readForCell.routeId)"
        cell.nameLabel.text = readForCell.nameLastFirstComma
        cell.addressLine1Label.text = readForCell.fullAddress
        cell.addressLine2Label.text = readForCell.addressLine2
        if readForCell.newFullMeterId != "" {
            cell.meterNumberLabel.text = readForCell.newFullMeterId
        } else {
            cell.meterNumberLabel.text = readForCell.fullMeterId
        }
        
        cell.meterLocationCodeLabel.text = "LOC: \(readForCell.locationCode.id)"
        
        if let est_Skips = readForCell.skips {
            cell.estimationLabel.text = "Est: \(est_Skips)"
        }
        cell.specialInstructionStackView.isHidden = readForCell.specialInstructions == nil || readForCell.specialInstructions == ""
        cell.keyStackView.isHidden = readForCell.keyNumber == nil || (readForCell.keyNumber ?? "").isEmpty
        
        return cell
    }
    
    // MARK: - Filter Button
    
    @IBAction func filterButtonClicked(_ sender: UIButton) {
        
        
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: (window?.frame.width)!, height: 45))
        toolbar.barStyle = .default
        
        var items = [UIBarButtonItem]()
        
        let backButton = UIBarButtonItem(title: "<", style: .plain, target: self, action: #selector(backToolbarButton))
        let forwardButton = UIBarButtonItem(title: ">", style: .plain, target: self, action: #selector(forwardToolbarButton))
        
        if let font = UIFont(name: "Arial", size: 24) {
            backButton.setTitleTextAttributes([NSAttributedString.Key.font: font], for: .normal)
            forwardButton.setTitleTextAttributes([NSAttributedString.Key.font: font], for: .normal)
            backButton.setTitleTextAttributes([NSAttributedString.Key.font: font], for: .highlighted)
            forwardButton.setTitleTextAttributes([NSAttributedString.Key.font: font], for: .highlighted)
        }
        
        
        items.append(backButton)
        items.append(forwardButton)
        items.append(UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil) )
        items.append(UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(hidePickerView)) )
        
        toolbar.items = items
        
        filterPicker = UIPickerView(frame: CGRect(x:0, y:0, width:(window?.frame.width)!, height: 220))
        
        filterPicker.delegate = self
        filterPicker.dataSource = self
        filterPicker.backgroundColor = .white
        invisibleTextField.inputView = filterPicker
        invisibleTextField.inputAccessoryView = toolbar
        
        invisibleTextField.becomeFirstResponder()
    }
    
    @objc func hidePickerView(){
        readingTable.scrollToFirstRow()
        
        let selectedRow = filterPicker.selectedRow(inComponent: 0)
        
        if FilterOptions.periodic[pickerOptions[selectedRow]] != nil {
            filter = FilterOptions.periodic[pickerOptions[selectedRow]]!
        }
        else {
            filter = {(MeterRead) -> Bool in return true}
        }
        
        filterAssigned = pickerOptions[selectedRow] != "All Meters"
        if filterAssigned {
            filterButton.setTitle("Filtering", for: .normal)
            filterButton.setTitleColor(GlobalConstants.YELLOW, for: .normal)
        }
        else {
            filterButton.setTitle("Filter", for: .normal)
            filterButton.setTitleColor(.white, for: .normal)
        }
        
        filterContentForSearchText(searchBar.text!)
        
        invisibleTextField.resignFirstResponder()
    }
    
    @objc func backToolbarButton(){
        let selectedRow = filterPicker.selectedRow(inComponent: 0)
        
        if selectedRow > 0 {
            filterPicker.selectRow(selectedRow - 1, inComponent: 0, animated: true)
        }
    }
    
    @objc func forwardToolbarButton(){
        let selectedRow = filterPicker.selectedRow(inComponent: 0)
        
        if selectedRow < filterPicker.numberOfRows(inComponent: 0) - 1 {
            filterPicker.selectRow(selectedRow + 1, inComponent: 0, animated: true)            
        }
    }

}

extension SummaryOrderDetailCell: SummaryLocationInformationCellDelegate {
    func didPressReturnButton(forMeterRead read: MeterRead?) {
        guard read != nil else { return }
        
        delegate?.didLaunchSegue(withMeterRead: read!)
        
    }
    
    
}
