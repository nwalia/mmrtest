//
//  RouteSummaryViewController+Helper.swift
//  Enbridge Meter Reader
//
//  Created by Deshpande, Ankush (CA - Toronto) on 2018-01-10.
//  Copyright © 2018 Enbridge. All rights reserved.
//

import Foundation
import UIKit
import AudioToolbox

extension UIColor {
    static var enbridgeYellow : UIColor{
        get{
           return UIColor(red: 247/255, green: 199/255, blue: 94/255, alpha: 1)
        }
    }
    static var clementine : UIColor {
        get{
            return UIColor(red: 255/255, green: 105/255, blue: 0/255, alpha: 1)
        }
    }
    static var dusk : UIColor {
        get{
            return UIColor(red: 93/255, green: 83/255, blue: 139/255, alpha: 1)
        }
    }
}

extension RouteSummaryViewController {
    func presentNotificationWith(title: String, message:String, sender: UIButton) {
        
         if (sender.titleLabel!.text! == "Request Resequence") ||  (sender.titleLabel!.text! == "Resequence Confirmed") {
         
            if  !(Model.sharedInstance.routes[Model.selectedRouteKey]!.resequence) {
         
                 // Show alert to confirm
                 AudioServicesPlaySystemSound(1315);
                 let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
         
                 // Change button to yellow and "Resequence Confirmed"
                 let CONFIRMAction = UIAlertAction(title: "Confirm", style: .default) { action in
                    RouteSummaryViewController.toggle = true
                    //print(RouteSummaryViewController.toggle)
                 sender.setTitleColor(UIColor.enbridgeYellow, for: .normal)
                 sender.setTitle("Resequence Confirmed", for: .normal)
                    // Set resequenced boolean for route to true
                    Model.sharedInstance.routes[Model.selectedRouteKey]!.resequence = true
                 }
                 let CancelAction = UIAlertAction(title: "Cancel", style: .default) { action in
                 }
         
                 alertController.addAction(CancelAction)
                 alertController.addAction(CONFIRMAction)
         
                 self.present(alertController, animated: true) {
                 // ...
                 }
         
             } else {
         
                 // Show alert to remove
                 AudioServicesPlaySystemSound(1315);
                 let alertController = UIAlertController(title: "Remove Request", message: "Do you want to remove your resequence request?"  , preferredStyle: .alert)
                
                 // Change button to white and "Request Resequence"
                 let CONFIRMAction = UIAlertAction(title: "Remove", style: .default) { action in
                    RouteSummaryViewController.toggle = false
                    //print(RouteSummaryViewController.toggle)
                 sender.setTitleColor(.white, for: .normal)
                 sender.setTitle("Request Resequence", for: .normal)
                    //Set resequenced boolean for route to false
                    Model.sharedInstance.routes[Model.selectedRouteKey]!.resequence = false
                 }
                 let CancelAction = UIAlertAction(title: "Cancel", style: .default) { action in
                 sender.setTitleColor(UIColor.enbridgeYellow, for: .normal)
                 sender.setTitle("Resequence Confirmed", for: .normal)
                 }
         
                 alertController.addAction(CancelAction)
                 alertController.addAction(CONFIRMAction)
         
                 self.present(alertController, animated: true) {
                 // ...
                 }
         
             }
         
         
         } else if sender.titleLabel!.text! == "Submit" {
         
             AudioServicesPlaySystemSound(1315);
             let alertController = UIAlertController(title: "Submit Route", message: "Are you sure you want to submit your data for this route?" , preferredStyle: .alert)
         
             let ConfirmAction = UIAlertAction(title: "Confirm", style: .default, handler: { action in
                
                DispatchQueue.main.async {
                    SelectRouteTableViewController.refreshControlRef?.endRefreshing()
                    if let tabsController = UIApplication.shared.delegate?.window??.rootViewController as? MainNavViewController {
                        if let selectVC = tabsController.viewControllers![0].children.first as? SelectRouteTableViewController{
                            selectVC.loadBlurView()
                        }
                    }
                }
                
                for currentMeterRead in (Model.sharedInstance.routes[Model.selectedRouteKey]?.meterReads)! {
                    do {
                        currentMeterRead.mmrStatus = "FIELD COMPLETE"
                        if currentMeterRead.isOutCard ?? false && !AppDelegate.hasMockData {
                            let fetchedEntities = try LocalStoreManager.ShareInstance?.updateOperation(type: RouteType.OUTCARD.rawValue, operation: MeterRead.mapTo_Operation(meterRead: currentMeterRead, withCompletionCode: ""))
                            print("Fetched From UpdateEntity Function Call:\(String(describing: fetchedEntities?.0)),\(String(describing: fetchedEntities?.1))")
                        }else if currentMeterRead.isPeriodic ?? false && !AppDelegate.hasMockData {
                            let fetchedEntities = try LocalStoreManager.ShareInstance?.updateOperation(type: RouteType.PERIODIC.rawValue, operation: MeterRead.mapTo_Operation(meterRead: currentMeterRead, withCompletionCode: ""))
                            print("Fetched From UpdateEntity Function Call:\(String(describing: fetchedEntities?.0)),\(String(describing: fetchedEntities?.1))")
                        }
                    } catch {
                        print("Update Operation Failed: \(error)")
                    }
                }
                
                LocalStoreManager.ShareInstance?.submitOutcardRoute{error in
                    if let tabsController = UIApplication.shared.delegate?.window??.rootViewController as? MainNavViewController {
                        if let selectVC = tabsController.viewControllers![0].children.first as? SelectRouteTableViewController{
                            OperationQueue.main.addOperation {
                                selectVC.unloadBlurView()
                            }
                        }
                    }
                }
                
                let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
         
                let OKAction = UIAlertAction(title: "Okay", style: .default) { [weak self] action in
             
               
                 switch RouteSummaryViewController.caller {
                 
                    case "outCardCellSummary"?:
                         //OutCardCell.showViewRouteSubmissionButtonFlag = true
                        let i = self?.navigationController?.viewControllers.firstIndex(of: self!)
                         let previousViewController = self?.navigationController?.viewControllers[i!-1] as? SelectRouteTableViewController
                         previousViewController?.tableView.reloadRows(at: [IndexPath(row: 2, section: 0)], with: .automatic)
                         self?.performSegue(withIdentifier: "unwindToSelectRoute", sender: self)
             
                     case "periodicCellSummary"?:
                         PeriodicCell.showViewRouteSubmissionButtonFlag = true
                         let i = self?.navigationController?.viewControllers.firstIndex(of: self!)
                         var previousViewController = self?.navigationController?.viewControllers[i!-1] as? SelectRouteTableViewController
                         previousViewController = previousViewController != nil ? self?.navigationController?.viewControllers[i!-1] as? SelectRouteTableViewController : self?.navigationController?.viewControllers[i!-3] as? SelectRouteTableViewController
                         // previousViewController?.tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
    //                     (previousViewController?.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! PeriodicCell).layoutSubviews()
                         self?.performSegue(withIdentifier: "unwindToSelectRoute", sender: self)
                 
                     case "OutcardCell"?:
                        // OutCardCell.showViewRouteSubmissionButtonFlag = true
                        let i = self?.navigationController?.viewControllers.firstIndex(of: self!)
                         let previousViewController = self?.navigationController?.viewControllers[i!-3] as? SelectRouteTableViewController
                         (previousViewController?.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? OutCardCell)?.layoutSubviews()
                         self?.performSegue(withIdentifier: "unwindToSelectRoute", sender: self)
                    
                    default:
                        return
                    }
         
             }
         
             alertController.addAction(OKAction)
         
             self.present(alertController, animated: true) {
             // ...
             }
             })
         
             let CancelAction = UIAlertAction(title: "Cancel", style: .default, handler: { action in
             // do nothing here
             })
         
             alertController.addAction(CancelAction)
             alertController.addAction(ConfirmAction)
         
             self.present(alertController, animated: true) {
             // ...
             }
         
         }
    }
}
