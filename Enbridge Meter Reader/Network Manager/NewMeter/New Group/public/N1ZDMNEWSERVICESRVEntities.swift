// # Proxy Compiler 17.12.5-600bdb-20180507

import Foundation
import SAPOData

open class N1ZDMNEWSERVICESRVEntities<Provider: DataServiceProvider>: DataService<Provider> {
    public override init(provider: Provider) {
        super.init(provider: provider)
        self.provider.metadata = N1ZDMNEWSERVICESRVEntitiesMetadata.document
    }

    open func fetchZdmNewService(matching query: DataQuery) throws -> N1ZdmNewService {
        return try CastRequired<N1ZdmNewService>.from(self.executeQuery(query.fromDefault(N1ZDMNEWSERVICESRVEntitiesMetadata.EntitySets.zdmNewServiceColl)).requiredEntity())
    }

    open func fetchZdmNewService(matching query: DataQuery, completionHandler: @escaping (N1ZdmNewService?, Error?) -> Void) {
        self.addBackgroundOperation {
            do {
                let result: N1ZdmNewService = try self.fetchZdmNewService(matching: query)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open func fetchZdmNewServiceColl(matching query: DataQuery = DataQuery()) throws -> Array<N1ZdmNewService> {
        return try N1ZdmNewService.array(from: self.executeQuery(query.fromDefault(N1ZDMNEWSERVICESRVEntitiesMetadata.EntitySets.zdmNewServiceColl)).entityList())
    }

    open func fetchZdmNewServiceColl(matching query: DataQuery = DataQuery(), completionHandler: @escaping (Array<N1ZdmNewService>?, Error?) -> Void) {
        self.addBackgroundOperation {
            do {
                let result: Array<N1ZdmNewService> = try self.fetchZdmNewServiceColl(matching: query)
                self.completionQueue.addOperation {
                    completionHandler(result, nil)
                }
            } catch let error {
                self.completionQueue.addOperation {
                    completionHandler(nil, error)
                }
            }
        }
    }

    open override func refreshMetadata() throws {
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }
        do {
            try ProxyInternal.refreshMetadata(service: self, fetcher: nil, options: N1ZDMNEWSERVICESRVEntitiesMetadataParser.options)
            N1ZDMNEWSERVICESRVEntitiesMetadataChanges.merge(metadata: self.metadata)
        }
    }

    @available(swift, deprecated: 4.0, renamed: "fetchZdmNewServiceColl")
    open func zdmNewServiceColl(query: DataQuery = DataQuery()) throws -> Array<N1ZdmNewService> {
        return try self.fetchZdmNewServiceColl(matching: query)
    }

    @available(swift, deprecated: 4.0, renamed: "fetchZdmNewServiceColl")
    open func zdmNewServiceColl(query: DataQuery = DataQuery(), completionHandler: @escaping (Array<N1ZdmNewService>?, Error?) -> Void) {
        self.fetchZdmNewServiceColl(matching: query, completionHandler: completionHandler)
    }
}
