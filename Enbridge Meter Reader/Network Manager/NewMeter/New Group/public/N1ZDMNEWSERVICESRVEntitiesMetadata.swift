// # Proxy Compiler 17.12.5-600bdb-20180507

import Foundation
import SAPOData

public class N1ZDMNEWSERVICESRVEntitiesMetadata {
    public static var document: CSDLDocument = N1ZDMNEWSERVICESRVEntitiesMetadata.resolve()

    private static func resolve() -> CSDLDocument {
        try! N1ZDMNEWSERVICESRVEntitiesFactory.registerAll()
        N1ZDMNEWSERVICESRVEntitiesMetadataParser.parsed.hasGeneratedProxies = true
        return N1ZDMNEWSERVICESRVEntitiesMetadataParser.parsed
    }

    public class EntityTypes {
        public static var zdmNewService: EntityType = N1ZDMNEWSERVICESRVEntitiesMetadataParser.parsed.entityType(withName: "ZDM_NEW_SERVICE_SRV.ZDM_NEW_SERVICE")
    }

    public class EntitySets {
        public static var zdmNewServiceColl: EntitySet = N1ZDMNEWSERVICESRVEntitiesMetadataParser.parsed.entitySet(withName: "ZDM_NEW_SERVICE_COLL")
    }
}
