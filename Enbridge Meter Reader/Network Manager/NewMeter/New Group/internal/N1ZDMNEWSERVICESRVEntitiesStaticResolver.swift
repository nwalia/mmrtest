// # Proxy Compiler 17.12.5-600bdb-20180507

import Foundation
import SAPOData

internal class N1ZDMNEWSERVICESRVEntitiesStaticResolver: ObjectBase {
    override init() {
    }

    class func resolve() {
        N1ZDMNEWSERVICESRVEntitiesStaticResolver.resolve1()
    }

    private class func resolve1() {
        Ignore.valueOf_any(N1ZDMNEWSERVICESRVEntitiesMetadata.EntityTypes.zdmNewService)
        Ignore.valueOf_any(N1ZDMNEWSERVICESRVEntitiesMetadata.EntitySets.zdmNewServiceColl)
        Ignore.valueOf_any(N1ZdmNewService.equnr)
        Ignore.valueOf_any(N1ZdmNewService.matnr)
        Ignore.valueOf_any(N1ZdmNewService.ableinh)
        Ignore.valueOf_any(N1ZdmNewService.vZwstand)
        Ignore.valueOf_any(N1ZdmNewService.address)
        Ignore.valueOf_any(N1ZdmNewService.dials)
    }
}
