// # Proxy Compiler 17.12.5-600bdb-20180507

import Foundation
import SAPOData

internal class N1ZDMNEWSERVICESRVEntitiesMetadataChanges: ObjectBase {
    override init() {
    }

    class func merge(metadata: CSDLDocument) {
        metadata.hasGeneratedProxies = true
        N1ZDMNEWSERVICESRVEntitiesMetadata.document = metadata
        N1ZDMNEWSERVICESRVEntitiesMetadataChanges.merge1(metadata: metadata)
        try! N1ZDMNEWSERVICESRVEntitiesFactory.registerAll()
    }

    private class func merge1(metadata: CSDLDocument) {
        Ignore.valueOf_any(metadata)
        if !N1ZDMNEWSERVICESRVEntitiesMetadata.EntityTypes.zdmNewService.isRemoved {
            N1ZDMNEWSERVICESRVEntitiesMetadata.EntityTypes.zdmNewService = metadata.entityType(withName: "ZDM_NEW_SERVICE_SRV.ZDM_NEW_SERVICE")
        }
        if !N1ZDMNEWSERVICESRVEntitiesMetadata.EntitySets.zdmNewServiceColl.isRemoved {
            N1ZDMNEWSERVICESRVEntitiesMetadata.EntitySets.zdmNewServiceColl = metadata.entitySet(withName: "ZDM_NEW_SERVICE_COLL")
        }
        if !N1ZdmNewService.equnr.isRemoved {
            N1ZdmNewService.equnr = N1ZDMNEWSERVICESRVEntitiesMetadata.EntityTypes.zdmNewService.property(withName: "Equnr")
        }
        if !N1ZdmNewService.matnr.isRemoved {
            N1ZdmNewService.matnr = N1ZDMNEWSERVICESRVEntitiesMetadata.EntityTypes.zdmNewService.property(withName: "Matnr")
        }
        if !N1ZdmNewService.ableinh.isRemoved {
            N1ZdmNewService.ableinh = N1ZDMNEWSERVICESRVEntitiesMetadata.EntityTypes.zdmNewService.property(withName: "Ableinh")
        }
        if !N1ZdmNewService.vZwstand.isRemoved {
            N1ZdmNewService.vZwstand = N1ZDMNEWSERVICESRVEntitiesMetadata.EntityTypes.zdmNewService.property(withName: "VZwstand")
        }
        if !N1ZdmNewService.address.isRemoved {
            N1ZdmNewService.address = N1ZDMNEWSERVICESRVEntitiesMetadata.EntityTypes.zdmNewService.property(withName: "Address")
        }
        if !N1ZdmNewService.dials.isRemoved {
            N1ZdmNewService.dials = N1ZDMNEWSERVICESRVEntitiesMetadata.EntityTypes.zdmNewService.property(withName: "Dials")
        }
    }
}
