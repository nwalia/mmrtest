// # Proxy Compiler 17.12.5-600bdb-20180507

import Foundation
import SAPOData

internal class N1ZDMNEWSERVICESRVEntitiesFactory {
    static func registerAll() throws {
        N1ZDMNEWSERVICESRVEntitiesMetadata.EntityTypes.zdmNewService.registerFactory(ObjectFactory.with(create: { N1ZdmNewService(withDefaults: false) }, createWithDecoder: { d in try N1ZdmNewService(from: d) }))
        N1ZDMNEWSERVICESRVEntitiesStaticResolver.resolve()
    }
}
