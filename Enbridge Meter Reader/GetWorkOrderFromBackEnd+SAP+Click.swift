

import Foundation
import UIKit
import SAPFoundation
import SAPOData
import SAPFiori
import SAPCommon
import SAPOfflineOData
import proxyclasses

class GetWorkOrderFromBackEnd_SAP_Click {
    static let sharedInstance = GetWorkOrderFromBackEnd_SAP_Click()
    internal let appDelegate = UIApplication.shared.delegate as! AppDelegate
    internal var ec1: M1Ec1<OnlineODataProvider> {
        return self.appDelegate.MEc1
    }
    //    private var ec1_offline: Ec1<OfflineODataProvider> {
    //        return LocalStoreOffCycle.returnSP();
    //    }
    static var entities: [M1Operation] = [M1Operation]()
    internal let logger = Logger.shared(named: "Get Work Order From Backend")
    internal var error : Error?
    static var urlSession : SAPURLSession?
    internal var MRUList:Set = Set<String>()   //used for sending the request to the backend
    internal var RouteKeyList:Set = Set<String>() // used for each route cell (disable/enable)
    internal var uploadSucceed =  true
    internal var tempRoute:Route = Route()
    internal var firstOffcycle:Bool = false
    internal var uploading = false
    internal var totalUpload = 0
    internal var uploadCounter = 0 {
        didSet {
            if uploadCounter >= totalUpload {
                uploading = false
                DispatchQueue.main.async {
                    SelectRouteTableViewController.refreshControlRef?.endRefreshing()
                    if let tabsController = UIApplication.shared.delegate?.window??.rootViewController as? MainNavViewController {
                        if let selectVC = tabsController.viewControllers![0].children.first as? SelectRouteTableViewController{
                            selectVC.unloadBlurView()
                            selectVC.tableView.reloadData()
                            if self.uploadSucceed {
                                //nothing for now
                            }else{
                                let alertController = UIAlertController(title: "Failed to acknowledge routes", message: String(describing: "Failed to sync with backend, route will be disabled and please retry"), preferredStyle: .alert)
                                alertController.addAction(UIAlertAction(title: "OK", style: .default))
                                selectVC.present(alertController, animated: true, completion: {})
                            }
                            
                        }
                    }
                }
                LogHelper.shared?.upload()
            }
        }
    }
    static var byteSent = 0 {
        didSet {
            DispatchQueue.main.async {
                if let tabsController = UIApplication.shared.delegate?.window??.rootViewController as? MainNavViewController {
                    if let selectVC = tabsController.viewControllers![0].children.first as? SelectRouteTableViewController{
                        for cell in selectVC.tableView.visibleCells {
                            if let offcycleCell = cell as? OffCycleCell {
                                if offcycleCell.uploading{
                                    offcycleCell.uploadingText = GlobalConstants.ROUTE_UPLOADING_MESSAGE + "(\(byteSent)Bytes)"
                                    offcycleCell.setNeedsLayout()
                                }
                            }else if let outcardCell = cell as? OutCardCell {
                                if outcardCell.uploading{
                                    outcardCell.uploadingText = GlobalConstants.ROUTE_UPLOADING_MESSAGE + "(\(byteSent)Bytes)"
                                    outcardCell.setNeedsLayout()
                                }
                            }else if let periodicCell = cell as? PeriodicCell {
                                if periodicCell.uploading{
                                    periodicCell.uploadingText = GlobalConstants.ROUTE_UPLOADING_MESSAGE + "(\(byteSent)Bytes)"
                                    periodicCell.setNeedsLayout()
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func loadData(completionHandler: @escaping () -> Void) {
        
        //        /////////////////////////////////////////////
        //        // uncomment me if you  want local test data!
        //         AppDelegate.hasMockData = true
        //         createMockData(numberOfReads: 2, type: "Periodic")
        //         createMockData(numberOfReads: 2, type: "Offcycle")
        //         createMockData(numberOfReads: 2, type: "Outcard")
        //        ////////////////////////////////////////////
        
        logger.logLevel = .debug
        
        LogHelper.shared?.debug("Attempting to get work orders", loggerObject: self.logger)
        
        //do the download
        LocalStoreManager.ShareInstance?.fetchByFresh(){[weak self] Error in
            guard Error  == nil else { // should never be the case, becasue user will be forced to retry
                return
            }
            self?.populateUI()
            completionHandler()
        }
    }
    
    // Creates mock data for dev/test use.
    func populateUI(){
        
        if let mETERREADINFOSet = LocalStoreManager.ShareInstance?.getAllWork() {
            GetWorkOrderFromBackEnd_SAP_Click.entities = self.setResequencedFlag(mETERREADINFOSet)
            LogHelper.shared?.debug("Getting data from online service provider ok", loggerObject: self.logger)
            
            self.processData()
            
            //sync work order status with backend/remove unscheduled work orders (offcycle only)
            if let route = Model.sharedInstance.routes[GlobalConstants.OFFCYCLE_ROUTE_ID] {
                let mr = route.meterReads
                
                let temp = self.syncMMR_Operation(operations: mETERREADINFOSet, mmr: mr, routeType: GlobalConstants.OFFCYCLE_ROUTE_ID)
                Model.sharedInstance.routes[GlobalConstants.OFFCYCLE_ROUTE_ID]?.meterReads = temp
                
                // --- Remove complete route --- //
                if Model.sharedInstance.routes[GlobalConstants.OFFCYCLE_ROUTE_ID]?.meterReads.count == 0 {
                    Model.sharedInstance.routes.removeValue(forKey: GlobalConstants.OFFCYCLE_ROUTE_ID)
                }
                
                // --- Method calling for locally save routes in DB --- //
                Persistence.shared.saveRoutes()
            }
        }
    }
    
    
    private func setResequencedFlag(_  infoSet : [M1Operation]) -> [M1Operation] {
        infoSet.forEach{$0.object?.task?.mmrResequence = false}
        return infoSet
    }
    
    
    //parameter -> new data , existing data
    private func syncMMR_Operation(operations:[M1Operation],mmr:[MeterRead],routeType:String) -> [MeterRead]{
        var mmrContainer = mmr
        
        // --- Not using anywhere thats why comment it --- //
        // let completeExpiryDate = Date().sixPMOfTodayEvening //Date().lastSecondOfDayAgo
        
        mmrContainer.enumerated().reversed().forEach{
            
            let jobId = $1.jobID
            let mmrObject = $1
            let index = $0
            let filteredOpt = operations.filter({ //check if the current WO (on device) still exisiting in new data from click
                (opt:M1Operation) -> Bool in
                if opt.callID == jobId {
                    return true
                }else {
                    return false
                }
            })
            if filteredOpt.count <= 0 { //the current WO (on device) doenst not on send from backend anymore
                if (mmrObject.mmrStatus != MeterReadNetworkStatus.FIELD_COMPLETE.rawValue && mmrObject.mmrStatus != MeterReadNetworkStatus.CANCEL.rawValue) || (routeType == GlobalConstants.OFFCYCLE_ROUTE_ID && mmrObject.status == .completed && mmrObject.rawReadDate != nil && (mmrObject.rawReadDate! < Date().lastSecondOfDayAgo || (mmrObject.rawReadDate! < Date().sixPMOfTodayEvening && Date() > Date().sixPMOfTodayEvening))) { //if (not FC & not Cancel) OR (FC or Cancel yeasterday), remove it
                    mmrContainer.remove(at: index)
                }
            }else{
                if mmrObject.mmrStatus != filteredOpt.first?.object?.task?.status {
                    mmrContainer[index].mmrStatus = filteredOpt.first?.object?.task?.status
                    mmrContainer[index].status = Utilities.sharedInstance.checkForNetworkStatus(MeterRead.mapTo_MeterRead(filteredOpt.first!))
                }
            }
        }
        
        return mmrContainer
    }
    
    
    func processData(){
        
        // --- Create temp route --- //
        tempRoute = Route(id:GlobalConstants.OFFCYCLE_ROUTE_ID)
        tempRoute.type = .OFFCYCLE
        
        // --- Perform all operations on tempRoute --- //
        // --- Replace GetWorkOrderFromBackEnd_SAP_Click.entities with finalTempArray --- //
        GetWorkOrderFromBackEnd_SAP_Click.entities.forEach {
            let meterRead = MeterRead.mapTo_MeterRead($0)
            
            
            insertIntoRoute(read: meterRead)
            
            // -- It applies for all readings in GetWorkOrderFromBackEnd_SAP_Click.entities --- //
            insertjob_ID_For(meter: meterRead)
        }
    
        
        // --- Check if tempRoute is nil, then bypass: otherwise (i) perform sorting (ii) add this tempRoute to Model.SharedInstance.Routes --- //
         if !tempRoute.meterReads.isEmpty {
            
        // --- Sorting ---- //
        tempRoute.meterReads = tempRoute.meterReads.sorted(by: {(w1,w2)  -> Bool in
            return w1.assignmentStart ?? "" < w2.assignmentStart ?? ""
        })
            
            // --- If firstOffcycle appears, then empty Model.sharedInstance.routes
            if firstOffcycle {
                
                Model.sharedInstance.routes[GlobalConstants.OFFCYCLE_ROUTE_ID] = Route(id:GlobalConstants.OFFCYCLE_ROUTE_ID)
                Model.sharedInstance.routes[GlobalConstants.OFFCYCLE_ROUTE_ID]?.type = .OFFCYCLE
                
               // --- This is problem with previous code thats why region is not showing --- //
                Model.sharedInstance.routes[GlobalConstants.OFFCYCLE_ROUTE_ID]?.region = tempRoute.region
                
                firstOffcycle = false
            }
            
        // --- Add Sorted array to existing Model.sharedInstance.routes --- //
       
        Model.sharedInstance.routes[GlobalConstants.OFFCYCLE_ROUTE_ID]!.meterReads += tempRoute.meterReads
        }
        
        for (_,route) in Model.sharedInstance.routes {
            route.populateAnnotations()
            
            //            // Sort periodic routes by their sequence number, dont need it anymore... already sorted by sequence number from backend
            //            if route.type == .PERIODIC {
            //                route.meterReads = route.meterReads.sorted(by: { (Int($0.mmrSequenceNum ?? "") ?? 0) < (Int($1.mmrSequenceNum ?? "") ?? 0) })
            //            }
            //Change route status
            if route.type == .OUTCARD && !(route.isSorted!){
                route.meterReads = route.meterReads.sorted(by: {(w1,w2) -> Bool in
                    if w1.routeId < w2.routeId {
                        return true
                    }else if w1.routeId > w2.routeId {
                        return false
                    }else {
                        return w1.mmrSequenceNum ?? "" < w2.mmrSequenceNum ?? ""
                    }
                })
                route.isSorted = true
            }
            
            // --- Comment it & place over other place --- //
            //            if route.type == .OFFCYCLE && !(route.isSorted!){
            //                route.meterReads = route.meterReads.sorted(by: {(w1,w2)  -> Bool in
            //                    return w1.assignmentStart ?? "" < w2.assignmentStart ?? ""
            //                })
            //                route.isSorted = true
            //            }
            
        }
        
    }
    
    
    // --- This method is called when individual values read go insert into Route --- //
    func insertIntoRoute(read: MeterRead) {
        let status = read.networkStatus
        
        // --- We have to make changes only on Offcycle --- //
        if read.type == "Offcycle" {
            
            
            // OFFCYCLE: collect all offcycle reads into one offcycle pseudo-route.
            if Model.sharedInstance.routes[GlobalConstants.OFFCYCLE_ROUTE_ID] != nil {
                
                // If route is completed - change status back to inprogress
                if Model.sharedInstance.routes[GlobalConstants.OFFCYCLE_ROUTE_ID]!.status == .completed  || Model.sharedInstance.routes[GlobalConstants.OFFCYCLE_ROUTE_ID]!.status == .pending {
                    Model.sharedInstance.routes[GlobalConstants.OFFCYCLE_ROUTE_ID]!.status = .inProgress
                }
                
                // --- Perform all alteration over here --- //
                // --- MAke verification before going to done anything --- //
                // ---- (i) Done: This method is to make sure that it calls every time it loads new read into Offcycle Route,
                // ---- (ii) also. first time it gone call
                if !Model.sharedInstance.routes[GlobalConstants.OFFCYCLE_ROUTE_ID]!.meterReads.contains(read){
                    tempRoute.meterReads.append(read)
                }
            }
            else {
                
                // ---  This method is called when Offcycle route is "nil" but we have to assign itto Model.sharedInstance.routes or may be it is first time ---- //
                
                let newRoute = Route(id:GlobalConstants.OFFCYCLE_ROUTE_ID)
                newRoute.type = .OFFCYCLE
                newRoute.meterReads.append(read)
                // Assign first not nil region as route region
                if newRoute.region == nil && read.region != nil {
                    newRoute.region = read.region
                }
                
                // --- Here to check region --- //
                //newRoute.region = newRoute.meterReads[0].region
                print("! newRoute.region: \(String(describing: newRoute.region))")
                
                // --- Here whenever first time added: it adds also to (i) Global Local Offcycle Store (ii) tempRoute
                firstOffcycle = true
                tempRoute = newRoute
                Model.sharedInstance.routes[GlobalConstants.OFFCYCLE_ROUTE_ID] = newRoute
            }
            
            if status == .EN_Route || status == .ON_SITE{
                Model.sharedInstance.routes[GlobalConstants.OFFCYCLE_ROUTE_ID]!.status = .inProgress
            }
        }
        else if read.type == "Outcard" {
            // OUTCARD: collect all outcard reads into one outcard pseudo-route.
            if Model.sharedInstance.routes[GlobalConstants.OUTCARD_ROUTE_ID + read.cycleNo] != nil {
                
                // Add it to route
                if !Model.sharedInstance.routes[GlobalConstants.OUTCARD_ROUTE_ID + read.cycleNo]!.meterReads.contains(read){
                    Model.sharedInstance.routes[GlobalConstants.OUTCARD_ROUTE_ID + read.cycleNo]!.meterReads.append(read)
                }
                
            }
            else {
                let newRoute = Route(id:GlobalConstants.OUTCARD_ROUTE_ID + read.cycleNo)
                newRoute.type = .OUTCARD
                newRoute.meterReads.append(read)
                newRoute.lastMeterCompleted = newRoute.meterReads[0].routeId
                newRoute.cycleNo = newRoute.meterReads[0].cycleNo
                
                // Assign first not nil region as route region
                if newRoute.region == nil && read.region != nil {
                    newRoute.region = read.region
                }
                
                Model.sharedInstance.routes[GlobalConstants.OUTCARD_ROUTE_ID + read.cycleNo] = newRoute
                //                print("DEBUG: Creating outcard bucket")
            }
        }
        else {
            if Model.sharedInstance.routes[read.routeId] != nil {
                // PERIODIC: If we have seen this routeID,  Insert into an already-created route.
                if !Model.sharedInstance.routes[read.routeId]!.meterReads.contains(read){
                    Model.sharedInstance.routes[read.routeId]!.meterReads.append(read)
                }
            }else{
                // PERIODIC: Else we haven't seen this routeID, create a new route with this ID.
                let newRoute = Route(id:read.routeId)
                newRoute.type = read.type == "Periodic" ? .PERIODIC : .OUTCARD
                newRoute.totalMeters = Int(read.mmrTotalMeters!)
                newRoute.meterReads.append(read)
                // Assign first not nil region as route region
                if newRoute.region == nil && read.region != nil {
                    newRoute.region = read.region
                }
                Model.sharedInstance.routes[read.routeId] = newRoute
            }
        }
    }
    
    
    func insertjob_ID_For(meter:MeterRead) {
        switch true {
        case meter.isOutCard!:
            MeterReadManager.shared.outcardData_CallId_ACK[meter.jobID] = false
        case meter.isOffcycle!:
            MeterReadManager.shared.off_cycle_CallId_ACK[meter.jobID] = false
        case meter.isPeriodic!:
            MeterReadManager.shared.periodicData_CallId_ACK[meter.jobID] = false
        default:
            return
        }
    }
}



//    private func checkForNetworkStatus(_ mmrStatus: String) ->  MeterReadStatus {
//        switch mmrStatus {
//        case MeterReadNetworkStatus.DISP.rawValue:
//            return .available
//        case MeterReadNetworkStatus.CANCEL.rawValue:
//            return .completed
//        case MeterReadNetworkStatus.ACK.rawValue:
//            return .available
//        case MeterReadNetworkStatus.EN_Route.rawValue:
//            return .available
//        case MeterReadNetworkStatus.ON_SITE.rawValue:
//            return .inProgress
//        case MeterReadNetworkStatus.FIELD_COMPLETE.rawValue:
//            return .completed
//        default:
//            return .completed
//        }
//    }
